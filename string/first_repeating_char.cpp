// Program to find the index of first repeating character in a string
#include <iostream>
using namespace std;

int getIndex(string s){
  // Case 1: empty string
  if(s.size() == 0)
    return -1;

  // Traverse through the string + insert in asciiSet
  int asciiSet[256] = {0};
  for(int i = 0; i < s.size(); i++){
    int v = s[i];

    if(asciiSet[v] == 1)
      return i;
    else
      asciiSet[v] = 1;
  }

  cout << "all unique chars" << endl;
  return -1;
}

int main(){
  string s = "mukal";
  cout << getIndex(s) << endl;
  return 0;
}
