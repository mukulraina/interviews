function checkIsomorphic(string1, string2){
  if(string1.length != string2.length)
    return false;

  // Iterate through both strings
  var hash = {};
  for(var i = 0; i < string1.length; i++){
    // Check if the key exists in the hash
    if(string1[i] in hash){
      if(hash[string1[i]] != string2[i])
        return false;
    }

    // If it doesn't exist, insert
    else{
      hash[string1[i]] = string2[i];
    }
  }

  return true;
}

console.log(checkIsomorphic("aab", "xxy"));
console.log(checkIsomorphic("aab", "xzy"));
