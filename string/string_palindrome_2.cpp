#include <iostream>
using namespace std;

bool isPalindrome(string s){
  if(s.size() == 0)
    return false;
  else if(s.size() == 1)
    return true;
  else{
    int i = 0;
    int j = s.size() - 1;

    while(i < j){
      if(s[i] != s[j])
        return false;
      i++;
      j--;
    }

    return true;
  }
}

int main(){
  string s1 = "mukul";
  string s2 = "madam";
  string s3 = "maddam";
  string s4 = "abcdef";

  cout << isPalindrome(s1) << endl;
  cout << isPalindrome(s2) << endl;
  cout << isPalindrome(s3) << endl;
  cout << isPalindrome(s4) << endl;

  return 0;
}