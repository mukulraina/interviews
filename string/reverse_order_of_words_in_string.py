def reverseOrder(s):
  if len(s) == 0 or len(s) == 1:
    return s;
  else:
    #get the words into the list
    list = s.split()

    #put words in a stack 
    stack = []
    for word in list:
      stack.append(word)

    #put words into a new list with reverse order
    reverseList = []
    while stack:
      reverseList.append(stack.pop())

    #join the list to make a string
    newString = " ".join(reverseList)

    #return it
    return newString
    
def main():
  s = "i   like  this      program   very   much"
  print reverseOrder(s)

if __name__ == "__main__":
  main()