#include <iostream>
using namespace std;

void swap(char &a, char &b){
  char t = a;
  a = b;
  b = t;
}

string reverse(string s){
  if(s.size() == 0 || s.size() == 1)
    return s;

  else{
    int a = 0;
    int b = s.size() - 1;
    while(a < b){
      swap(s[a],s[b]);
      a++;
      b--;
    }
    return s;
  } 
}

int main(){
  cout << reverse("mukul") << endl;
  return 0;
}