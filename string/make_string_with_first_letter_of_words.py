def makeWord(s):
  if(len(s) == 0 or len(s) == 1):
    return s

  else:
    list = s.split()

    newString = []

    for word in list:
      newString.append(word[0])

    return "".join(newString)

def main():
  print makeWord("   abc    def    ghij  ")

if __name__ == "__main__":
  main()