#include <iostream>
using namespace std;

// time: O(n)
int getScore(string s){
  int score = 0;
  for(int i = 0; i < s.size(); i++){
    int v = s[i];
    score += v;
  }
  return score;
}

bool anagram(string s1, string s2){
  if(s1.size() != s2.size())
    return false;

  else if(getScore(s1) == getScore(s2))
    return true;

  else
    return false;
}

int main(){
  cout << anagram("dog","good") << endl;
  return 0;
}
