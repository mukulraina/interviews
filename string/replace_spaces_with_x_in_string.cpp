#include <iostream>
using namespace std;

void replace(char *a, int trueLength){
  char *b = a;
  char *l = a;
  int i = 0;

  // make *b point to f
  while(i != trueLength){
    b++;
    i++;
  }

  // make l point to the last char (just before the null char)
  while(*l != '\0')
    l++;

  // l points to null char right now so -- once
  l--;

  // now traverse b and l
  while(b != a){
    if(*b != ' '){
      *l = *b;
      b--;
      l--;
    }
    // space char encountered
    else{
      b--;

      *l = '0';
      l--;
      *l = '2';
      l--;
      *l = '%';
      l--;
      cout << a << endl;
    }
  }
}

int main(){
  char c[] = "  a bc d        ";
  replace(c,7);
  cout << c << endl;

  return 0;
}