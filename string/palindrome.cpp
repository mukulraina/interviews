#include <iostream>
using namespace std;

bool helper(string s, int l, int h){
  if(l >= h)
    return true;
  else{
    if(s[l] != s[h])
      return false;
    else
      return helper(s,l+1,h-1);
  }
}

bool isPalindromeRecursive(string s){
  return helper(s,0,s.size() - 1);
}

bool isPalindromeIterative(string s){
  if(s.size() == 0 || s.size() == 1)
    return true;

  else{
    int i = 0; 
    int j = s.size() - 1;

    while(i < j){
      if(s[i] != s[j])
        return false;
      i++;
      j--;
    }

    // If the program came till here that means it was never false
    return true;
  }
}

int main(){
  string s = "madam";
  cout << isPalindromeIterative(s) << endl;
  cout << isPalindromeRecursive(s) << endl;
  return 0;
}
