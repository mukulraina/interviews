#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<char,int> Mymap;

char firstNonRepeating(string s){
  if(s.size() == 0 || s.size() == 1)
    return -1;
  else{
    //declare hash
    Mymap m;
    Mymap::iterator iter;

    // put the string in hash + increment their occurences 
    for(int i = 0; i < s.size(); i++){
      iter = m.find(s[i]);

      // not in hash
      if(iter == m.end())
        m.insert(Mymap::value_type(s[i],1));
      
      // increment the value
      else
        iter->second++;
    }

    // traverse again
    for(int i = 0; i < s.size(); i++){
      iter = m.find(s[i]);

      if(iter->second == 1)
        return iter->first;
    }

    return 'X';
  }
}

int main(){
  string s = "geeksforgeeks";
  cout << firstNonRepeating(s) << endl;
  return 0;
}
