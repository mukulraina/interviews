// check CTCI Solution
#include <iostream>
#include <string>
#include <sstream>
using namespace std;;

// function to convert an int to string
string intToString(int v){
  string t; // string that will store the integer as string
  stringstream ss; // stream of characters that will store the integer
  ss << v; // writing the integer to the stream 
  t = ss.str(); // calling the str() function of stringstream to output the sequence of characters as string
  return t;
}

string runLengthEncoding(string s){
  if(s.size() == 0 || s.size() == 1)
    return "ERROR";
  else{
    char lastChar = s[0]; // to keep track of the last diff character
    string r; // the final encoded string as result
    int c = 1; // the initial count as 1
    int i = 1; // start with the 2nd element as 'l' as the first element

    while(i < s.size()){
      if(s[i] == lastChar)
        c++;
      else{
        r += lastChar + intToString(c);
        lastChar = s[i];
        c = 1;
      }
      i++;
    }

    // so the last sequenece of the string (maybe aaa or e) still needs to be written
    r += lastChar + intToString(c);

    return r;
  }
}

// Test Client
int main(){
  // Normal Case
  cout<<runLengthEncoding("aabcccccaaa")<<'\n';
  cout<<runLengthEncoding("abbccccccde")<<'\n';
  // Extreme Case
  cout<<runLengthEncoding("a")<<'\n';

  // Null/Illegal Case
  //cout<<runLengthEncoding("")<<'\n';

  // Strange Input

  return 0;
 }
