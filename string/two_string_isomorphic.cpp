#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;
typedef unordered_map<char,int> Mymap;

void stringToHash(string s, Mymap &m){
  if(s.size() == 0)
    return;
  else{
    Mymap::iterator iter;

    // Traverse the string
    for(int i = 0; i < s.size(); i++){
      iter = m.find(s[i]);

      // If its not there
      if(iter == m.end())
        m.insert(Mymap::value_type(s[i],i));
    }
  }
}

void getEncoding(string s, Mymap &m, vector<int> &v){
  Mymap::iterator iter;

  for(int i = 0; i < s.size(); i++){
    iter = m.find(s[i]);
    v.push_back(iter->second);
  }
}

bool comareEncodings(vector<int> v1, vector<int> v2){
  if(v1.size() != v2.size())
    return false;
  else{
    int i = 0; 
    while(i < v1.size()){
      if(v1[i] != v2[i])
        return false;
      i++;
    }

    return true;
  }
}

bool areIsomorphic(string s1, string s2){
  if(s1.size() != s2.size())
    return false;
  else{
    Mymap m1;
    Mymap m2;

    // Put the strings in the hashes
    stringToHash(s1,m1);
    stringToHash(s2,m2);

    vector<int> encoding1;
    vector<int> encoding2;

    // Get the encodings for both strings
    getEncoding(s1,m1,encoding1);
    getEncoding(s2,m2,encoding2);

    // Compare encodings
    return comareEncodings(encoding1,encoding2);
  }
}

int main(){
  string s1 = "hate";
  string s2 = "hell";

  cout << areIsomorphic(s1,s2) << endl;
  return 0;
}