#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

bool isUnique(string s){
  if(s.size() == 0 || s.size() > 256)
    return false;

  else{
    // sort the string
    sort(s.begin(),s.end());

    // traverse + compare
    for(int i = 1; i < s.size(); i++){
      if(s[i] == s[i+1])
        return false;
    }

    // If it reached till here, return true
    return true;
  }
}

int main(){
  string s = "abcdef";
  cout << isUnique(s) << endl;
  return 0;
}
