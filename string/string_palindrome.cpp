#include <iostream>
using namespace std;

bool isPalindromeHelper(string s){
  int j = s.size() - 1;
  for(int i = 0; i < s.size() / 2; i++){
    if(s[i] != s[j])
      return false;
    j--;
  }
  return true;
}

bool isPalindrome(string s){
  if(s.size() == 0)
    return false;

  else if(s.size() == 1)
    return true;

  else
    return isPalindromeHelper(s);
}

int main(){
  string s = "madam";
  cout << isPalindrome(s) << endl;
  return 0;
}
