#include <iostream>
using namespace std;

void replace_spaces(char *p, int originalLength, int space_count){
  // Make a pointer point to the last index
  char *l = p + originalLength + space_count;
  l--;


}

int main(){
  string s = "hello world etc etc";
  int space_count = 0;

  // Count the number of spaces in the string
  for(int i = 0; i < s.size(); i++){
    if(s[i] == ' ')
      space_count++;
  }

  // Make a new string with extra space 
  char *new_string = new char[s.size() + space_count * 2 + 1];

  // Copy the contents of the original string to the new one 
  for(int i = 0; i < s.size(); i++){
    new_string[i] = s[i];
  }

  cout << "the original string is: " << s << endl;

  replace_spaces(new_string,s.size(),space_count);
  cout << "The updated string is: " << new_string << endl;
  return 0;
}
