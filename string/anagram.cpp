bool anagram(string s1, string s2){
  if(s1.size() != s2.size())
    return false;
  else{
    int asciiSet[256] = {0};
    // Track # of occurences in the first string
    for(int i = 0; i < s1.size(); i++){
      int v = s1[i];
      asciiSet[v]++;
    }

    // Compare the # of occurences with the 2nd string
    for(int i = 0; i < s2.size(); i++){
      int v = s2[i];
      asciiSet[v]--;
    }

    // All elements of asciiSet should be 0 now
    for(int i = 0; i < 256; i++){
      if(asciiSet[i] > 0)
        return false;
    }
    return true;
  }
}