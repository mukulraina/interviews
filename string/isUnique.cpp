/* 
 * Program to check if all the charcters in a string are uniqure or not - Bool array approach
 * Algorithm:
 *  1) Make bool asciiSet[256] to represent every possible ascii character and assign all to false
 *  2) Traverse through the array
 *      a) if something is false, turn it into true
 *      b) if something is true, that means its already encountered once that means no unique, exit
 */
#include <iostream>
#include <string> 
using namespace std;

/*
 * Checks if the string has all unique characters
 * @param s         String that needs to be checked
 * @param asciiSet  Boolean array to has 1 array location for each 256 ascii characters
 * @param value     Integer typecast of the character s[i]
 * @return          True if all unique, else false
 * Time             O(n)
 */
bool isUnique(const std::string& s){
  if (s.size() == 0 || s.size() > 256)
    return false;
  
  bool asciiSet[256] = {false};
  for(int i = 0; i < s.size(); i++){
    int v = s[i];
    cout << v << endl;
    if(asciiSet[v] == true)
      return false;
    else
      asciiSet[v] = true;
  }
  return true;
}

// Test Client
int main(){
  std::cout<<isUnique("Aukul")<<'\n';
  std::cout<<isUnique("abcdef")<<'\n';
  
  return 0;
}
