#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<char,int> Mymap;

char firstNonRepeating(string s){
  if(s.size() == 0 || s.size() == 1)
    return -1;
  else{
    // declare hash
    Mymap m;
    Mymap::iterator iter;

    // traverse + check in hahs
    for(int i = 0; i < s.size(); i++){
      iter = m.find(s[i]);

      // not in hash, just insert
      if(iter == m.end())
        m.insert(Mymap::value_type(s[i],0));

      // in hash, return this char
      else
        return iter->first;
    }

    // if loop came till here that all char were unique
    return 'X';
  }
}

int main(){
  string s = "geksforks";
  string s1 = "abcdef";
  cout << firstNonRepeating(s1) << endl;
  return 0;
}
