#include <iostream>
using namespace std;

void swap(char &a, char &b){
  char t = a;
  a = b;
  b = t;
}

string reverse1(string s){
  if(s.size() == 0 || s.size() == 1)
    return s;

  else{
    int i = 0;
    int j = s.size() - 1;

    while(i < j){
      swap(s[i],s[j]);
      i++;
      j--;
    }

    return s;
  }
}

string helper(string s, int l, int h){
  if(l >= h)
    return s;
  else{
    swap(s[l],s[h]);
    return helper(s,l + 1,h - 1);
  }
}

string reverse2(string s){
  return helper(s,0,s.size() - 1);
}

int main(){
  string s = "1234 5678";
  cout << reverse1(s) << endl;
  cout << reverse2(s) << endl;
  return 0;
}
