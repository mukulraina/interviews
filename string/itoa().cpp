#include <iostream>
#include <string>
using namespace std;

string reverse(string &s){
    int i = 0; 
    int j = s.size() - 1;
    
    while(i < j){
        char t = s[i];
        s[i] = s[j];
        s[j] = t;
    }
}

// Converts a number into string
// if numberis 1234 it will return a string saying 1234
string intToString(int n){
    string result;
    char c;
    
    while(n > 0){
        // get a digit from the number
        int digit = n % 10;
        
        // update the value of the number
        n = n / 10;
        
        // get the ascii value of the digit
        c = '0' + digit;
        
        // append the char to the result string
        result += c;
    }
    
    return reverse(result);
}

int main(){
   cout << intToString(1234) << endl;
   return 0;
}

