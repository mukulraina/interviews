#include <iostream>
using namespace std;

// OL = original length of characters (without the extra buffer in the end)
// sp = # of spaces
// NL = new length (original length + buffer)
void replace_spaces(char *s, int OL, int sp){
  int i = OL - 1; // 9
  int NL = OL + sp * 2; // 16
  int j = NL - 1; // 15
  
  while(i >= 0){
    if(s[i] != ' '){
      s[j] = s[i];
      j--;
      i--;
    }
    else{
      s[j] = '0';
      j--;
      s[j] = '2';
      j--;
      s[j] = '%';
      j--;
      i--;
    }
  }
}

int main(){
  string s = "ab cd ef g";
  int space_count = 0;

  // Count the number of spaces in the string
  for(int i = 0; i < s.size(); i++){
    if(s[i] == ' ')
      space_count++;
  }

  // Make a new string with extra space (original size + spaceCount * 2)
  char *new_string = new char[s.size() + space_count * 2];

  // Copy the contents of the original string to the new one 
  for(int i = 0; i < s.size(); i++){
    new_string[i] = s[i];
  }

  cout << "the original string is: " << s << endl;

  replace_spaces(new_string,s.size(),space_count);
  cout << "The updated string is: " << new_string << endl;
  return 0;
}
