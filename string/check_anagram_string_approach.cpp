bool checkAnagram(string a, string b){
  if(a.size() != b.size()) // can't be anagram if different sizes
    return false;

  // sort both of the strings - O(n logn)
  sort(a.begin(), a.end());
  sort(b.begin(), b.end());

  // compare the sorted strings now
  // they should be exactly equal to be anagram
  for (int i = 0; i < s1.size(); i++){
    if(s1[i] != s2[i])
      return false; // if found a character to be not matching, return
  }
  return true;
}