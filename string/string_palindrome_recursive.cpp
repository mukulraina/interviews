#include <iostream>
using namespace std;

bool isPalindrome(char *s, int l){
  if(l == 0 || l == 1)
    return true;

  else{
    if(s[0] != s[l - 1])
      return false;
    else
      return isPalindrome(s+1,l-2);
  }
}

int main(){
  char s[] = "lakkul";
  int size = 6;
  cout << isPalindrome(s,size) << endl;
  return 0;
}