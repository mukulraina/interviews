#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

void printPairs(int *a, int size, int v){
  if(size == 0 || size == 1)
    return;
  else{
    Mymap m;
    Mymap::iterator it;

    for(int i = 0; i < size; i++){
      it = m.find(v - a[i]);

      if(it != m.end())// found it
        cout << a[i] << ":" << v - a[i] << endl;
      else // didn't find it, insert
        m.insert(Mymap::value_type(a[i],1));
    }
  }
}

int main(){
  int a[] = {1, 4, 4, 45, 6, 2,10, 8};
  int v = 8;
  int size = 8;
  printPairs(a,size,v); 
  return 0;
}