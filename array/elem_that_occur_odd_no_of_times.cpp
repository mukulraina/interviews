#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

void elementsThatOccurOddNumberOfTimes(int *a, int size){
    // Case 1: size 0 array
    if(size == 0)
        cout << "sorry empty list" << endl;

    // Case 2: size 1 array
    else if(size == 1)
        cout << a[0] << endl;

    // Case 3: More than 1 elements
    else{
        // Make a hash
        Mymap hash;
        Mymap::iterator iter;

        // Traverse through the array and put elemens in hash
        // Time: O(n)
        for(int i = 0; i < size; i++){
            iter = hash.find(a[i]);
            if(iter == hash.end())
                hash.insert(Mymap::value_type(a[i],1));
            else
                iter->second++;
        }

        // Now we traverse again and see the ->second of all elements in the hash
        // Time: O(n)
        for(int i = 0; i < size; i++){
            iter = hash.find(a[i]);
            if(iter->second % 2 != 0)
                cout << iter->first << endl;
        }
    }
}

int main()
{
    int a[] = {1,4,5,6,2,7,8,2,1,4};
    
    elementsThatOccurOddNumberOfTimes(a, 10);
    cout<<'\n';
    
    return 0;
}