#include <iostream>
using namespace std;

void getSubarray(int *a, int size, int sum){
  if(size == 0)
    return;
  else if(size == 1){
    if(a[0] != sum)
      return;
    else
      cout << 0;
  }
  else{
    int i = 0; 
    int c = 0;

    for(i = 0; i < size; i++){
      c = c + a[i];

      if(c == sum){
        cout << "0:" << i << endl;
      }

      if(c > sum)
        break;
    }

    if(c < sum) // that means the array size was too small for the sum
      return;

    // if c > sum that means i points the final location
    // start removing elements from the start
    int k = 0;
    while(k < i){
      c = c - a[k];
      cout << c << endl;
      if(c == sum)
        break;

      k++;
    }

    // if k surpassed i, no sum was exact equal to 33
    if(k >= i){
      cout << "k>=i" << endl;
      return;
    }

    // that means it was found!
    cout << k + 1 << ":" << i << endl;
  }
}


int main(){
  int a[] = {1,4,20,3,10,5};
  int sizeA = 6;
  int sumA = 33;

  int b[] = {1,4,0,0,3,10,5};
  int sizeB = 7;
  int sumB = 7;

  getSubarray(a,sizeA,sumA);
  getSubarray(b,sizeB,sumB);

  return 0;
}