#include <iostream>
#include <vector>
using namespace std;

void printVector(vector<int> result){
  for(int i = 0; i < result.size(); i++)
    cout << result[i] << '\t';
  cout << endl;
}

void intersection(vector<int> a, vector<int> b, int aSize, int bSize){
  if(aSize == 0 || bSize == 0)
    return;

  vector<int> result;

  int i = 0;
  int j = 0;

  while(i < aSize && j < bSize){
    if(a[i] < b[j])
      i++;
    else if(b[j] < a[i])
      j++;
    else{
      // if its empty, just push the common element + leave
      if(result.size() == 0)
        result.push_back(a[i]);

      // else, there are some elements in the result vector, so check
      // if the element you're trying to push is not already pushed
      else{
        // get the index of the last element in the vector
        int lastIndex = result.size() - 1;

        // check if its not already there
        if(a[i] != result[lastIndex])
          result.push_back(a[i]);
      }

      i++;
      j++;
    }
  }

  printVector(result);
}

int main(){
  vector<int> a;
  vector<int> b;

  a.push_back(1);
  a.push_back(1);
  a.push_back(3);
  a.push_back(4);
  a.push_back(5);
  a.push_back(6);
  a.push_back(7);
  a.push_back(8);

  b.push_back(1);
  b.push_back(1);
  b.push_back(1);
  b.push_back(2);
  b.push_back(2);
  b.push_back(3);
  b.push_back(5);
  b.push_back(6);
  b.push_back(8);


  intersection(a,b,a.size(),b.size());

  return 0;
}
