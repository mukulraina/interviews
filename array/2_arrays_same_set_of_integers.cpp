#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

void printVector(vector<int> a){
  for(int i = 0; i < a.size(); i++)
    cout << a[i] << '\t';
  cout << endl;
}

// Still need to test for all cases
/*
bool sortingApproach(vector<int> a, vector<int> b){
  if(a.size() == 0 && b.size() == 0)
    return true;

  else if(a.size() == 0 || b.size() == 0)
    return false;

  else if(a.size() != b.size())

  // So now we know both of them have same 
  else{
    // Sort the 2 arrays - O(n logn)
    sort(a.begin(),a.end());
    sort(b.begin(),b.end());

    // Now compare
  }
}
*/

int main(){
  vector<int> a;
  a.push_back(1);
  a.push_back(2);
  a.push_back(3);
  a.push_back(4);
  a.push_back(5);

  vector<int> b;
  b.push_back(5);
  b.push_back(2);
  b.push_back(1);
  b.push_back(4);
  b.push_back(3);

  cout << sortingApproach(a,b);
}