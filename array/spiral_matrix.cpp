/* This code is adopted from the solution given 
   @ http://effprog.blogspot.com/2011/01/spiral-printing-of-two-dimensional.html */
 
#include <stdio.h>
#define R 3
#define C 6
 
void spiralPrint(int highR, int highC, int a[R][C])
{
    int i, lowR = 0, lowC = 0;
 
    /*  lowR - starting row index
        highR - ending row index
        lowC - starting column index
        highC - ending column index
        i - iterator
    */
 
    while (lowR < highR && lowC < highC)
    {
        /* Print the first row from the remaining rows */
        for (i = lowC; i < highC; ++i)
        {
            printf("%d ", a[lowR][i]);
        }
        lowR++;
 
        /* Print the last column from the remaining columns */
        for (i = lowR; i < highR; ++i)
        {
            printf("%d ", a[i][highC-1]);
        }
        highC--;
 
        /* Print the last row from the remaining rows */
        // so that you don't print the same row twice
        if ( lowR < highR)
        {
            for (i = highC-1; i >= lowC; --i)
            {
                printf("%d ", a[highR-1][i]);
            }
            highR--;
        }
 
        /* Print the first column from the remaining columns */
        if (lowC < highC)
        {
            for (i = highR-1; i >= lowR; --i)
            {
                printf("%d ", a[i][lowC]);
            }
            lowC++;    
        }        
    }
}
 
/* Driver program to test above functions */
int main()
{
    int a[R][C] = { {1,  2,  3,  4,  5,  6},
        {7,  8,  9,  10, 11, 12},
        {13, 14, 15, 16, 17, 18}
    };
 
    spiralPrint(R, C, a);
    return 0;
}
 
/* OUTPUT:
  1 2 3 4 5 6 12 18 17 16 15 14 13 7 8 9 10 11
*/