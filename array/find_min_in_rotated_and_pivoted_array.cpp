#include <iostream>
using namespace std;

int findMin(int a[], int low, int high)
{
    // base case
    if (high < low)  return a[0];
 
    // only 1 element left
    if (high == low) return a[low];
 
    // Find mid
    int mid = low + (high - low)/2; /*(low + high)/2;*/
 
    // Check if element (mid+1) is minimum element.
    if (mid < high && a[mid+1] < a[mid])
       return a[mid+1];
 
    // Check if mid itself is minimum element
    if (mid > low && a[mid] < a[mid - 1])
       return a[mid];
 
    // Decide whether we need to go to left half or right half
    if (a[high] > a[mid])
       return findMin(a, low, mid-1);
    return findMin(a, mid+1, high);
}

void testcase1(){
    int a[] = {5, 6, 1, 2, 3, 4};
    int size = 6;
    cout << findMin(a,0,size - 1) << endl;
}

void testcase2(){
    int a[] = {1, 2, 3, 4};
    int size = 4;
    cout << findMin(a,0,size - 1)  << endl;
}

void testcase3(){
    int a[] = {2, 1};
    int size = 2;
    cout << findMin(a,0,size - 1)  << endl;
}

int main(){
    testcase1();
    testcase2();
    testcase3();

    return 0;
}