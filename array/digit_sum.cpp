#include <iostream>
using namespace std;

bool singleDigitNumber(int n){
  if(n >=0 && n <= 9)
    return true;
  else
    return false;
}

void helper(int n, int &s){
  while(n > 0){
    s += n % 10; 
    n = n / 10;
  }
}

int getSumDigit(int n){
  if(singleDigitNumber(n))
    return n;
  else{
    int s = 0;
    helper(n,s);
    return s;
  }
}

int main(){
  int n = 1234;
  cout << getSumDigit(n) << endl;
  return 0;
}
