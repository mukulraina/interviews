#include <iostream>
using namespace std;

int min(int a, int b){
  if(a < b)
    return 0;
  else 
    return 1;
}

void getMinAnd2ndMin(int *a, int s){
  if(s == 0 || s == 1)
    cout << -1;
  else if(s == 2){
    cout << -1;
  }
  else{
    int min1 = INT_MAX;
    int min2 = INT_MAX;

    for(int i = 0; i < s; i++){
      if(a[i] < min1){
        min2 = min1;
        min1 = a[i];
      }

      if(a[i] > min1 && a[i] < min2)
        min2 = a[i];
    }

    cout << min1 << endl;
    cout << min2 << endl;
  }
}

int main(){
  int a[] = {12,11,13,5,6,10,34,1};
  getMinAnd2ndMin(a,8);
  return 0;
}
