#include <iostream>
using namespace std;

int getSumHelper(int *a, int size, int &sum){
  if(size == 0)
    return sum;
  else{
    sum += a[0];
    return getSumHelper(a+1,size-1,sum);
  }
}

int getSum(int *a, int size){
  // Case 1: 0 size
  if(size == 0)
    return -1;

  // Case 2: 1 element 
  else if(size == 1)
    return a[0];

  // Case 3: More than 1 element
  else{
    int sum = 0;
    return getSumHelper(a,size,sum);
  }
}

int main(){
  int a[] = {8,5,10,4,7,9,14,3};
  int size = 8;
  cout << getSum(a,size) << endl;
  return 0;
}
