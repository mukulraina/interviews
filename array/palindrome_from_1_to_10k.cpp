/*
  Program to count number of palindrome numbers between 1 to 10,000

  Approach 1: Traversing + checking every number is palindrome or not
    In this approach we would have run a loop from 1 to 10,000 and 
    passing every number to a function isPalindrome() to check if its palindrome 
    or not. 

    However isPalindrome() takes O(n) to run whether you take the 
    reverse the number approach, comparing elements from start to end or put all 
    digits on a stack and then keep poping it. 
  
    So when we call isPalindrome() n times the time complexity would have been O(n^2)

  Approach 2: Generating prime numbers between 1 and 10,000
    That meant we need to generate a palindrome number for 1 digit, 2 digits, 3 digits
    and 4 digits elements

    I noticed a pattern for all of them and every loop takes O(n) to run, but since they
    are seperate 4 loops and not nested it takes O(n) time

    Patterns: 
      1 digits - all of them are palindrome
      2 digits - if both digits are equal
      3 digits - if 1st and 3rd digits are equal
      4 digits - if (1st && 4th) && (2nd && 3rd) digits are equal 
*/

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

/*
 * Converts an integer to a string
 * @param n       Number that needs to be converted to string
 * @param result  String that has the same contents as of the integer but of string type
 * @return        Converts an integer to string and returns it
 */
string intToString(int n){
  stringstream convert; // stringstream used for converstion
  convert << n; //add the value of 'n' to the characters in the stream
  string result = convert.str(); //set 'result' to the content of the stream
  return result;
}

/*
 * Returns the number of palindromes between 1 to 10,000
 * @param count         Variable to keep track of the number of palindromes 
 * @param s             Integer is converted to a string called 's'
 * @param numOfDigits   Number of digits in the number
 * @return              Returns the number of palindromes between 1 to 10,000
 * Time:                O(n)
 */
int getNoOfPalindromes(){
  int count = 0;

  for(int i = 1; i <= 9999; i++){
    string s = intToString(i);
    int numOfDigits = s.size();
    
    switch(numOfDigits){
      case 1:
        count++;
        break;
      case 2:
        if(s[0] == s[1])
          count++;
        break;
      case 3:
        if(s[0] == s[2])
          count++;
        break;
      case 4:
        if((s[0] == s[3]) && (s[1] == s[2]))
          count++;
        break;
      default: 
        cout << "error" << endl;
    }
  }
  return count;
}

// Test Client
int main(){
  cout << getNoOfPalindromes() << endl; // Returns 198
  return 0;
}
