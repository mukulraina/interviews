// dont follow geeksforgeeks for this question
#include<iostream>
using namespace std;

void swap(int &a, int &b){
  int t = a;
  a = b;
  b = t;
}

void rev(int *a, int l, int h){
  while(l < h){
    swap(a[l],a[h]);
    l++;
    h--;
  }
}

// Time: O(n)
void rotate(int *a, int s, int k){
  if(s == 0 || s == 1)
    return;
  else{
    rev(a,0,s-1);

    rev(a,0,s-1-k);

    rev(a,s - k, s - 1);
  }
}

void printArray(int *a, int s){
  for(int i = 0; i < s; i++)
    cout << a[i] << '\t';
  cout << endl;
}

int main(){
  int a[] = {1,2,3,4,5,6,7};
  int s = 7;
  int k = 4;

  rotate(a,s,k);

  printArray(a,s);

  return 0;
}