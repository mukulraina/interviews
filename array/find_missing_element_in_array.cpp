int getMissingNumber(int *a, int size){
  if(size == 0)
    return -1;
  else{
    // the range will be 1 greater than the size of elements
    int max = size + 1;
    
    // get the sum of all the elements 
    // could have ran a loop but using this math formula is O(1)
    int sum = ((max * (max + 1)) / 2);

    // Traverse through the array and subtracting each element from sum
    for(int i = 0; i < size; i++){
        sum = sum - a[i];
    }
    
    // In the end after subtracting all elements sum will be the missing element
    return sum;
  }
}