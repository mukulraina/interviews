#include <iostream>
using namespace std;

void swap(int &a, int &b){
  cout << "h" << endl;
  int t = a;
  a = b;
  b = t;
}

void printArray(int *a, int s){
  for(int i = 0; i < s; i++)
    cout << a[i] << '\t';
  cout << endl;
}

void sortArray(int *a, int s){
  if(s == 0 || s == 1)
    return;

  else{
    int i = 0;
    int j = size - 1;

    while(i < j){
      while(a[i] == 0 && i < j)
        i++;

      while(a[j] == 1 && i < j)
        j--;

      if(i < j){
        swap(a[i],a[j]);
        i++;
        j--;
      }
    }
  }
}

int main(){
  int a[] = {0,1,0,1,0,0,1,1,1,0};
  int size = 10;

  sortArray(a,size);

  printArray(a,size);

  return 0;
}
