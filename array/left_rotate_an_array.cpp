#include <iostream>
#include <queue>
using namespace std;

void leftRotateTempBuffer(int *a, int k, int size){
	if(size == 0 || size == 1 || k == 0)
		return;

	queue<int> q;
	int i;

	// push the first k elements in the queue
	for(i = 0; i < k; i++){
		q.push(a[i]);
	}

	// now i points to the 3rd element (if k was 2)
	int j = 0;
	while(i < size){
		a[j] = a[i];
		j++;
		i++;
	}

	// now j points to the first location from where we need to enter queue elements
	while(q.empty() != true){
		a[j] = q.front();
		q.pop();
		j++;
	}
}

void printArray(int *a, int size){
	for(int i = 0; i < size; i++)
		cout << a[i] << '\t';
	cout << endl;
}

int main(){
	int a[] = {1, 2, 3, 4, 5, 6, 7};
	int size = 7;

	printArray(a,size);
  leftRotateTempBuffer(a, 2, size);
  printArray(a,size);
}