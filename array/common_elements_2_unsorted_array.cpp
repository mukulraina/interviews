#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

void printCommonElements(int *a, int aSize, int *b, int bSize){
	if(aSize == 0 || bSize == 0)
		return;
	else{
		// Declare hash + iterator
		Mymap m;
		Mymap::iterator it;

		// Traverse the first array + put it into hash
		for(int i = 0; i < aSize; i++){
			it = m.find(a[i]);

			// only insert if its not already present in the hash
			if(it == m.end())
				m.insert(Mymap::value_type(a[i],1));
		}

		// Traverse the 2nd array + check if a[i] is present in the hash
		// if its present in the hash, that menas its present in the array, hence part of the intersection
		for(int i = 0; i < bSize; i++){
			it = m.find(b[i]);

			// its the element is in the hash already
			if(it != m.end())
				cout << b[i] << '\t';
		}
		cout << endl;
	}
}

int main(){
	int a[] = {1,4,5,2,7,3,9};
	int aSize = 7;

	int b[] = {5,2,4,9,5};
	int bSize = 5;

	printCommonElements(a,aSize,b,bSize);

	return 0;
}