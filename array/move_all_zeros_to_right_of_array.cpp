#include <iostream>
using namespace std;

void shift(int *a, int size){
	if(size == 0 || size == 1)
		return;

	int i = 0;
	int j = 0;

	// move the non zero elements to the left
	while(i < size){
		// if you found non zero element, push it onto the a[j] location + increment j
		if(a[i] != 0){
			a[j] = a[i];
			j++;
		}

		i++;
	}

	// populate zeros now on the right side
	while(j < size){
		a[j] = 0;
		j++;
	}
}

void printArray(int *a, int size){
	for(int i = 0; i < size; i++)
		cout << a[i] << '\t';

	cout << endl;
}

int main(){
	int a[] = {1, 9, 8, 4, 0, 0, 2, 7, 0, 6, 0, 9};
	int size = 12;

	printArray(a,size);

	shift(a,size);

	printArray(a,size);
	return 0;
}