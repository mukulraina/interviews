#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

// intead of traversing twice - lookup in the hash as you're inserting in it 
// look at better solution in geeksforgeeks/array
void printPairs(int *a, int size, int v){
  // Case 1: empty array
  if(size == 0)
    cout << "empty array" << endl;

  // Case 2: 1 element in the array
  else if(size == 1)
    cout << "sorry no possibility of a pair" << endl;

  // Case 3: 2 elements in the array
  else if(size == 2){
    if((a[0] + a[1]) == v)
      cout << a[0] << ":" << a[1] << endl; 
  }

  // Case 4: MOre than 2 elements in the array
  else{
    // Declare a hash
    Mymap hash;
    Mymap::iterator iter;

    // Traverse and put all values in hash - O(n)
    for(int i = 0; i < size; i++){
      iter = hash.find(a[i]);

      // if its not in the hash, insert
      if(iter == hash.end())
        hash.insert(Mymap::value_type(a[i],1));
      
      // Increment the # of occurence of the element
      else
        iter->second++;
    }

    // Traverse again and try finding pairs
    for(int i = 0; i < size; i++){
      iter = hash.find(v - a[i]);

      // Lets say you found the element,
      if(iter != hash.end() && iter->second != -1){

        //make sure its not the same one (4,4 -> 8)
        if(a[i] == (v - a[i])){
          if(iter->second > 1)
            cout << a[i] << ":" << iter->first << endl;
        }
        else
          cout << a[i] << ":" << iter->first << endl;

        // Mark the current value as checked
        iter = hash.find(a[i]);
        iter->second = -1; // so that when we find 2,6 and 6,2 we dont print it twice
      }
    }
  }
}

int main(){
  int a[] = {1, 4, 4, 45, 6, 2,10, 8};
  int v = 8;
  int size = 8;
  printPairs(a,size,v); 
  return 0;
}