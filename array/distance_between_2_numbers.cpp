#include <iostream>
using namespace std;

int min(int a, int b){
  return (a < b)? a : b;
}

int getDistance(int *arr, int size,int x, int y){
  if(size == 0 || size == 1)
    return -1;

  else{
    int i = 0;
    int a = INT_MIN;
    int b = INT_MIN;
    int aLoc = INT_MIN;
    int bLoc = INT_MIN;
    int d;

    //Traverse to find a
    while(arr[i] != x && arr[i] != y && i < size){
      i++;
    }

    // None of those values not found in array
    if(i == size - 1){
      cout << "sorry none of those 2 values were found in the array" << endl;
      return -1;
    }

    if(arr[i] == x){
      a = x; // a points to the first number encountered 
      b = y;
    }

    // That means a[i] is y
    else{
      b = x;
      a = y; // a still points to the 1st number encountered
    }

    // Traverse to find b
    while(arr[i] != b && i < size){

      // If you found a again - update a's location
      if(arr[i] == a)
        aLoc = i;

      i++;
    }

    if(i == size - 1){
      cout << "second value not found in the array" << endl;
      return -1;
    }

    // Now i points to b
    bLoc = i;

    // Calculate the initial distance
    d = bLoc - aLoc;

    // Now traverse the loop normally
    while(i < size){
      
      // If we found b first, update b's location
      if(arr[i] == b){
        bLoc = i;
        d = min(d,(bLoc - aLoc));
      }

      if(arr[i] == a){
        aLoc = i;
        d = min(d,(aLoc - bLoc));
      }

      i++;
    }

    // Return distance
    return d;
  }
}

int main(){
/* 
  int a[] = {2,5,3,5,4,4,2,3};
  int size = 8;
  int x = 2;
  int y = 3;

  cout << getDistance(a,size,x,y) << endl;
*/
  int b[] = {3, 5, 4, 2, 6, 5, 6, 6, 5, 4, 8, 3};
  int size = 12;
  int x = 6;
  int y = 3;

  cout << getDistance(b,size,x,y) << endl;


  return 0;
}