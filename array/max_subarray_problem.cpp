#include<iostream>
using namespace std;

int getMax(int *a, int s){
  int max = INT_MIN;
  for(int i = 0; i < s; i++){
    if(a[i] > max)
      max = a[i];
  }
  return max;
}

bool allElementNegative(int *a, int s){
  if(s == 0)
    return false;
  else{
    for(int i = 0; i < s;i++){
      if(a[i] >= 0)
        return false;
    }
    return true;
  }
}

int getMaxSubarraySum(int *a, int s){
  if(s == 0)
    return -1;
  else if(s == 1)
    return a[0];
  else if(allElementNegative(a,s))
    return getMax(a,s);
  else{
    int currentMax = 0; // current sum
    int maxSum = 0; // max sum so far
    int maxLeft = 0;
    int maxRight = 0;
    int left = 0;
    int right = 0;

    // traverse the array
    for(int i = 0; i < s; i++){
      currentMax = currentMax + a[i];

      if(currentMax > maxSum){
        maxSum = currentMax;
        right = i;
        maxLeft = left;
        maxRight = right;
      }

      if(currentMax < 0){
        currentMax = 0;
        left = i + 1;
        right = i + 1;
      }
    }

    cout << maxLeft << ":" << maxRight << endl;
    return maxSum;
  }
}

int main(){
  int a[] = {-2,-3,4,-1,2,-4,-2};
  int b[] = {-2, -3, 4, -1, -2, 1, 5, -3};
  int sA = 7;
  int sB = 8;
  cout << getMaxSubarraySum(a,7) << endl;
  cout << getMaxSubarraySum(b,8) << endl; 
  return 0;
}