int lastOccurence(int *a, int l, int h, int x, int size){
	if(l > h)
		return -1;
	else{
		int m = (l + h) / 2;

		// need to have a size here (not h cos h changes with each recursive call)
		if((a[m] == x) && ((m == size - 1) || a[m + 1] > x)
			return m;
		else if(x < a[m])
			return lastOccurence(a,0,m-1,x,size);
		else
			return lastOccurence(a,m+1,h,x,size);
	}
}