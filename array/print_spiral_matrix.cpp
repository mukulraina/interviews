#include <iostream>
using namespace std;

#define R 3
#define C 6

void spiralPrint(int m, int n, int a[R][C]){
  int r0 = 0;
  int c0 = 0;
  int rMax = m - 1;
  int cMax = n - 1; 
  int i;

  while(r0 <= rMax && c0 <= cMax){
    
    for(i = c0 ; i <= cMax; i++)
      cout << a[r0][i] << endl;
    r0++;

    for(i = r0; i <= rMax; i++)
      cout << a[i][cMax] << endl;
    cMax--;

    // extra conditions to make sure that^
    // the conditions we defined could have turned true cos
    // of the above 2 loops
    // so if r0 > rMax THAT MEANS THAT ROW IS ALREADY PRINTED
    // SO DONT PRINT IT AGAIN
    if(r0 < rMax){
      for(i = cMax; i >= c0; i--)
        cout << a[rMax][i] << endl;
      rMax--;
    }

    // TO MAKE SURE THAT COLUMAN IS NOT ALREADY PRINTED
    if(c0 < cMax){
    for(i = rMax; i >= r0; i--)
      cout << a[i][c0] << endl;
    c0++;
    }
    
  }
}

int main()
{
    int a[R][C] = {{1,  2,  3,  4,  5,  6},
                  {7,  8,  9,  10, 11, 12},
                  {13, 14, 15, 16, 17, 18}
    };
 
    spiralPrint(R, C, a);
    return 0;
}
