#include <iostream>
using namespace std;

void printArray(int *a, int size){
	for(int i = 0; i < size; i++)
		cout << a[i] << '\t';
	cout << endl;
}

void swap(int &a, int &b){
	int t = a;
	a = b;
	b = t;
}

void moveZerosToRight(int *a, int size){
	if(size == 0)
		return;
	else{
		int zeroIndex = 0;
		int j = 0;

		// Traverse to the first 0 + keep track of its location
		while(j < size){

				if(a[j] == 0){
					zeroIndex = j;
					break;
				}
				
			j++;
		}

		// Traverse the rest of the array
		while(j < size){
			
			if(a[j] != 0){
				swap(a[j],a[zeroIndex]);
				zeroIndex++;
			}
			
			j++;
		}

	}
}

int main(){
	int a[] = {1, 9, 8, 4, 0, 0, 2, 7, 0, 6, 0};
	int size = 11;

	printArray(a,size);
	moveZerosToRight(a,size);
	printArray(a,size);
	return 0;
}