// Case 1: all elements same, every element is a peak element
// Case 2: increasing order, last element is peak
// Case 3: decreasing order, first element is peak
// Case 4: random order
#include <iostream>
using namespace std;

/*
	if middle element is greater than both of its neighbors, then we return it. 
	If the middle element is smaller than the its left neighbor, then there is always a peak in left half (Why? take few examples). 
	If the middle element is smaller than the its right neighbor, then there is always a peak in right half (due to same reason as left half). 
*/
int getPeakElement(int *a, int l, int h, int size){
	int m = (l + h) / 2;

	// check if mid is peak element
	if((m ==0 || a[m-1] < a[m]) && (m == size - 1 || a[m] > a[m+1]))
		return a[m];

	// if left is bigger than mid, there must be a peak element in left half
	else if(mid > 0 && a[m-1] > a[m])
		return peak(a,l,m-1,size);

	else // if the right neighbour is bigger
		return peak(a,m+1,h,size);
}

int main(){
	int a[] =  {10, 20, 15, 2, 23, 90, 67};
	int l = 0;
	int h = 6
	int size = 7;

	cout << getPeakElement(a,l,h,size);

	return 0;
}