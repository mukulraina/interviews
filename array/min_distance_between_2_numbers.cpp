#include <iostream>
using namespace std;

int min(int a, int b){
  return (a < b)? a : b;
}

// fixed a bug - was returning the most recent dist (not the minimum one)
int getDistance(int *arr, int size,int x, int y){
  if(size == 0 || size == 1)
    return -1;
  else{
    int a;
    int b;
    int aLoc = -1;
    int bLoc = -1;
    int d = 0;
    int i = 0;
    int minD = -1;

    // Look for the first element
    while(i < size){
      if(arr[i] == x || arr[i] == y){
        a = arr[i];
        aLoc = i;
        break;
      }
      i++;
    }

    // none of the 2 numbers were found
    if(i == size)
      return -1;

    if(a == x)
      b = y;
    else
      b = x;

    // Now lets look for the 2nd element + traverse the array
    while(i < size){
      // found the 2nd element
      if(arr[i] == b){
        bLoc = i;
        d = abs(bLoc - aLoc);

        if(minD == -1)
          minD = d;
        if(d < minD)
          minD = d;
      }

      // Found the 1st element again
      if(arr[i] == a){
        aLoc = i;

        // only do the stuff if the 2nd element has been found once
        if(bLoc != -1){
          d = abs(aLoc - bLoc);
          
          if(minD == -1)
            minD = d;
          if(d < minD)
            minD = d;
        }
      }

      i++;
    }

    return minD;
  }
}

int main(){
 
  int a[] = {2,5,3,5,4,4,2,1,1,1,3};
  int size = 11;
  int x = 2;
  int y = 3;

  cout << getDistance(a,size,x,y) << endl;
/*
  int b[] = {3, 5, 4, 2, 6, 5, 6, 6, 5, 4, 8, 3};
  int size = 12;
  int x = 6;
  int y = 3;

  cout << getDistance(b,size,x,y) << endl;
*/

  return 0;
}