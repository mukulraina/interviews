#include <iostream>
using namespace std;

int fib(int n){
  if(n <= 1)
    return n;
  else
    return (fib(n - 2) + fib(n - 1));
}

int fibIterative(int n){
  if(n <= 1)
    return n;
  else{
    int f0 = 0;
    int f1 = 1;
    int sum = 0;
    for(int i = 2; i <= n; i++){
      sum = f0 + f1;
      f0 = f1;
      f1 = sum;
    }
    return f1;
  }
}

int main(){ 
  cout << fibIterative(7) << endl;
  return 0;
}