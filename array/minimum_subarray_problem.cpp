void getMinimumSubarray(int *a, int size){
	if(size == 0)
		return;
	else if(allPositive(a,size)) // if all +ve number, return the smallest of them
		cout << getSmallest(a,size) << endl; 
	else if(allNegative(a,size)) // if all -ve number, return the biggest of them all
		cout << getBiggest(a,size) << endl;
	else{
		// that means there's some combo of -ve and +ve number
		int minL = 0;
		int minR = 0;
		int left = 0; 
		int right = 0;
		int currentMinSum = 0;
		int overallMinSum = 0;

		for(int i = 0; i < size; i++){
			currentMinSum += a[i];

			if(currentMinSum >= 0){
				currentMinSum = 0;
				left = i + 1;
				right = i + 1;
			}
			else{
				// that means there's some -ve sum that can be compared
				if(currentMinSum < overallMinSum){
					overallMinSum = currentMinSum;
					minR = i;
					right = i;
				}
			}
		}

		cout << overallMinSum << '\t' << minL << '\t' << minR << endl;
	}
}