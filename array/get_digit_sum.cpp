#include <iostream>
using namespace std;

int getSumDigitHelper(int n, int &sum){
  if(n == 0)
    return sum;

  else{
    int digit = n % 10;
    sum += digit;
    n = n / 10;
    return getSumDigitHelper(n,sum);
  }
}

int getSumDigit(int n){
  int sum = 0;
  return getSumDigitHelper(n,sum);
}

int main(){
  int n = 1234;
  cout << getSumDigit(n) << endl;
  return 0;
}
