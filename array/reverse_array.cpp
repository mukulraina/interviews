#include <iostream>
using namespace std;

void swap(int &a, int &b){
  int t = a;
  a = b;
  b = t;
}

void reverse(int *a, int size){
  if(size == 0 || size == 1)
    return;

  else{
    int f = 0;
    int l = size - 1;
    while(l > f){
      swap(a[f],a[l]);
      f++;
      l--;
    }
  }
}

void printArray(int *a, int size){
  for(int i = 0; i < size; i++)
    cout << a[i] << '\t';
  cout << endl;
}

int main(){
  int a[] = {1,2,3,4,5};
  int size = 5;

  cout << "arry before reverse" << endl;
  printArray(a,size);
  
  reverse(a,size);

  cout << "arry after reverse" << endl;
  printArray(a,size);
  return 0;
}
