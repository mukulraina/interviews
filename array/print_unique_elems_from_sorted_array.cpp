#include <iostream>
using namespace std;

void printUnique(int *a, int size){
  if(size == 0)
    return;
  else if(size == 1)
    cout << a[0] << endl;
  else{
    for(int i = 0; i < size - 1; i++)
      if(a[i] != a[i + 1])
        cout << a[i] << '\t';
    cout << endl;
  }
}

int main(){
  int a[] = {1,2,2,3,4,5,5,6,7,7,8,9};
  int size = 12;

  printUnique(a,size);

  return 0;
}
