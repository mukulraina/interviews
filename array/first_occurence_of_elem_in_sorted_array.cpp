int firstOccurence(int *a, int l, int h, int x){
	if(l > h)
		return -1;
	else{
		// get mid
		int m = (l + h) / 2;

		if((a[m] == x) && (m == 0 || a[m - 1] < x)
			return m;
		else if(x < a[m])
			return firstOccurence(a,0,m-1,x);
		else
			return firstOccurence(a,m+1,h,x);
	}
}