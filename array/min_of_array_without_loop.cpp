#include<iostream>
using namespace std;

void getMinHelper(int *a, int l, int h, int &min){
  if(l <= h){
    if(a[l] < min)
      min = a[l];

    getMinHelper(a,l+1,h,min);
  }
}

int getMin(int *a, int size){
  if(size == 0)
    return -1;
  else if(size == 1)
    return a[0];
  else{
    int min = a[0];
    int l = 0; 
    int h = size - 1;
    getMinHelper(a,l,h,min);
    return min;
  }
}

int main(){
  int a[] = {4,1,3,6,1,7,3,8,3,1,0};
  int size = 11;
  cout << getMin(a,size) << endl;
  return 0;
}
