#include <iostream>
using namespace std;

void swap(int &a, int &b){
  int t = a;
  a = b;
  b = t;
}

void get2Nos(int *a, int size, int &f, int &s){
  // first no
  f = a[0];

  // finding 2nd no
  int k = 0; 
  while(a[k] == f)
    k++;
  
  //now k points to the other no (got 2nd no)
  s = a[k];
}

void getBigSmall(int f, int s, int &smaller, int &bigger){
  if(f > s){
    bigger = f;
    smaller = s;
  }
  else{
    bigger = s;
    smaller = f;
  }
}

void sort(int *a, int size){
  if(size == 0 || size == 1)
    return;
  else{
    int l = 0;
    int h = size - 1;

    // find the 2 distinct nos
    int f,s;
    get2Nos(a,size,f,s);

    // find which one is bigger/smaller
    int smaller, bigger;
    getBigSmall(f,s,smaller,bigger);

    // now traverse the loop again from the start now that you know the 2 nos
    while(l < h){
      while(a[l] == smaller && l < h)
        l++;

      while(a[h] == bigger && l < h)
        h--;

      if(l < h){
        swap(a[l],a[h]);
        l++;
        h--;
      }
    }
  }
}

void printArray(int *a, int s){
  if(s == 0)
    return;
  else{
    for(int i = 0; i < s; i++)
      cout << a[i] << '\t';
    cout << endl;
  }
}

int main(){
  int a[] = {4,4,7,7,4,7,7,4,7,4,7,7};
  int s = 12;

  printArray(a,12);

  sort(a,12);

  printArray(a,12);

  return 0;
}