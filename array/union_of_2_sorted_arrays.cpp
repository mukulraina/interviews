#include <iostream>
#include <vector>
using namespace std;

void printVector(vector<int> result){
  for(int i = 0; i < result.size(); i++)
    cout << result[i] << '\t';
  cout << endl;
}

void getUnion(vector<int> a, vector<int> b){
  if(a.size() == 0 || b.size() == 0)
    return;

  vector<int> result;

  int i = 0;
  int j = 0;

  // push 1 element for now, so that we dont have to check 3 times that
  // its empty or not
  result.push_back(a[i]);

  while(i < a.size() && j < b.size()){
    int lastIndex = result.size() - 1;

    if(a[i] < b[j]){
      if(a[i] != result[lastIndex])
        result.push_back(a[i]);

      i++;
    }
    else if(b[j] < a[i]){
      if(b[j] != result[lastIndex])
        result.push_back(b[j]);

      j++;
    }
    else{
      if(a[i] != result[lastIndex])
        result.push_back(a[i]);

      i++;
      j++;
    }
  }

  // if any of those 2 arrays is still left, just push it to the union
  // but still check if the elements are not there already
  while(i < a.size()){
    int lastIndex = result.size() - 1;

    if(a[i] != result[lastIndex])
        result.push_back(a[i]);

    i++;
  }

  while(j < b.size()){
    int lastIndex = result.size() - 1;

    if(b[j] != result[lastIndex])
        result.push_back(b[j]);

    j++;
  }

  //print the result
  printVector(result);
}

int main(){
  vector<int> a;
  vector<int> b;

  a.push_back(1);
  a.push_back(1);
  a.push_back(3);
  a.push_back(4);
  a.push_back(5);
  a.push_back(6);
  a.push_back(7);
  a.push_back(8);

  b.push_back(1);
  b.push_back(1);
  b.push_back(1);
  b.push_back(2);
  b.push_back(2);
  b.push_back(3);
  b.push_back(5);
  b.push_back(6);
  b.push_back(8);


  getUnion(a,b);

  return 0;
}
