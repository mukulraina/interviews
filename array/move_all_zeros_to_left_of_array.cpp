#include <iostream>
using namespace std;

void pushZerosToLeft(int *a, int size){
  if(size == 0 || size == 1)
    return;
  
  int i = size - 1;
  int j = size - 1;

  while(i >= 0){
    if(a[i] != 0){
      a[j] = a[i];
      j--;
    }
    i--;
  }

  while(j >=0){
    a[j] = 0;
    j--;
  }
}

void printArray(int *a, int size){
  for(int i = 0; i < size; i++)
    cout << a[i] << '\t';
  cout << endl;
}

int main(){
  int a[] = {5,0,2,0,0,8,1,0,3,9};
  int size = 10;

  printArray(a,size);

  pushZerosToLeft(a,size);
  
  printArray(a,size);

  return 0;
}