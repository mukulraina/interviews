#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

int getSum(int n){
  int sum = 0;
  while(n > 0){
    int digit = n % 10;
    sum += digit * digit;
    n = n / 10;
  }
  return sum;
}

bool isHappyHelper(int n, Mymap &hash){
  // get the sum of square of digits
  int sum = getSum(n);

  if(sum == 1)
    return true;

  // Check if the sum has appeared in the hash, if not then insert it
  Mymap::iterator iter;
  iter = hash.find(sum);
  if(iter != hash.end())
    return false;
  else{
    hash.insert(Mymap::value_type(sum,0));
    return isHappyHelper(sum,hash); 
  }
}

bool isHappy(int n){
  Mymap hash;
  return isHappyHelper(n,hash);
}

int main(){
  for(int i = 1; i < 50; i++){
    if(isHappy(i))
      cout << i << endl;
  }
  return 0;
}