int firstOccurence(int *a, int l, int h, int x){
	if(l > h)
		return -1;
	else{
		// get mid
		int m = (l + h) / 2;

		if((a[m] == x) && (m == 0 || a[m - 1] < x)
			return m;
		else if(x < a[m])
			return firstOccurence(a,0,m-1,x);
		else
			return firstOccurence(a,m+1,h,x);
	}
}

int lastOccurence(int *a, int l, int h, int x, int size){
	if(l > h)
		return -1;
	else{
		int m = (l + h) / 2;

		// need to have a size here (not h cos h changes with each recursive call)
		if((a[m] == x) && ((m == size - 1) || a[m + 1] > x)
			return m;
		else if(x < a[m])
			return lastOccurence(a,0,m-1,x,size);
		else
			return lastOccurence(a,m+1,h,x,size);
	}
}

int noOfOccurences(int *a, int x, int size){
	// get the first occurence
	int f = firstOccurence(a,0,size - 1,x);

	if(f == -1)
		return -1;

	// get the last occurence
	int s = lastOccurence(a,0,size-1,x,size);

	// return the difference
	return (j - i + 1);
}