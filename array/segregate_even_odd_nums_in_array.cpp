#include <iostream>
#include <queue>
using namespace std;

bool isOdd(int a){
	return ((a % 2) == 1);
}

bool isEven(int a){
	return ((a % 2) == 0);
}

void segregateEvenOddElements(int *a, int size){
	if(size == 0 || size == 1)
		return;

	queue<int> evenQ;
	queue<int> oddQ;

	// divide array into even odd elements
	for(int i = 0; i < size; i++){
		if(isEven(a[i]))
			evenQ.push(a[i]);
		else
			oddQ.push(a[i]);
	}

	int j = 0;

	// overwrite the original array
	while(evenQ.empty() != true){
		a[j] = evenQ.front();
		evenQ.pop();
		j++;
	}

	while(oddQ.empty() != true){
		a[j] = oddQ.front();
		oddQ.pop();
		j++;
	}
}

void printArray(int *a, int size){
	for(int i = 0; i < size; i++)
		cout << a[i] << '\t';

	cout << endl;
}

int main(){
	int a[] = {3,2,4,1,9,7,5,10,1};
	int size = 9;

	printArray(a,size);

	segregateEvenOddElements(a,size);

	printArray(a,size);
	return 0;
}