#include <iostream>
using namespace std;

void printArray(int *a, int size){
	if(size == 0)
		return;
	else{
		for(int i = 0; i < size; i++)
			cout << a[i] << '\t';
		cout << endl;
	}
}

// space will be O(n) due to the call stack
// instea of O(1) as in the iterative solution
void reverseArray(int *a, int l, int h){
	if(l < h){
		swap(a[l],a[h]);
		reverseArray(a,l+1,h-1);
	}
}

int main(){
	int a[] = {1,2,3,4,5};
	
	printArray(a,5);
	reverseArray(a,0,4);
	printArray(a,5);
	
	return 0;
}