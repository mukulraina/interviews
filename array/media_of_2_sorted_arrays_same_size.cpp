// http://www.programcreek.com/2012/12/leetcode-median-of-two-sorted-arrays-java/
// http://leetcode.com/2011/03/median-of-two-sorted-arrays.html
// http://leetcode.com/2011/01/find-k-th-smallest-element-in-union-of.html
// https://www.youtube.com/watch?v=_H50Ir-Tves
#include <iostream>
using namespace std;

int median(int a[], int n){
  if(n % 2 == 0)
    return (a[n/2] + a[n+1]) / 2;
  else
    return a[n/2];
}

int getMax(int x, int y){
    return x > y? x : y;
}
 
int getMin(int x, int y){
    return x > y? y : x;
}

// Time: O(log n)
int getMedian(int a[], int b[], int n){
  // empty array
  if(n <= 0)
    return -1;

  // 1 element array
  else if(n == 1)
    return (a[0] + b[0]) / 2;

  // 2 elements in array
  else if(n == 2){
    int max = getMax(a[0],b[0]);
    int min = getMin(a[1],b[1]);
    return (max + min) / 2;
  }

  else{
    int m1 = median(a,n);   // median of a
    int m2  = median(b,n);   // median of b

    if(m1 == m2) // If both medians are equal, then return anyone of them
      return m1;
    else if(m1 < m2){
      if(n % 2 == 0)
        return getMedian(a + n/2 - 1, b, n - n/2 + 1);
      else
        return getMedian(a + n/2, b, n - n/2);
    }
    else{
      if(n % 2 == 0)
        return getMedian(b + n/2 - 1, a, n - n/2 + 1);
      else
        return getMedian(b + n/2, a, n - n/2);
    }
  } 
}

int main(){
  int a[] = {1, 2, 3, 6};
  int b[] = {4, 6, 8, 10};
  int size = 4;
  cout << getMedian(a,b,size);
  cout << endl;
  return 0;
}
