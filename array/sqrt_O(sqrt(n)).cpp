#include <iostream>
using namespace std;

// return sqrt of a number if its a perfect square
// Time: O(sqrt(n))
int sqrt(int v){
  int i = 1;
  int counter = 0;

  while(v > 0){
    v = v - i;
    i = i + 2;

    counter++;

    if(v == 0)
      return counter;
  }
  return -1;
}

int main(){
  int v1 = 25;
  int v2 = 36;
  int v3 = 18;

  cout << sqrt(v1) << endl;
  cout << sqrt(v2) << endl;
  cout << sqrt(v3) << endl;

  return 0;
}