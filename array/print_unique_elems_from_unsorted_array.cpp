#include <iostream>
#include <unordered_map>
using namespace std;

typedef unordered_map<int,int> Mymap;

void printUnique(int *a, int size){
  if(size == 0)
    return;
  else if(size == 1)
    cout << a[0] << endl;
  else{
    // Declare a hash
    Mymap m;
    Mymap::iterator iter;

    // Traverse the array + check/insert hash
    for(int i = 0; i < size; i++){
      iter = m.find(a[i]);

      // if its not present in the hash
      if(iter == m.end()){
        cout << a[i] << '\t';
        m.insert(Mymap::value_type(a[i],1));
      }
    }

    cout << endl;
  }
}

int main(){
  int a[] = {1,2,2,3,4,5,5,6,7,7,8,9};
  int size = 12;

  printUnique(a,size);

  return 0;
}
