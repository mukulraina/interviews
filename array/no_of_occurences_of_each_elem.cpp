// print the # of occurences of each number in the array
#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

void noOfOccur(int *a, int size){
  if(size == 0)
    return;

  else if(size == 1)
    cout << a[0] << "\t" << 1 << endl;

  else{
    // Declare a hash
    Mymap m;
    Mymap::iterator iter;

    // Put values in the hash, if already increase their count
    for(int i = 0; i < size; i++){
      iter = m.find(a[i]);

      // not in hash
      if(iter == m.end())
        // insert it
        m.insert(Mymap::value_type(a[i],1));
      else
        // increase count
        iter->second++;
    }

    // Traverse the array again + print key value paits
    for(int i = 0; i < size; i++){
      iter = m.find(a[i]);

      if(iter->second != -1){
        cout << iter->first << '\t' << iter->second << endl;
        iter->second = -1;
      }
    }
  }
}

int main(){
  int a[] = {1,4,3,1,6,2,5,8,5,9,1};
  int size = 11;

  noOfOccur(a,size);

  return 0;
}