#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

// Still need to test for all cases
/*
void printCommonElementsHashApproach(int *a, int *b, int aSize, int bSize){
  if(aSize == 0 || bSize == 0)
    return;

  else{
    // Put a in hash
    Mymap m;
    Mymap::iterator iter;

    for(int i = 0; i < aSize; i++){
      iter = m.find(a[i]);

      if(iter == m.end())
        m.insert(Mymap::value_type(a[i],1));

    }

    // Traverse b + see if the elements are already in hash
    for(int i = 0; i < bSize; i++){
      iter = m.find(b[i]);

      if(iter != m.end())
        cout << b[i] << endl;
    }

  }
}
*/

// Still need to test for all cases
/*
void printCommonElementsInplaceApproach(int *a, int *b, int aSize, int bSize){
  if(aSize == 0 || bSize == 0)
    return;

  else{
    int i = 0;
    int j = 0;

    while(i < aSize && j < bSize){
      // Traverse the first array
      while(b[j] < a[i] && i < aSize){
        if(a[i] == b[j])
          cout << a[i] << endl;

        i++;
      }

      // To check if the first array ended
      if(i > aSize)
        break;

      // Traverse the 2nd array
      while(a[i] < b[j] && j < bSize){
        if(a[i] == b[j])
          cout << a[i] << endl;

        j++;
      }
    }
  }
}
*/

int main(){
  int a[] = {1,3,4,5,6,7};
  int b[] = {2,3,5,6};
  int aSize = 6;
  int bSize = 4;

  printCommonElementsInplaceApproach(a,b,aSize,bSize);

  return 0;
}