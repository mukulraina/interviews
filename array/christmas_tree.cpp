// Program to print a christmas tree of height h
#include <iostream>
using namespace std;

/*
 * A helper function to print a christmas tree of height h
 * @param sp  Number of spaces at each level 
 * @param e   Number of '*' at at each level
 * Time: O(n ^ 2) 
 */
void printChristmasTreeHelper(int sp, int e){
  if(sp >= 0){
    for(int i = 0; i < sp; i++){
      cout << ' ';
    }
    for(int i = 0; i < e; i++){
      cout << '*';
    }
    cout << endl;
    printChristmasTreeHelper(sp - 1,e + 2);
  }
}

/*
 * Function to print a christmas tree of height h
 * @param h   Height of the tree
 * @param e   Number of '*' in the tree at each level
 */
void printChristmasTree(int h){
  int e = 1;
  printChristmasTreeHelper(h-1,e);
}

/*
 * Client to print a christmas tree of height h
 */
int main(){
  int h;

  h = 0;
  cout << "Printing tree of height h" << h << endl;
  printChristmasTree(h);
  cout << endl;

  h = 1;
  cout << "Printing tree of height h" << h << endl;
  printChristmasTree(h);
  cout << endl;

  h = 2;
  cout << "Printing tree of height h" << h << endl;
  printChristmasTree(h);
  cout << endl;

  h = 5;
  cout << "Printing tree of height h" << h << endl;
  printChristmasTree(h);
  cout << endl;

  h = 10;
  cout << "Printing tree of height h" << h << endl;
  printChristmasTree(h);
  cout << endl;
  return 0;
}
