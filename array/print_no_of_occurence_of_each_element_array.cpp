void noOfOccur(int *a, int size){
  if(size == 0)
    return;
  else{
    // Declare a hash
    Mymap m;
    Mymap::iterator iter;

    // Put values in the hash, if already increase their count
    for(int i = 0; i < size; i++){
      iter = m.find(a[i]); // lookup

      if(iter == m.end()) // if not in hash
        m.insert(Mymap::value_type(a[i],1));         // insert it
      else // if already in hash
        iter->second++; // increase count
    }

    // Traverse the array again + print key value paits
    for(int i = 0; i < size; i++){
      iter = m.find(a[i]); //lookup

      if(iter->second != -1){
        cout << iter->first << '\t' << iter->second << endl;
        iter->second = -1;
      }
    }
  }
}