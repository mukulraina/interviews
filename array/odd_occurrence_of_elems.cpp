#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> myMap;

bool isOdd(int n){
    return (n % 2 == 1);
}

void getOddOccurrence(int *a, int size){
    if(size == 0)
        cout << "sorry empty array" << endl;

    else if(size == 1)
        cout << a[0] << endl;

    else{
        // Declare a hash
        myMap hash;

        // Declare an iterator to access the hash
        myMap::iterator iter;

        // Traverse the array
        for(int i = 0; i < size; i++){
            iter = hash.find(a[i]);

            // If the element is already present increase its counter
            if (iter == hash.end())
                hash.insert(myMap::value_type(a[i],1));
            
            // If its not, insert it + ->value to be 1
            else
                iter->second++;
        }

        // Traverse the array again + print the elememnts whose value is odd
        for(int i = 0; i < size; i++){
            iter = hash.find(a[i]);
            int noOfOccurrence = iter->second;
            if(isOdd(noOfOccurrence))
                cout << a[i] << endl;
        }
    }
}

int main()
{
    int a[] = {1,4,5,6,2,7,8,2,1,4};
    int b[] = {};
    int c[] = {1};
    
    getOddOccurrence(a, 10);
    cout<<'\n';
    
    getOddOccurrence(b,0);
    cout<<'\n';
    
    getOddOccurrence(c,1);
    cout<<'\n';
    
    return 0;
}
