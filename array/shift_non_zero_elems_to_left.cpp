#include <iostream>
using namespace std;

void pushZerosToEnd(int *a, int size){
  if(size == 0)
    return;
  else{
    int c = 0;

    // Traverse the array once
    for(int i = 0; i < size; i++){
      if(a[i] != 0){
        a[c] = a[i];
        c++;
      }      
    }

    // Mark all the remaining elements 0
    for(int i = c; i < size; i++)
      a[i] = 0;
  }
}

void printArray(int *a, int size){
  if(size == 0)
    return;
  else{
    for(int i = 0; i < size; i++){
    cout << a[i] << '\t';
    }
    cout << endl;
  }
}

int main(){
  int a[] = {5,0,2,0,0,8,1,0,3,9};
  int size = 10;

  cout << "array before shifting" << endl;
  printArray(a,size);

  cout << "shifting elements" << endl;
  pushZerosToEnd(a,size);
  cout << "..." << endl;

  cout << "arry after shifting" << endl;
  printArray(a,size);

  return 0;
}
