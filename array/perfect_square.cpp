#include <iostream>
using namespace std;

bool isPerfect(int v){
  int i = 1;

  while(v > 0){
    v = v - i;
    i = i + 2;

    if(v == 0)
      return true;
  }
  return false;
}

int main(){
  int v1 = 25;
  int v2 = 36;
  int v3 = 18;

  cout << isPerfect(v1) << endl;
  cout << isPerfect(v2) << endl;
  cout << isPerfect(v3) << endl;

  return 0;
}