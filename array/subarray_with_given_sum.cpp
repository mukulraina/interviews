#include <iostream>
using namespace std;

bool subArraySum(int *a, int size, int v){
  if(size == 0)
    return false;

  else if(size == 1){
    if(a[0] == v)
      return true;
    else
      return false;
  }

  else{
    int i = 0; 
    int j = 0;
    int sum = 0;

    // After this loop j points to 10
    while(j < size){
      sum += a[j];
      if(sum > v)
        break;

      j++;
    }

    if(j == size)
      return false;

    // Increment i
    while(i < size){
      sum -= a[i];

      if(sum == v){
        cout << "starting index: " << i << endl;
        cout << "ending index: " << j << endl;
        return true;
      }

      i++;
    }

    return false;
  }
}

int main(){
  int a[] = {1,4,20,3,10,5};
  int size = 6;

  cout << subArraySum(a,size,33) << endl;
  return 0;
}