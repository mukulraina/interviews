#include <iostream>
using namespace std;

// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    void insert(int v);
    void printInOrder(Node *node);
    int predecessor(int v);
    int getMax(Node *n);
    int getMin(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(root == NULL){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

int BST::getMax(Node *n){
  if(n->right == NULL)
    return n->value;
  else
    return getMax(n->right);
}

int BST::getMin(Node *n){
  if(n->left == NULL)
    return n->value;
  else
    return getMin(n->left);
}

int BST::predecessor(int v){
  // Case 1: empty tree
  if(root == NULL){
    cout << "sorry empty tree" << endl;
    return -1;
  }

  // Case 2: check if the element is in the tree or not
  Node *c = root;
  while(c != NULL){
    if(c->value == v)
      break;
    else if(v < c->value)
      c = c->left;
    else
      c = c->right;
  }

  // If c is null, the element was not found in the tree
  if(c == NULL){
    cout << "sorry the element was not found in the tree" << endl;
    return -1;
  }

  // Otherwise c is pointing to the element
  // if c is the smallest elment in the tree, it wont have any pred
  if(v == getMin(root)){
    cout << "sorry the smallest element in the tree doesnt have any pred" << endl;
    return -1;
  }

  // If it has left subtree, the biggest no of left subtree will be its pred
  if(c->left != NULL)
    return getMax(c->left);

  // Otherwise traverse from the top again and keep track of the pred
  Node *pred = NULL;
  c = root;
  while(c->value != v){
    if(v > c->value){
      pred = c;
      c = c->right;
    }
    else 
      c = c->left;
  }

  return pred->value;
}

int main(){
  BST *tree = new BST();
  tree->insert(10);
  tree->insert(8);
  tree->insert(14);
  tree->insert(6);
  tree->insert(9);
  tree->insert(12);
  tree->insert(15);
  tree->printInOrder(tree->root);
  cout << endl;
      
  std::cout<<"The predecessor of 8 is: "<<tree->predecessor(8)<<'\n';
  std::cout<<"The predecessor of 15 is: "<<tree->predecessor(15)<<'\n';
  std::cout<<"The predecessor of 12 is: "<<tree->predecessor(12)<<'\n';
  std::cout<<"The predecessor of 6 is: "<<tree->predecessor(6)<<'\n';
  std::cout<<"The predecessor of 99 is: "<<tree->predecessor(99)<<'\n';
  return 0;
}
