#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
  int getLevelIterative(Node *n,int v);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

int BST::getLevelIterative(Node *n, int v){
  // Case 1: empty tree
  if(n == NULL)
    return -1;

  // Case 2: 1 or more nodes
  else{
    // Find the node
    Node *c = n;
    int i = 0;
    while(c != NULL){
      if(c->value == v)
        break;
      i++;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(c == NULL){
      cout << "sorry this element is not in the tree" << endl;
      return -1;
    }
    else
      return i;
  }
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(6);
  tree->insert(15);
  tree->insert(3);
  tree->insert(8);
  tree->insert(2);
  tree->insert(5);
  tree->insert(7);
  tree->insert(1);


  tree->inOrder(tree->root);
  cout << endl;

  cout << tree->getLevelIterative(tree->root,99) << endl;
  return 0;
};