#include <iostream>
using namespace std;

// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    void insert(int v);
    void printInOrder(Node *node);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(root == NULL){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

Node* search(Node *n, int v){
  if(n == NULL) // base case
    return NULL;
  else if(n->value == v)
    return n;
  else{
    // check left subtree
    Node* left = search(n->left,v);

    // if found it in left, return that 
    // otherwise go in right
    if(left != NULL)
      return left;

    // check right
    Node* right = search(n->right,v);

    return right; // will return right regardless you found or not (cos no other option)
  }
}

int main(){
  BST *tree = new BST();
  tree->insert(8);
  tree->insert(5);
  tree->insert(10);
  tree->insert(4);
  tree->insert(7);
  tree->insert(9);
  tree->insert(14);
  tree->insert(3);
  tree->printInOrder(tree->root);
  cout << endl;
  
  Node* t = search(tree->root,7);
  if(t == NULL)
    cout << "not found" << endl;
  else 
    cout << "found: " << t->value << endl;


  return 0;
}
