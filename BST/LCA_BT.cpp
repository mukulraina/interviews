#include <iostream>
#include <vector>
using namespace std;

// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int v);
    void printInOrder(Node *node);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

bool isDescendent(Node *n, int v){
  if(n == NULL)
    return false;
  else if(n->value == v)
    return true;
  else 
    return (isDescendent(n->left,v) || isDescendent(n->right,v));
}

int LCAhelper(Node *n, int p, int q){
  if(n == NULL)
    return -1;

  else if(n->value == p || n->value == q)
    return n->value;

  else{
    bool p_on_left = isDescendent(n->left,p);
    bool q_on_left = isDescendent(n->left,q);

    // both on different sides
    if(p_on_left != q_on_left)
      return n->value;
    
    // both on same sides
    else{
      // both of them are on left side
      if(p_on_left == true)
        return LCAhelper(n->left,p,q);
      else
        // both on right sides
        return LCAhelper(n->right,p,q);
    }
  }
}

int LCA(Node *n, int a, int b){
  if(isDescendent(n,a) == false || isDescendent(n,b) == false)
    return -1;
  else
    return LCAhelper(n,a,b);
}

int main(){
  BST *tree = new BST();
  tree->insert(5);
  tree->insert(3);
  tree->insert(7);
  tree->insert(1);
  tree->insert(4);
  tree->insert(6);
  tree->insert(10);
  tree->insert(8);

  tree->printInOrder(tree->root);
  cout << endl;
  
  cout << LCA(tree->root,6,8);

  cout << endl;
  return 0;
}
