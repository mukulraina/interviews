#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void deleteTree(Node *n){
  if(n == NULL)
    return; // base case
  else{
    // return left and right subtree first
    deleteTree(n->left);
    deleteTree(n->right);

    // now delete the node itself
    // make sure you set the root = NULL in the main()
    delete(n);
  }
}

// Time: O(n)
Node * getPointerToNode(Node *n, int v){
  // base case
  if(n == NULL)
    return NULL;
  else{
    // check the current node
    if(n->value == v)
      return n;

    // check if the value was found in the left subtree
    Node *tempLeft = getPointerToNode(n->left, v);

    // if temp wasn't NULL, that means the pointer was found + so return from here only
    if(tempLeft != NULL)
      return tempLeft;

    // now if we reached here that means the node wasn't found in the left subtree, so check the right one
    Node *tempRight = getPointerToNode(n->right, v);

    // if its NULL in right too, then no option, just return NULL (not found anywhere in the tree)
    if(tempRight == NULL)
      return NULL;  
    else
      return tempRight;
  }
}

int main(){
  BST *tree = new BST();

  tree->insert(20);
  tree->insert(25);
  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(12);
  tree->insert(18);
  tree->insert(19);
  tree->insert(3);
  tree->insert(6);

  int v = 15;
  Node *t = getPointerToNode(tree->root, v);
  if(t == NULL)
    cout << "sorry not found" << endl;
  else
    cout << t->value << endl;

  // always delete the tree
  deleteTree(tree->root);
  tree->root = NULL;
  return 0;
};