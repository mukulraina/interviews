#include <iostream>
#include <vector>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void binaryTreeToVector(Node *n, vector<int> &v){
  // Do any sort of traversal + push it in vector
  if(n == NULL)
    return;
  else{
    binaryTreeToVector(n->left,v);
    v.push_back(n->value);
    binaryTreeToVector(n->right,v);
  }
}

void printVector(std::vector<int> v){
  for(int i = 0; i < v.size(); i++){
    cout << v[i] << '\t';
  }
  cout << endl;
}

int main(){
  BST *tree = new BST();

  tree->insert(15);
  tree->insert(10);
  tree->insert(20);
  tree->insert(7);
  tree->insert(12);
  tree->insert(17);
  tree->insert(25);

  tree->inOrder(tree->root);
  cout << endl;

  // Manually messing up with the BST to 
  // convert it into BT (by messing up with the BST property)
  tree->root->value = 1;
  tree->root->left->value = 2;
  tree->root->right->value = 3;
  tree->root->left->left->value = 4;
  tree->root->left->right->value = 5;
  tree->root->right->value = 3;
  tree->root->right->left->value = 6;
  tree->root->right->right->value = 1;

  tree->inOrder(tree->root);

  // Now we just want to convert this binary tree to a vector
  vector<int> v;
  binaryTreeToVector(tree->root,v);
  cout << endl;
  cout << "printing the vector that was made from binary tree" << endl;
  printVector(v);
  return 0;
};
