#include <iostream>
#include <list>
#include <vector>
using namespace std;

// Node
class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
  Node();
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void inOrderTraversal(Node *node);
  void insert(int v);
};

void BST::insert(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: empty tree
  if(!root){
    root = newNode;
    return;
  }

  // Case 2: Normal case
  // Traverse through the tree + find the parent
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;

    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

BST::BST(){
  root = NULL;
}

void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << '\t';
    inOrderTraversal(node->right);
  }
}

// NOT WORKING YET
void putLevelNodes(Node *n, vector<list<int> > &VOL, int level){
  // When they reach the base case, (no more nodes left), dont do anything, just return
  if(n == NULL)
    return;
  else{
    VOL[level].push_front(n->value);

    // left subtree
    putLevelNodes(n->left,VOL,level + 1);

    // right subtree
    putLevelNodes(n->right,VOL,level + 1);
  }
}
// NOT WORKING YET
void putSameLevelNodesInLL(Node *n){
  if(n == NULL)
    return;

  // Make a vector of linked lists
  vector<list<int> > VOL;

  // Put all the nodes of the same level in LL
  putLevelNodes(n,VOL,0);

  
  // Print them
  for(int i = 0; i < VOL.size(); i++){
    // Print the LL at index i
    while(VOL[i].empty() != true){
      int v = VOL[i].front();
      cout << v << '\t';
      VOL[i].pop_front();
    }
    cout << endl;
  }
  
}

int main(){
  int a[] = {15,10,18,5,12,17,20};
  int size = 7;

  BST *tree = new BST();
  
  for(int i = 0; i < size; i++)
    tree->insert(a[i]);

  tree->inOrderTraversal(tree->root);
  cout << endl;

  putSameLevelNodesInLL(tree->root);

  return 0;
}

























































