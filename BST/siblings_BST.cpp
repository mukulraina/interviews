#include <iostream>
using namespace std;


// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
    Node *nextRight;
};
Node::Node(){
  left = right = nextRight = NULL;
}
Node::Node(int v){
  value = v;
  left = right = nextRight = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int v);
    void printInOrder(Node *node);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

// recursive search is time: O(n) but space O(n) (so iterative is better)
Node *getNode(Node *n, int a){
  if(n == NULL)
    return NULL;
  else if(n->value == a)
    return n;
  else if(n->value < a)
    return getNode(n->right,a);
  else
    return getNode(n->left,a);
}

// got seg fault cos I was trying to compare n->left->value (seg fault when we tried getting NULL->value)
bool areSiblings(Node *n, Node *a, Node* b){
  if(n == NULL)
    return false;
  else
    return ((n->left == a && n->right == b) || (n->left == b && n->right == a) || areSiblings(n->left,a,b) || areSiblings(n->right,a,b));
}

int main(){
  BST *tree = new BST();
  tree->insert(8);
  tree->insert(5);
  tree->insert(10);
  tree->insert(4);
  tree->insert(7);
  tree->insert(9);
  tree->insert(14);
  tree->insert(3);

  int a = 14;
  int b = 9;
  cout << areSiblings(tree->root,getNode(tree->root,a),getNode(tree->root,b)) << endl;

  return 0;
}
