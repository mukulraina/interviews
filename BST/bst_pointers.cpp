#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  void insert(Node **r, int v);
  void insert2(Node *r, int v);
  void inorder(Node *n);
  BST();
};
BST::BST(){
  root = NULL;
}

void BST::insert(Node ** r,int v){
  cout << "address: "<< &r <<"\tvalue: " << (*r)->value << endl;
}

void BST::insert2(Node *r,int v){
  cout << "address: "<< &r <<"\tvalue: " << r << endl;
}


int main(){
  BST *bst = new BST();
  Node *newNode = new Node(5);
  bst->root = newNode;
  bst->insert(&bst->root,1);
  bst->insert2(bst->root,1);
  return 0;
}