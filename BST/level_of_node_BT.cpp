#include <iostream>
using namespace std;

// Node
class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
  Node();
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void inOrderTraversal(Node *node);
  void insert(int v);
};

void BST::insert(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: empty tree
  if(!root){
    root = newNode;
    return;
  }

  // Case 2: Normal case
  // Traverse through the tree + find the parent
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;

    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

BST::BST(){
  root = NULL;
}

void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << '\t';
    inOrderTraversal(node->right);
  }
}

int helper(Node *n, int v, int l){
  // if you reach NULL that means the value wasn't found (atleast in that subtree) 
  if(n == NULL)
    return 0;

  // check the root, see if it has the value
  if(n->value == v)
    return l;

  // check for the value in the left subtree
  int leftLevel = helper(n->left,v,l + 1);
  // if the value was found in the left subtree (i.e. leftLevel is NOT 0 - return the level)
  if(leftLevel != 0) // that means value was found in the left subtree
    return leftLevel;

  // now we get here only if the value was not in the root + value was not in the left subtree
  // check in the right subtree
  int rightLevel = helper(n->right,v,l + 1);
  return rightLevel; // return rightLevel cos if its not find in the right now its not in the tree 
                    // cos we checked earlier that it wasn't in the left subtree
                    // so either its gonna be 0 (if it hits NULL) or 'l' where value was found
}

int getLevel(Node *n, int v){
  int l = 1;
  return helper(n,v,l);
}
int main(){
  int a[] = {15,10,18,5,12,17,20};
  int size = 7;

  BST *tree = new BST();
  
  for(int i = 0; i < size; i++)
    tree->insert(a[i]);

  tree->inOrderTraversal(tree->root);
  cout << endl;

  int v = 15;
  cout << "level: " << getLevel(tree->root,v) << endl;

  return 0;
}