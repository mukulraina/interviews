#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;


// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
    Node *nextRight;
};
Node::Node(){
  left = right = nextRight = NULL;
}
Node::Node(int v){
  value = v;
  left = right = nextRight = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int v);
    void printInOrder(Node *node);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

// Time: O(1)
int max(int &a, int &b){
  if(a > b)
    return a;
  return b;
}

// Time: O(n)
int getNumberOfLevels(Node *n){
  if(n == NULL)
    return 0;

  int l = getNumberOfLevels(n->left);
  int r = getNumberOfLevels(n->right);

  return (max(l,r) + 1);
}

// using hash approach - populates hash as: levelNumber->NumberOfNodesInLevel
// Time: O(n)
void helper(Node *n, int level, Mymap &m){
  if(n != NULL){
    helper(n->left,level + 1,m);

      // check if the level no (e.g. 1 or 2) is in the tree or not
      Mymap::iterator iter;
      iter = m.find(level);

      // if it's not, insert it
      if(iter == m.end())
        m.insert(Mymap::value_type(level,1));
      // if it is, increase the no of nodes in it (which is the value of the key-value pair)
      else
        iter->second++;
    
    helper(n->right,level + 1,m);
  }
}

//Time:O(n) space:O(n)
int levelWithMaxNodes(Node *n){
  int level = 0;
  Mymap m;
  Mymap::iterator iter;

  // Time:O(n)
  helper(n,level,m);

  // Time:O(n)
  int numOfLevels = getNumberOfLevels(n);

  // lookup the level from hash
  iter = m.find(0);
  int maxLevel = 0; //iter->first
  int maxNodes = iter->second; // initially the max stores the no of nodes in level 0

  // Time:O(n)
  for(int i = 0; i < numOfLevels; i++){
    iter = m.find(i); // lookup a level no in hash
    // compare its no of nodes with the max
    if(maxNodes < iter->second){
      maxLevel = iter->first;
      maxNodes = iter->second;
    }
  }

  return maxLevel;
}

int main(){
  BST *tree = new BST();
  tree->insert(8);
  tree->insert(5);
  tree->insert(10);
  tree->insert(4);
  tree->insert(7);
  tree->insert(9);
  tree->insert(14);

  tree->insert(3);
  tree->printInOrder(tree->root);
  cout << endl;

  cout << levelWithMaxNodes(tree->root);
  cout << endl;
  return 0;
}
