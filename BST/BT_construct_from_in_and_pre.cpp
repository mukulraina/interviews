#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<char,int> Mymap;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void inOrderTraversal(Node *n){
  if(n == NULL){
    cout << "empty";
    return;
  }
  else{
    inOrderTraversal(n->left);
    cout << n->value << '\t';
    inOrderTraversal(n->right);
  }
}

void deleteTree(Node *n){
  if(n == NULL)
    return; // base case
  else{
    // return left and right subtree first
    deleteTree(n->left);
    deleteTree(n->right);

    // now delete the node itself
    // make sure you set the root = NULL in the main()
    delete(n);
  }
}

/*
  So we bascially used the hash to lookup the corresponding inorder values cos now
  the time taken to search the index of a char is essentially O(1) instead of O(n)

  so now we just keep looking up + then traversing the preOrder array, which takes O(n)
  + keep making a node (which takes O(1)) and return the root

  So we do the following step n times:
    - move in preOrder array
    - make a node 

  So essentially the time complexitiy of the whole thing boils down to O(n)
*/
Node* helper(char in[], char pre[], int inStart, int inEnd, Mymap &hash){
  if(inStart > inEnd){
    return NULL;
  }
  else{
  // declare static so that the changes we made to preIndex are reflected
  static int preIndex = 0;

  // Make a node with the root gotten from preOrder
  Node *root = new Node(pre[preIndex]);
  preIndex++; // increment the preIndex as well

  // look up the position of the root from inOrder - O(1)
  Mymap::iterator it = hash.find(root->value);
  int inOrderIndex = it->second;

  // call it on right and left subtrees
  root->left = helper(in,pre,inStart,inOrderIndex - 1,hash);
  root->right = helper(in,pre,inOrderIndex + 1,inEnd,hash);

  return root;
  }
}

/*
  So we make a hash and put all the inorder values + with their corresponding indicies in it
  That takes O(n)

  Then we call the helper function
*/
Node *buildTree(char in[], char pre[], int inStart, int inEnd){
  // declare a hash for inOrder
  Mymap hash;
  Mymap::iterator it;

  // put all elements of inOrder in the hash
  int i = 0; 

  // Time: O(n)
  // hash would be <character, index of char in inOrder>
  while(i <= inEnd){
    hash.insert(Mymap::value_type(in[i],i));
  }

  return helper(in,pre,inStart,inEnd,hash);
}

int main(){
  char in[] = {'D', 'B', 'E', 'A', 'F', 'C'};
  char pre[] = {'A', 'B', 'D', 'E', 'C', 'F'};

  int inStart = 0;
  int inEnd = 5;

  Node *root = buildTree(in,pre,inStart,inEnd);

  inOrderTraversal(root);

  // always delete the tree
  deleteTree(root);
  root = NULL;
  return 0;
};