#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = NULL;
  right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  // Make a node
  Node *newNode = new Node(v);

  // Case 1: empty tree
  if(root == NULL)
    root = newNode;

  // Case 2: 1 or more node
  else{
    // Traverse and find parent
    Node *c = root;
    Node *p = NULL;
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    // Now p points to the parent
    if(v < p->value)
      p->left = newNode;
    else
      p->right = newNode;
  } 
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;

  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void deleteTree(Node *n){
  if(n == NULL)
    return; // base case
  else{
    // return left and right subtree first
    deleteTree(n->left);
    deleteTree(n->right);

    // now delete the node itself
    // make sure you set the root = NULL in the main()
    delete(n);
  }
}

void BST::deleteNode(Node *node, int n){
  /* Empty Tree */
  if(node == NULL){
    std::cout<<"Sorry empty tree"<<'\n';
    return ;
  }

  Node *current = node;
  Node *succ;
  Node *prev;
  int flag = 0, deletionCase;

  /* Find the location of the node to be deleted */
  while(current != NULL && current->getValue() != n){
    if(n < current->getValue()){
      prev = current;
      current = current->getLeft();
    }
    else{
      prev = current;
      current = current->getRight();
    }
  }

  if(current == NULL){
    std::cout<<"Sorry the value you want to delete is not in the tree"<<'\n';
    return ;
  }

  /* Find out the case of deletion */
  if(current->getLeft() == NULL && current->getRight() == NULL)
    deletionCase = 1; //Node has no children
  else if(current->getLeft() != NULL && current->getRight() != NULL)
    deletionCase = 3; //Node has 2 children
  else
    deletionCase = 2; //Node only has 1 child
  
  /* Deletion case 1: Node has no children */
  if(deletionCase == 1){
    if(prev->getLeft() == current) //If the node is a left child
      prev->setLeft(NULL);
    else
      prev->setRight(NULL);
    delete(current);
  }
  /* Deletion case 2: Node only has 1 child */
  if(deletionCase == 2){
    if(prev->getLeft() == current){
      // check whether the single child is on right or left
      if(current->getLeft() != NULL)
        prev->setLeft(current->getLeft());
      else
        prev->setLeft(current->getRight());
    }
    else{
      // check whether the single child is on right or left
      if(current->getLeft() != NULL)
        prev->setLeft(current->getLeft());
      else
        prev->setLeft(current->getRight());
    }
    delete(current);
  }
  /* Deletion case 3: Node has 2 children */
  if(deletionCase == 3){
    int successorValue = successor(node,current->getValue());
    deleteNode(node,successorValue);
    current->setValue(successorValue);
  }
}


int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(3);
  tree->insert(6);
  tree->insert(11);

  tree->inOrder(tree->root);
  cout << endl;

  // always delete the tree
  deleteTree(tree->root);
  tree->root = NULL;
  return 0;
};