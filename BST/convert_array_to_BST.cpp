#include <iostream>
#include <vector>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void binaryTreeToVector(Node *n, vector<int> &v){
  // Do any sort of traversal + push it in vector
  if(n == NULL)
    return;
  else{
    binaryTreeToVector(n->left,v);
    v.push_back(n->value);
    binaryTreeToVector(n->right,v);
  }
}

void printVector(std::vector<int> v){
  for(int i = 0; i < v.size(); i++){
    cout << v[i] << '\t';
  }
  cout << endl;
}

int main(){
  BST *tree = new BST();
  int a[] = {15,10,20,7,12,17,25};
  for(int i = 0; i < 7; i++){
    tree->insert(a[i]);  
  }
  tree->inOrder(tree->root);
  cout << endl;
  return 0;
};
