// Implemen t a function to check if a binary tree is a binary search tree.
// Approach 2: inOrder traversal without queue (just use 1 variable)
#include <iostream>
#include <vector>
#include <limits.h>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrderTraversal(Node *node);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: empty tree
  if(!root){
    root = newNode;
    return;
  }

  // Case 2: Normal case
  // Traverse through the tree + find the parent
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;

    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << '\t';
    inOrderTraversal(node->right);
  }
}

void isBSTHelper(Node *n, vector<int> &v){
  if(n != NULL){
    isBSTHelper(n->left,v);
    v.push_back(n->value);
    isBSTHelper(n->right,v);
  }
}

bool isSorted(vector<int> &v){
  if(v.size() == 0)
    return false;
  else if(v.size() == 1)
    return true;
  else{
    for(int i = 0; i < v.size() - 1; i++){
      if(v[i] > v[i + 1])
        return false;
    }
    return true;
  }
}

bool isBST(Node *n){
  if(n == NULL)
    return false;
  else if(n->left == NULL && n->right == NULL)
    return true;
  else{
    vector<int> v;  
    isBSTHelper(n,v);
    return isSorted(v);
  }
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(3);
  tree->insert(6);
  tree->insert(11);

  tree->root->right->left->value = 16;

  tree->inOrderTraversal(tree->root);
  cout << endl;

  cout << isBST(tree->root);
  cout << endl;

  return 0;
};