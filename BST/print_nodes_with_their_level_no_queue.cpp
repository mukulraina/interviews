#include <iostream>
#include <queue>
using namespace std;

// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int v);
    void printInOrder(Node *node);
    void level(Node *n);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

class levelNode{
  public:
    Node *n;
    int level;
    levelNode(Node *node, int l);
};
levelNode::levelNode(Node *node, int l){
  n = node;
  level = l;
}

void BST::level(Node *n){
  if(n == NULL)
    return;
  else{
    // declare a queue of levelNode objects
    queue<levelNode *> q1;
    queue<levelNode *> q2;

    int l = 0;
    //push root on the queue1 + queue2
    levelNode *LNode = new levelNode(n,l);
    q1.push(LNode);
    q2.push(LNode);

    //traverese queue1
    while(q1.empty() != true){
      // pop a node
      levelNode *t = q1.front();
      q1.pop();
      int levelFromNode = t->level;

      if(t->n->left != NULL){
        levelNode *tempLNode = new levelNode(t->n->left,levelFromNode + 1);
        q1.push(tempLNode);
        q2.push(tempLNode);
      }

      if(t->n->right != NULL){
        levelNode *tempLNode = new levelNode(t->n->right,levelFromNode + 1);
        q1.push(tempLNode);
        q2.push(tempLNode);
      }
    }

    //print queue
    while(q2.empty() != true){
      levelNode *t = q2.front();
      q2.pop();
      cout << t->n->value << ":" << t->level << endl;
    }
  }
}

int main(){
  BST *tree = new BST();
  tree->insert(8);
  tree->insert(5);
  tree->insert(10);
  tree->insert(4);
  tree->insert(7);
  tree->insert(9);
  tree->insert(14);

  tree->insert(3);
  tree->printInOrder(tree->root);
  cout << endl;
  
  tree->level(tree->root);
  cout << endl;
  return 0;
}
