#include <iostream>
#include <vector>
using namespace std;

// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int v);
    void printInOrder(Node *node);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

void printVector(vector<int> v){
  for(int i = 0; i < v.size(); i++)
    cout << v[i] << '\t';
  cout << endl;
}

bool getPath(Node *n, int v, vector<int> &path){
  if(n == NULL)
    return false;
  else{
    Node *c = n;

    // Traverse 
    while(c != NULL){
      if(v < c->value){
        path.push_back(c->value);
        c = c->left;
      }
      else if(v > c->value){
        path.push_back(c->value);
        c = c->right;
      }
      else
        break;
    }

    // If c is NULL, that means it reached the end of the loop 
    // That means the value is NOT in the tree
    if(c == NULL)
      return false;
    else 
      return true;
  }
}

int getDivPoint(vector<int> pA, vector<int> pB){


  if(pA.size() == 0 || pB.size() == 0)
    return -1;
  else{
    // Check to see if they have atleast common ancestor
    if(pA[0] != pB[0])
      return -1;

    else{
      int i = 0;
      int j = 0;

      // Keep traversing as long as there are the same values + array doesn't end
      while((pA[i] == pB[j]) && i < pA.size() && j < pB.size()){
        i++;
        j++;
      }

      i--;
      return pA[i];
    }
  }
}

int LCA(Node *n, int a, int b){
  if(n == NULL)
    return -1;
  else if(n->left == NULL && n->right == NULL)
    return -1;
  else{
    // Make 2 vectors to store the path of these nodes (if they exist)
    vector<int> pA;
    vector<int> pB;

    // Check if there are in tree, if they are populate the vectors
    if(getPath(n,a,pA) == false)
      return -1;

    if(getPath(n,b,pB) == false)
      return -1;
  
    // Now we know both the nodes are in the tree + we have both of their paths
    // Now we return the divergin point of these 2 paths/ vectors
    return getDivPoint(pA,pB);
  }
}

int main(){
  BST *tree = new BST();
  tree->insert(5);
  tree->insert(3);
  tree->insert(7);
  tree->insert(1);
  tree->insert(4);
  tree->insert(6);
  tree->insert(10);
  tree->insert(8);

  tree->printInOrder(tree->root);
  cout << endl;
  
  cout << LCA(tree->root,6,8);

  cout << endl;
  return 0;
}
