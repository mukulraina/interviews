#include <iostream>
#include <queue>
using namespace std;

// Node
class Node{
  public:
    char value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int v);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

int search(char a[], char c){
  int i = 0;
  while(a[i] != '\0')
    if(a[i] == c)
      return i;
  return -1;
}

Node* helper(char pre[], char in[], int &preIndex, int inStart, int inEnd){
  if(inStart > inEnd)
    return NULL;
  
  // get the most recent char from preChar
  char preChar = pre[preIndex];
  preIndex++;

  // Make a node
  Node *n = new Node(preChar);

  // check if only 1 element, return from here only
  if(inStart == inEnd)
    return n;

  // Get the location of the preChar in inorder traversal
  int locInorder = search(in,preChar);

  // recursively call to get left subtree
  // since preIndex is being used by reference, its value is already increased
  n->left = helper(pre,in,preIndex,inStart,locInorder - 1);
  n->right = helper(pre,in,preIndex,locInorder + 1, inEnd);

  return n;
}

Node* buildTree(char pre[], char in[]){
  int preIndex = 0;
  int inStart = 0;
  int inEnd = 5;
  return helper(pre,in,preIndex,inStart,inEnd);
}

int main(){
  char in[] = {'D', 'B', 'E', 'A', 'F', 'C'};
  char pre[] = {'A', 'B', 'D', 'E', 'C', 'F'};

  Node *n = buildTree(pre,in);


  return 0;
}
