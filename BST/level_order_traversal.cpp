#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
  void levelOrderTraversal(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void BST::levelOrderTraversal(Node *n){
  // Case 1: 0 nodes in the tree
  if(n == NULL)
    cout << "sorry empty tree" << endl;

  // Case 2: 1 node in the tree
  else if(n->left == NULL && n->right == NULL)
    cout << n->value <<'\t';

  // Case 3: More than 1 node in the tree
  else{
    // Make a queue to store pointers to the tree nodes
    queue<Node *> q;

    // Put the root in the queue
    q.push(n);

    // Traverse the queue and keep putting the children onto it
    while(q.empty() != true){
      //take first element and print it
      Node *c = q.front();
      cout << c->value << '\t';
      q.pop();

      // Put the children of it onto the queue
      if(c->left != NULL)
        q.push(c->left);
      if(c->right != NULL)
        q.push(c->right);
    }
  }
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(3);
  tree->insert(6);
  tree->insert(11);


  tree->inOrder(tree->root);
  cout << endl;
  tree->levelOrderTraversal(tree->root);
  cout << endl;
  return 0;
};