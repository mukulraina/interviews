#include <iostream>
using namespace std;

// Node
class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
  Node();
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void inOrderTraversal(Node *node);
  Node *makeTree(int *a, int l, int h);
};

BST::BST(){
  root = NULL;
}

void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << '\t';
    inOrderTraversal(node->right);
  }
}

Node* BST::makeTree(int *a, int l, int h){
  if(l > h)
    return NULL;

  else{
    int m = (l + h) / 2;

    Node *n = new Node(a[m]);
  
    n->left = makeTree(a,l,m-1);

    n->right = makeTree(a,m+1,h);

    return n;
  }
}

int main(){
  int a[] = {3,4,5,6,7,8,9,10,12,13,14};
  int size = 11;

  BST *tree = new BST();
  tree->root = tree->makeTree(a,0,10);
  tree->inOrderTraversal(tree->root);
  cout << endl;

  return 0;
}