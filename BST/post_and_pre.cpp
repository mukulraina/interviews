#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
  void preOrder(Node *n);
  void postOrder(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void BST::preOrder(Node *n){
  if(n != NULL){
    cout << n->value << '\t';
    preOrder(n->left);
    preOrder(n->right);
  }
}

void BST::postOrder(Node *n){
  if(n != NULL){
    postOrder(n->left);
    postOrder(n->right);
    cout << n->value << '\t';
  }
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(3);
  tree->insert(6);
  tree->insert(11);


  tree->inOrder(tree->root);
  cout << endl;

  tree->preOrder(tree->root);
  cout << endl;

  tree->postOrder(tree->root);
  cout << endl;
  return 0;
};