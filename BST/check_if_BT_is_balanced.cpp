int max(int a, int b){
  return ((a > b) ? a : b);
}

int BST::getHeight(Node *n){
  if(n == NULL)
    return 0;

  // check if left subtree is balanced
  int l = getHeight(n->left);
  if(l == -1)
    return -1;

  // check if right subtree is balanced
  int r = getHeight(n->right);
  if(r == -1)
    return -1;

  // check if the current tree is balanced
  int d = l - r;
  if(abs(d) > 1)
    return -1;
  else
    return max(l,r) + 1;
}

bool BST::isBalanced(Node *n){
  if(getHeight(n) == -1)
    return false;
  else
    return true;
}