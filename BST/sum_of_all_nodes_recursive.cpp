#include <iostream>
#include <queue>
#include <stack>
using namespace std;

// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int v);
    void printInOrder(Node *node);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

bool singleNodeBST(Node *n){
  return (n->left == NULL && n->right == NULL);
}

void helper(Node *n, int &sum){
  if(n != NULL){
    helper(n->left,sum);
    sum += n->value;
    helper(n->right,sum);
  }
}

int sum(Node *n){
  if(n == NULL)
    return 0;
  else
    return sum(n->left) + n->value + sum(n->right);
}

int main(){
  BST *tree = new BST();
  tree->insert(1);
  tree->insert(2);
  tree->insert(3);
  tree->insert(4);
  tree->insert(5);
  tree->insert(6);
  tree->insert(7);
  tree->insert(8);
  tree->insert(9);
  tree->insert(10);



  tree->printInOrder(tree->root);
  cout << endl;
  
  cout << sum(tree->root);
  cout << endl;
  return 0;
}
