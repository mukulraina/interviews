#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
  int getLevel(Node *n,int v);
  int getLevelHelper(Node *n, int v, int &level);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

int BST::getLevelHelper(Node *n, int v, int &level){
  // It will only hit null if the node is not in the tree
  if(n == NULL)
    return -1;

  else if(n->value == v)
    return level;
  
  else{
    level++;
    if(v < n->value)
      return getLevelHelper(n->left,v,level);
    else
      return getLevelHelper(n->right,v,level);
  }
}

int BST::getLevel(Node *n, int v){
  int level = 0;
  return getLevelHelper(n,v,level);
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(6);
  tree->insert(15);
  tree->insert(3);
  tree->insert(8);
  tree->insert(2);
  tree->insert(5);
  tree->insert(7);
  tree->insert(1);


  tree->inOrder(tree->root);
  cout << endl;

  cout << tree->getLevel(tree->root,99) << endl;
  return 0;
};