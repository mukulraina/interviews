#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
  void BFSTraversal(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

// just using BFS traversal
Node* getPointer(Node *n, int v){
  if(n == NULL)
    return NULL;

  else{
    queue<Node*> q;

    q.push(n);

    while(q.empty() != true){
      Node *c = q.front();
      
      // if you find the value, return the pointer
      if(c->value == v){
        cout << "A";
        return c;
      }

      q.pop();

      // insert left + right children in the tree
      if(c->left != NULL)
        q.push(c->left);
      if(c->right != NULL)
        q.push(c->right);
    }
    
    // If the program comes till here that means the value wasn't found
    // so return NULL
    return NULL;
  }
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(12);
  tree->insert(18);
  tree->insert(3);
  tree->insert(6);

  Node *ptr = NULL;
  ptr = getPointer(tree->root,3);
  if(ptr == NULL)
    cout << "sorry the node wasn't found in the tree" << endl;
  else
    cout << "the node returned the following value: " << ptr->value << endl;
  ptr = getPointer(tree->root,5);
  if(ptr == NULL)
    cout << "sorry the node wasn't found in the tree" << endl;
  else
    cout << "the node returned the following value: " << ptr->value << endl;
  ptr = getPointer(tree->root,99);
  if(ptr == NULL)
    cout << "sorry the node wasn't found in the tree" << endl;
  else
    cout << "the node returned the following value: " << ptr->value << endl;

  return 0;
};