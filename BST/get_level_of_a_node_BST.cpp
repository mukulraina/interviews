#include <iostream>
using namespace std;

// Node
class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
  Node();
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void inOrderTraversal(Node *node);
  void insert(int v);
};

void BST::insert(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: empty tree
  if(!root){
    root = newNode;
    return;
  }

  // Case 2: Normal case
  // Traverse through the tree + find the parent
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;

    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

BST::BST(){
  root = NULL;
}

void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << '\t';
    inOrderTraversal(node->right);
  }
}

// will only work for BST (not BT)
int helper(Node *n, int v, int l){
  if(n == NULL)
    return -1;
  else{
    if(n->value == v)
      return l;
    else if(v < n->value)
      return helper(n->left,v,l+1);
    else
      return helper(n->right,v,l+1);
  }
}

int getLevel(Node *n, int v){
  if(n == NULL)
    return -1;
  else
    return helper(n,v,1);
}
int main(){
  int a[] = {15,10,18,5,12,17,20};
  int size = 7;

  BST *tree = new BST();
  
  for(int i = 0; i < size; i++)
    tree->insert(a[i]);

  tree->inOrderTraversal(tree->root);
  cout << endl;

  int v = 17;
  cout << "level: " << getLevel(tree->root,v) << endl;

  return 0;
}