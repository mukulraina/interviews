#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  void insert(int v);
  void inorder(Node *n);
  BST();
};
BST::BST(){
  root = NULL;
}

void BST::insert(int v){
  // Make the new node
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;

  else{
    Node *p = NULL;
    Node *c = root;

    // Traverse until c hits NULL
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    // Now p points to the parent
    if(v < p->value)
      p->left = newNode;
    else
      p->right = newNode;
  }
}

void BST::inorder(Node *n){
  if(n == NULL)
    return;
  else{
    inorder(n->left);
    cout << n->value << '\t';
    inorder(n->right);
  }
}

void getSumHelper(Node *n, int &sum){
  if(n != NULL){
    getSumHelper(n->left,sum);
    sum = sum + n->value;
    getSumHelper(n->right,sum);
  } 
}

int getSum(Node *n){
  if(n == NULL)
    return -1;
  else if(n->left == NULL || n->right == NULL)
    return n->value;
  else{
    int sum = 0;
    getSumHelper(n,sum);
    return sum;
  }
}

int main(){
  BST *bst = new BST();

  bst->insert(10);
  bst->insert(5);
  bst->insert(15);
  bst->insert(3);
  bst->insert(8);
  bst->insert(12);
  bst->insert(17);
  bst->insert(2);

  bst->inorder(bst->root);
  cout << endl;

  cout << "sum: " << getSum(bst->root) << endl;

  return 0;
}