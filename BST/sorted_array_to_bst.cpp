// http://www.geeksforgeeks.org/sorted-array-to-balanced-bst/
#include <iostream>
using namespace std;

// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    void insert(int v);
    void printInOrder(Node *node);
    Node* makeBST(int *a, int l, int h);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(root == NULL){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

Node* BST::makeBST(int *a, int l, int h){
  if(l > h)
    return NULL;

  else{
    int mid = (l + h) / 2;
    
    Node *n = new Node(a[mid]);

    n->left = makeBST(a,l,mid - 1);
    n->right = makeBST(a,mid + 1,h);

    return n;
  }
}

int main(){
  BST *tree = new BST();
  int a[] = {1,2,3,4,5,6,7};
  tree->root = tree->makeBST(a,0,6);
  tree->printInOrder(tree->root);
  cout << endl;
  return 0;
}