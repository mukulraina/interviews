#include <iostream>
#include <queue>
#include <stack>
using namespace std;

// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int v);
    void printInOrder(Node *node);
    void reverseLevelOrder(Node *n);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

void BST::reverseLevelOrder(Node *n){
  if(n == NULL)
    return;
  else{
    queue<Node *> q;
    stack<Node *> s;

    q.push(n);
    s.push(n);

    while(q.empty() != true){
      Node *t = q.front();
      q.pop();
      s.push(t);

      
      // right first, left second
      if(t->right != NULL)
        q.push(t->right);

      if(t->left != NULL)
        q.push(t->left);

    }

        // Print the stack
    while(s.empty() != true){
      Node *t = s.top();
      cout << t->value << '\t';
      s.pop();
    }
  }
}

int main(){
  BST *tree = new BST();
  tree->insert(8);
  tree->insert(5);
  tree->insert(10);
  tree->insert(4);
  tree->insert(7);
  tree->insert(9);
  tree->insert(14);


  tree->printInOrder(tree->root);
  cout << endl;
  
  tree->reverseLevelOrder(tree->root);
  cout << endl;
  return 0;
}
