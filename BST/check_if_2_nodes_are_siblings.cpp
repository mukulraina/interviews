#include <iostream>
using namespace std;

// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    void insert(int v);
    void printInOrder(Node *node);
    int successor(int v);
    int getMax(Node *n);
    int getMin(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(root == NULL){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

int BST::getMax(Node *n){
  if(n->right == NULL)
    return n->value;
  else
    return getMax(n->right);
}

int BST::getMin(Node *n){
  if(n->left == NULL)
    return n->value;
  else
    return getMin(n->left);
}

bool areSiblings(Node *n, Node *a, Node *b){
  if(n == NULL)
    return false;
  else
    return ((n->left == a && n->right == b) ||
            (n->left == b && n->right == a) ||
            areSiblings(n->left,a,b) ||
            areSiblings(n->right,a,b));
}

Node *getPointerBST(Node *r, int v){
  // check if tree is empty
  if(r == NULL)
    return NULL;

  else{
    Node *c = r;
    while(c != NULL){
      if(c->value == v)
        break;
      else if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    // if c points to NULL the value wasn't found 
    // If c is not NULL then its pointing to the node so return that
    if(c == NULL)
      return NULL;
    else
      return c;
    }
}

int main(){
  BST *tree = new BST();
  tree->insert(8);
  tree->insert(5);
  tree->insert(10);
  tree->insert(4);
  tree->insert(7);
  tree->insert(9);
  tree->insert(14);
  tree->insert(3);
  tree->printInOrder(tree->root);
  cout << endl;
  
  cout << areSiblings(tree->root,getPointerBST(tree->root,5),getPointerBST(tree->root,10)) << endl;
  cout << areSiblings(tree->root,getPointerBST(tree->root,4),getPointerBST(tree->root,7)) << endl;
  cout << areSiblings(tree->root,getPointerBST(tree->root,5),getPointerBST(tree->root,7)) << endl;
  cout << areSiblings(tree->root,getPointerBST(tree->root,10),getPointerBST(tree->root,7)) << endl;
  return 0;
}