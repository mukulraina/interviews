#include <iostream>
#include <queue>
using namespace std;

// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
    Node *nextRight;
};
Node::Node(){
  left = right = nextRight = NULL;
}
Node::Node(int v){
  value = v;
  left = right = nextRight = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int v);
    void printInOrder(Node *node);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

void helper(Node *n, int levelCounter, int level){
  if(n != NULL){
    helper(n->left,levelCounter + 1, level);
    if(levelCounter == level)
      cout << n->value << '\t';
    helper(n->right,levelCounter + 1, level);
  }
}

void print(Node *n, int level){
  int levelCounter = 0;
  helper(n,levelCounter,level);
  cout << endl;
}

int main(){
  BST *tree = new BST();
  tree->insert(8);
  tree->insert(5);
  tree->insert(10);
  tree->insert(4);
  tree->insert(7);
  tree->insert(9);
  tree->insert(14);

  tree->insert(3);
  tree->printInOrder(tree->root);
  cout << endl;

  int level = 2;

  print(tree->root,level);
  cout << endl;
  return 0;
}
