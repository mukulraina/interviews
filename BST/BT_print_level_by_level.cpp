#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
  void BFSTraversal(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void printLevelByLevel(Node *n){
  if(n == NULL)
    return;
  else{
    // Declare 2 counter variables
    int nodesInCurrentLevel = 0;
    int nodesInNextLevel = 0;

    // declare a queue
    queue<Node *> q;

    // push the first node in it + increment current level nodes
    q.push(n);
    nodesInCurrentLevel++;
    
    while(q.empty() != true){
      // pop a node from the queue
      Node *t = q.front();
      q.pop();

      // Decrement the counter 
      nodesInCurrentLevel--;

      // print the current node
      cout << t->value << '\t';

      // push the children nodes in the queue
      if(t->left != NULL)
        q.push(t->left);
      if(t->right != NULL)
        q.push(t->right);

      // since the 2 nodes are in next level
      nodesInNextLevel += 2;

      // if no nodes in current level, that means new level to start
      // so enter a newline in here
      if(nodesInCurrentLevel == 0){
        cout << '\n';
        // so the nodesInNextLevel are now the current level nodes
        nodesInCurrentLevel = nodesInNextLevel;
        // we make the nodes in next level 0 cos we start tracking the next level now
        nodesInNextLevel = 0;
      }
    }
  }
}

void deleteTree(Node *n){
  if(n == NULL)
    return; // base case
  else{
    // return left and right subtree first
    deleteTree(n->left);
    deleteTree(n->right);

    // now delete the node itself
    // make sure you set the root = NULL in the main()
    delete(n);
  }
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(12);
  tree->insert(18);
  tree->insert(3);
  tree->insert(6);

  printLevelByLevel(tree->root);
  cout << endl;

  // always delete the tree
  deleteTree(tree->root);
  tree->root = NULL;
  return 0;
};