#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
  int maxDepth(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

int getMax(int a, int b){
  if(a > b)
    return a;
  else
    return b;
}

int BST::maxDepth(Node *n){
  if(n == NULL)
    return 0;

  else{
    int l = maxDepth(n->left);
    int r = maxDepth(n->right);
    return getMax(l,r) + 1;
  }
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(6);
  tree->insert(15);
  tree->insert(3);
  tree->insert(8);
  tree->insert(2);
  tree->insert(5);
  tree->insert(7);
  tree->insert(1);


  tree->inOrder(tree->root);
  cout << endl;

  cout << tree->maxDepth(tree->root) << endl;
  return 0;
};