// Find vertical sum of the nodes that are in same vertical line.
// Incomplete code
#include <iostream>
#include <unordered_map>
using namespace std;
typedef std::unordered_map<int,int> Mymap;

// Node Class
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int value);
    void printInfo();
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int value){
  this->value = value;
  left = right = NULL;
}
void Node::printInfo(){
  cout << "value: " << value << endl;
}


// Tree Class
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int v);
    void inOrderTraversal(Node *node);
    int sum(Node *n);
    void sumHelper(Node *n, int hD, Mymap &hash);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  // Make a node
  Node *newNode = new Node(v);

  // Case 1: if tree is empty
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Normal Case
  Node *current = root;
  Node *parent = NULL;
  while(current != NULL){
    parent = current;
    if (v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now current points to the parent
  // Decide whether to put as left or right child
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << endl;
    inOrderTraversal(node->right);
  }
}

int BST::sum(Node *n){
  if(n == NULL)
    return;

  else{
    // Create a hash
    Mymap hash;

    // Call the helper to store the vertical sums in the hash
    sumHelper(n,0,hash);

    // Print the values of hash table
    // hD -> sum
  }
}

void BST::sumHelper(Node *n, int hD, Mymap &hash){
  if(n == NULL)
    return;

  else{
    // Store the vertical sum for left subtree
    sumHelper(n->left,hD-1,hash);

    // Update the vertical sum for hD for this node
    Mymap::iterator iter;
    iter = hash.find(hD);
    if(iter != hash.end())
      iter->second += n->value;
    else
      hash.insert(Mymap::value_type(hD,n->value));

    // Store the vertical sum for the right subtree
    sumHelper(n->right,hD+1,hash);
  }
}

// Test Client
int main(){
  BST *tree = new BST();
  int count[100] = {0};

  tree->insert(6);
  tree->insert(3);
  tree->insert(7);
  tree->insert(2);
  tree->insert(5);
  tree->insert(9);
  
  tree->inOrderTraversal(tree->root);
  
  cout << endl;
  tree->sum(tree->root);
  return 0;
}
