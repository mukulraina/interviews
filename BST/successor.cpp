#include <iostream>
using namespace std;

// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    void insert(int v);
    void printInOrder(Node *node);
    int successor(int v);
    int getMax(Node *n);
    int getMin(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(root == NULL){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

int BST::getMax(Node *n){
  if(n->right == NULL)
    return n->value;
  else
    return getMax(n->right);
}

int BST::getMin(Node *n){
  if(n->left == NULL)
    return n->value;
  else
    return getMin(n->left);
}

int BST::successor(int v){
  if(root == NULL)
    return -1;
  
  Node *c = root;
  while(c != NULL){
    if(c->value == v)
      break;
    else if(v < c->value)
      c = c->left;
    else
      c = c->right;
  }

  if(c == NULL){
    cout << "sorry element is not in the tree" << endl;
    return -1;
  }

  if (v == getMax(root)){
    cout << "sorry the max element in the tree cannot have a successor" << endl;
    return -1;
  }

  if(c->right != NULL)
    return getMin(c->right);

  Node *succ = NULL;
  c = root;
  while(c != NULL){
    if(c->value == v)
      break;

    else if(v < c->value){
      succ = c;
      c = c->left;
    }

    else 
      c = c->right;
  }

  return succ->value;
}

int main(){
  BST *tree = new BST();
  tree->insert(8);
  tree->insert(5);
  tree->insert(10);
  tree->insert(4);
  tree->insert(7);
  tree->insert(9);
  tree->insert(14);
  tree->insert(3);
  tree->printInOrder(tree->root);
  cout << endl;
  
  std::cout<<"The successor of 99 is: "<<tree->successor(99)<<'\n';
  std::cout<<"The successor of 8 is: "<<tree->successor(8)<<'\n';
  std::cout<<"The successor of 10 is: "<<tree->successor(10)<<'\n';
  std::cout<<"The successor of 14 is: "<<tree->successor(14)<<'\n';
  return 0;
}
