#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
  void BFSTraversal(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void printNodesWithoutSiblings(Node *n){
  if(n == NULL)
    return;
  else{
    // check if left child of the current node is a single child
    if(n->left != NULL && n->right == NULL)
      cout << n->left->value << endl;

    // check if right child of the current node is a single child
    if(n->left == NULL && n->right != NULL)
      cout << n->right->value << endl;

    // print the single child nodes from left subtree
    printNodesWithoutSiblings(n->left);
    
    // print the single child nodes from right subtree
    printNodesWithoutSiblings(n->right);
  }
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(11);
  tree->insert(6);
  tree->insert(12);

  printNodesWithoutSiblings(tree->root);

  return 0;
};