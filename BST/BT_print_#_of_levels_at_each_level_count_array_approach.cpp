#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void deleteTree(Node *n){
  if(n == NULL)
    return; // base case
  else{
    // return left and right subtree first
    deleteTree(n->left);
    deleteTree(n->right);

    // now delete the node itself
    // make sure you set the root = NULL in the main()
    delete(n);
  }
}

int max(int a, int b){
  if(a > b)
    return a;
  else 
    return b;
}

/*
  why do we need to find the height?
    - cos the size of the countArray will be the # of levels in the tree
    - and # of levels in the tree is equal to the height of the tree
*/
int getHeight(Node *n){
  if(n == NULL)
    return 0;
  else{
    int leftHeight = getHeight(n->left);
    int rightHeight = getHeight(n->right);

    return max(leftHeight,rightHeight) + 1;
  }
}

void helper(Node *n, int *countArray, int currentLevel){
  if(n == NULL)
    return;
  else{
    // increment the # of nodes in the current level
    countArray[currentLevel]++;

    // do it for the left + right subtree
    // make sure you increment the currentLevel when you go for the subtrees
    helper(n->left, countArray, currentLevel + 1); 
    helper(n->right, countArray, currentLevel + 1);
  }
}

void initializeAllToZero(int *countArray, int h){
  for(int i = 0; i <= h; i++)
    countArray[i] = 0;
}

void printNumNodesInEachLevel(Node *n){
  if(n == NULL)
    return;
  else{
    // get the number of levels in the tree
    int numOfLevels = getHeight(n); // Time:(n)

    int countArray[numOfLevels];

    // initialize all values to zero
    initializeAllToZero(countArray, numOfLevels);

    int currentLevel = 0;

    // populate the countArray using a helper function
    helper(n,countArray,currentLevel);

    // print count array
    for(int i = 0; i <= numOfLevels; i++){
      cout << i << ":" << countArray[i] << endl;
    }
  }
} 

int main(){
  BST *tree = new BST();

  tree->insert(20);
  tree->insert(25);
  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(12);
  tree->insert(18);
  tree->insert(19);
  tree->insert(3);
  tree->insert(6);

  printNumNodesInEachLevel(tree->root);

  // always delete the tree
  deleteTree(tree->root);
  tree->root = NULL;
  return 0;
};