// Time: O(n) Space:O(1)
void rev(Node *n){
  if(n != NULL){
    Node *t = n->left;
    n->left = n->right;
    n->right = t;

    rev(n->left);
    rev(n->right);
  }
}