#include<iostream>
using namespace std;

int my_atoi(char *s){
  int v = 0;

  for(int i = 0; s[i] != '\0' ; i++){
    v = (v * 10) + (s[i] - '0');
  }
  
  return v;
}

int main(){
  char c[] = "89789";
  cout << my_atoi(c) << endl;
  return 0;
}
