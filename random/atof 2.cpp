#include <iostream>
#include <math.h>
#include <iomanip> // for setprecision()
using namespace std;

double myAtof(string s){
	int sign = 1;
	int startingIndex = 0;
	int num = 0;

	if(s[0] == '-'){
		sign = -1;
		startingIndex = 1;
	}

	int i = startingIndex;

	// traverse the digits before the decimal
	while(i < s.size() && s[i] != '.'){
		int v = s[i];

		num = (num * 10) + (v - '0');

		i++;
	}

	i++;
	double num2 = 0.0;
	int j = 0;


	// traverse the digits after the decimal
	while(i < s.size()){
		double v = s[i];

		num2 = (num2 * 10.0) + (v - '0');

		i++;
		j++;
	}

	num2 = num2 / pow(10.0,j);


	return ((sign * num) + num2);
}

/*
	- not working correctly for negative numbers, fix that
	- add support for input like this e-25 or e20
*/
int main(){
	cout << setprecision(16);
	string s = "1234.5678";
	string s1 = "-1234.5678";

	cout << myAtof(s) << endl;
	cout << myAtof(s1) << endl;

	return 0;
}