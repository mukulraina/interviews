#include <iostream>
using namespace std;

class Node{
	public:
		int value;
		Node *next;
		Node(int v);
};
Node::Node(int v){
	value = v;
	next = NULL;
}

class stackLL{
	public:
		Node *head;
		void push(int v);
		void pop();
		int top();
		void print();
		stackLL();
};

stackLL::stackLL(){
	head = NULL;
}

// Time: (1)
void stackLL::push(int v){
	// make a node
	Node *newNode = new Node(v);

	// if 0 node in stack
	if(head == NULL)
		head = newNode;
	else{
		// otherwise insert at the beginning
		newNode->next = head; //make the newNode point to the first node
		head = newNode;
	}
}

// Time: O(1)
void stackLL::pop(){
	// if no nodes in the stack
	if(head == NULL)
		return;

	// delete the first node
	Node *t = head;
	head = head->next;
	delete t;
}

// Time: O(1)
int stackLL::top(){
	if(head == NULL)
		return -1;

	return head->value;
}

void stackLL::print(){
	if(head == NULL)
		return;
	else{
		Node *c = head;
		while(c != NULL){
			cout << c->value << '\t';
			c = c->next;
		}
		cout << endl;
	}
}

int main(){
	stackLL s;

	s.push(1);
	s.push(2);
	s.push(3);
	s.push(4);
	s.push(5);

	s.print();

	cout << s.top() << endl;

	s.pop();
	s.pop();

	s.print();

	cout << s.top() << endl;
	s.print();

	return 0;
}