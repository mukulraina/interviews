#include<iostream>
using namespace std;

double power(double x, int n){
  if(n == 0)
    return 1;

  double temp = power(x,n/2);

    if(n % 2 == 0)
      return temp * temp;
    else
      return x * temp * temp;
  
}

double pow(double x, int n){
  if(n < 0)
    return  1 / power(x,-n);
  else
    return power(x,n);
}

int main(){

  cout << pow(4,-2) << endl;
  return 0;
}
