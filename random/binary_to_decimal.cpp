#include <iostream>
#include <math.h>
using namespace std;

int binaryToDecimal(int n){
	int decimal = 0;
	int i = 0;
	int rem;

	while(n != 0){
		rem = n % 10;
		n = n / 10;
		decimal = decimal + (rem * pow(2,i));
		i++;
	}

	return decimal;
}

int main(){
	int value = 1010000;

	cout << binaryToDecimal(value) << endl;
	
	return 0;
}