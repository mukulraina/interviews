#include <iostream>
using namespace std;

class Node{
	public:
		int value;
		Node *next;
		Node(int v);
};
Node::Node(int v){
	value = v;
	next = NULL;
}

class queueLL{
	public:
		Node *head;
		Node *last;
		queueLL();
		void push(int v);
		void pop();
		int front();
		int back();
		void print();
};
queueLL::queueLL(){
	head = NULL;
	last = NULL;
}

// Time:O(1)
void queueLL::push(int v){
	Node *newNode = new Node(v);

	if(head == NULL){
		head = newNode;
		last = newNode;
	}
	else{
		// insert in the end
		last->next = newNode;
		last = last->next;
	}
}

// Time:O(1)
void queueLL::pop(){
	if(head == NULL)
		return;
	else{
		// remove from the start
		Node *t = head;
		head = head->next;
		delete t;
	}
}

int queueLL::front(){
	if(head == NULL)
		return -1;
	else
		return head->value;
}

int queueLL::back(){
	if(head == NULL)
		return -1;
	else
		return last->value;
}

void queueLL::print(){
	if(head == NULL)
		return;
	else{
		Node *c = head;
		while(c != NULL){
			cout << c->value << '\t';
			c = c->next;
		}
		cout << endl;
	}
}

int main(){
	queueLL q;

	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);
	q.push(5);

	q.print();
	cout << q.front() << endl;
	cout << q.back() << endl;

	q.pop();
	q.pop();

	q.print();
	cout << q.front() << endl;
	cout << q.back() << endl;

	q.push(6);
	q.push(7);
	q.push(8);

	q.print();
	cout << q.front() << endl;
	cout << q.back() << endl;
	return 0;
}