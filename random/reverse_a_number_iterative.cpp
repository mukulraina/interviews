#include <iostream>
using namespace std;

int getReverseNum(int n){

	int revNum = 0;

	while(n > 0){
		int digit = n % 10;
		revNum = (revNum * 10) + digit;
		n = n / 10;
	}

	return revNum;
}

int main(){
	int n = 4562;

	cout << getReverseNum(n) << endl;

	return 0;
}