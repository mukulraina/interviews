#include <iostream>
using namespace std;

int getFib(int num){
	if(num == 0 || num == 1)
		return num;
	else
		return getFib(num - 1) + getFib(num - 2);
}

int main(){
	int num = 6;

	cout << getFib(num) << endl;

	return 0;
}