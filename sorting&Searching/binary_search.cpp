#include <iostream>
using namespace std;

int BS(int *a, int l, int h, int v){
  // Base Case
  if(l > h)
    return -1; // Element wasn't found in the array

  else{
    int m = (l + h) / 2;

    if(v == a[m])
      return m;

    else if(v < a[m])
      return BS(a,l,m-1,v);

    else
      return BS(a,m+1,h,v);
  }
}

int main(){
  int a[] = {2,3,4,10,40};
  int l = 0;
  int h = 4;
  int v = 11;
  cout << BS(a,l,h,v) << endl;
  return 0;
}