#include <iostream>
using namespace std;

void printArray(int *a, int size){
  for(int i = 0; i < size; i++){
    cout << a[i] << '\t';
  }
  cout << endl;
}

void counting_sort(int *a, int size, int range){
  // Make a countArray to keep track of # of occurences of each element
  // Size of this countArray will be the range of integers (max elem of array)
  // Assign 0 to all the elements and increment if they appear in 'a' array
  int c[8] = {0}; // 1 bigger than the max no in the array
  for(int i = 0; i < size; i++){
    c[a[i]]++;
  }


  // Now do the prefixSum
  for(int i = 1; i < 8; i++){
    c[i] += c[i - 1];
  }

  // Now Make a tempArray and traverse 'a' and get final index of elems in a using 
  // updated countArray (after we have done prefixSum on it)
  int output[size]; // tempArray will be same size as the original array
  for(int i = 0; i < size; i++){
    int v = a[i];
    int indexOfValue = c[v];
    indexOfValue--;
    output[indexOfValue] = v;
    c[v]--;
  }

  // Copy the output array to the original array
  for(int i = 0; i < size; i++){
    a[i] = output[i];
  }
}

int main(){
  int a[] = {1,4,0,2,7,5,2,3,2};
  int size = 9;
  // range is the max number of the array
  int range = 7;
  counting_sort(a,size,range);
  printArray(a,size);
  return 0;
}
