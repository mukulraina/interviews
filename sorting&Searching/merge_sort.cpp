#include <iostream>
using namespace std;

void merge(int *a, int low, int mid, int high){
  int i, j, k, tempArray[50];
  i = low;
  k = low;
  j = mid + 1;
  while(i <= mid && j <= high){
    if(a[i] < a[j]){
      tempArray[k] = a[i];
      k++;
      i++;
    }
    else{
      tempArray[k] = a[j];
      k++;
      j++;
    }
  }
  /* If the 1st subarray wasn't done */
  while(i <= mid){
    tempArray[k] = a[i];
    k++;
    i++;
  }
  /* If the 2nd subarray wasn't done */ 
  while(j <= high){
    tempArray[k] = a[j];
    k++;
    j++;
  }
  /* Copy the tempArray into the original one */
  for(int i = low ; i < k; i++){
    a[i] = tempArray[i];
  }
}

void mergeSort(int *a, int low, int high){
  int mid;
  if (low < high){
    mid = (low + high) / 2;
    mergeSort(a,low,mid);
    mergeSort(a,mid+1,high);
    merge(a,low,mid,high);
  }
}

int main(){
    int i;
    int a[10] = {17, 37, 7, 11, 75, 90, 46, 65, 11, 78};

    mergeSort(a, 0, 9);
    
    cout<<"sorted array\n";
    for (i = 0; i < 10; i++){
        cout<<a[i]<<endl;
    }
  return 0;
}