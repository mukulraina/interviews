#include <iostream>
using namespace std;

void quickSort(int *a, int left, int right){
  int i = left;
  int j = right;

  // get pivot
  int pivot = a[(i + j) / 2];

  // Partition
  while(i <= j){
    while(a[i] < pivot)
      i++;

    while(a[j] > pivot)
      j--;

    if(i <= j){
      swap(a[i],a[j]);
      i++;
      j--;
    }
  }

  // Do it recusively on left and right halves
  if(left < j)
    quickSort(a,left,j);
  if(i < right)
    quickSort(a,i,right);
}


int main(){
  int i;
  int a[10] = {17, 37, 7, 11, 75, 90, 46, 65, 11, 78};
  quickSort(a, 0, 9);
  cout<<"sorted array\n";
  for (i = 0; i < 10; i++){
      cout<<a[i]<<endl;
  }
return 0;
}