#include <iostream>
using namespace std;

// Returns the index of the element (if not found returns -1)
// Time: O(logn) Space: O(1) (inspace - no extra space required)
int BS(int *a, int l, int h, int v){
  while(l <= h){
    // get mid
    int m = (l + h);

    if(a[m] == v)
      return m;
    else if(v < a[m])
      h = m - 1;
    else
      l = m + 1;
  }

  // if it reaches here that means the value wasn't found
  return -1;
}

int main(){
  int a[] = {2,3,4,10,40,50,60,70,80};
  int l = 0;
  int h = 9;

  cout << BS(a,l,h,3) << endl;
  cout << BS(a,l,h,1) << endl;
  cout << BS(a,l,h,99) << endl;

  return 0;
}