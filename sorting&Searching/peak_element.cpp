#include <iostream>
using namespace std;

int findPeak(int *a, int l, int h){
  int m = (l + h) / 2;

  if(a[m] > a[m + 1] && a[m] > a[m-1])
    return a[m];

  else if(a[m] < a[m + 1])
    return findPeak(a,m+1,h);

  else
    return findPeak(a,l,m-1);
}

int main(){
  int a[] = {1, 3, 20, 4, 1, 0};
  int l = 0;
  int h = 5;
  cout << findPeak(a,l,h) << endl;
  return 0;
}
