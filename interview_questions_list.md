Programming Problems
==========

***please refer to workflowy for now***

**Resources**
  - go more detailed into solutions of CTCI
  - geeksforgeeks
  - workflowy
  - leetcode
  - glassdoor
  - careercup
  - stanford 
    - LL: http://cslibrary.stanford.edu/110/
    - BT: http://cslibrary.stanford.edu/110/


**General**
=======================================

| ProgrammingProblem | Solution | Source      
| :------------- |:-------------| :-------------|
| atoi() 
| itoa()
| pow()
| sqrt()

**Arrays**
=======================================

| ProgrammingProblem | Solution | Source      
| :------------- |:-------------| :-------------|
| element occuring odd number of times | 
| pair of elements in array whose sum is x
| segregate 0s n 1s in array
| sort an array of only 2 distinct numbers
| segregate even and odd elements in array
 
**String**
------
| ProgrammingProblem | Solution | Source      
| :------------- |:-------------| :-------------|
| Check if string has all unique characters | CTCI 
| Reverse a null terminated string | CTCI
| Check if 2 strings are anagrams | CTCI
| Replace spaces in a string with %20 | CTCI
| run length encoding | CTCI


**Binary Search Tree**
====================

| ProgrammingProblem | Solution | Source      
| :------------- |:-------------| :-------------|
| Node and binary search tree classes | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BST_basic.cpp | http://cslibrary.stanford.edu/110/ | 
| Insert a node | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BST_basic.cpp |  |
| Delete the tree  | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BT_delete_tree.cpp | http://www.geeksforgeeks.org/write-a-c-program-to-delete-a-tree/ | 
| Tree Traversel | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/tree_traversals.cpp | http://en.wikipedia.org/wiki/Tree_traversal
| Search a node | 1.Iterative-https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BST_search_iter.cpp    2.Recursive-https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BST_search_recur.cpp| |
| Find parent | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BST_find_parent.cpp | | 
| Max and Min | 1.Recursive: https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BST_max_min.cpp 2.Iterative: https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BST_max_min.cpp | |
| successor | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BST_successor.cpp | http://algorithmsandme.blogspot.ca/2013/08/binary-search-tree-inorder-successor.html#.VKtAd4rF8Yc
| Predecessor | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BST_predecessor.cpp | http://algorithmsandme.blogspot.ca/2013/08/binary-search-tree-inorder-successor.html#.VKtAd4rF8Yc
| Delete a node | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BST_delete_a_node.cpp | 1.http://www.algolist.net/Data_structures/Binary_search_tree/Removal 2.http://geeksquiz.com/binary-search-tree-set-2-delete/|
| Lowest common ancestor in BST | https://github.com/mukul-raina/practice-cpp/blob/master/BST/LCA_BST.cpp | | 


**Binary Tree**

| ProgrammingProblem | Solution | Source      
| :------------- |:-------------| :-------------|
| Search for a value (left + right approach) | 1.https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BT_search_leftright.cpp 2.https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BT_search_approach2.cpp |  | 
| Size of the tree | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BT_size.cpp |  |
| Get # of leaf nodes | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BT_num_leaf_nodes.cpp |  |
| Get sum of all nodes | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BT_sum_of_all_nodes.cpp |  |
| Delete a binary tree | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BT_delete_tree.cpp | | 
| Identical trees | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BT_identical_trees.cpp | | 
| Height of a tree (max depth) | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BT_height(max_depth).cpp | |
| Level of a node | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BT_level_of_a_node.cpp | http://www.geeksforgeeks.org/get-level-of-a-node-in-a-binary-tree/| 
| Level order traversal | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BT_level_order_trav.cpp | http://www.geeksforgeeks.org/level-order-tree-traversal/
| Print all nodes from a given level | 1.https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BT_all_nodes_of_level.cpp 2.https://github.com/mukul-raina/practice-cpp/blob/master/BST/print_all_nodes_at_a_given_level.cpp | | 
| Number of nodes in a given level | 1.https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BT_num_nodes_in_level.cpp 2.https://github.com/mukul-raina/practice-cpp/blob/master/BST/no_of_nodes_in_given_level.cpp 3.https://github.com/mukul-raina/practice-cpp/blob/master/BST/print_all_nodes_from_same_level.cpp | |
| Search a binary tree using BFS | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BT_search_BFS.cpp | http://www.geeksforgeeks.org/level-order-tree-traversal/| 
| Search a binary tree using DFS | | http://www.geeksforgeeks.org/depth-first-traversal-for-a-graph/| 
| Check if a binary tree is BST | 1.https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/verify_bst.cpp 2.https://github.com/mukul-raina/practice-cpp/blob/master/BST/verify_BST_inorder.cpp | http://www.geeksforgeeks.org/a-program-to-check-if-a-binary-tree-is-bst-or-not/ |
| Print a tree level by level | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/BT_print_level_by_level.cpp | http://leetcode.com/2010/09/printing-binary-tree-in-level-order.html | http://www.ardendertat.com/2011/12/05/programming-interview-questions-20-tree-level-order-print/| 
| Level with max # of nodes | https://github.com/mukul-raina/practice-cpp/blob/master/BST/return_level_with_max_no_of_nodes_BST.cpp | | 
| Check if 2 nodes are siblings | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/check_if_2_nodes_are_siblings.cpp | http://www.geeksforgeeks.org/check-two-nodes-cousins-binary-tree/  |
| Nodes without siblings | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/nodes_without_siblings.cpp | http://www.geeksforgeeks.org/print-nodes-dont-sibling-binary-tree/ | 
| Check if 2 nodes are cousins | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/check_if_2_nodes_are_cousins.cpp | http://www.geeksforgeeks.org/check-two-nodes-cousins-binary-tree/ | 
| Check if a tree is subtree of another | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/if_a_tree_is_subtree_of_another.cpp | 1.http://www.geeksforgeeks.org/check-if-a-binary-tree-is-subtree-of-another-binary-tree/ 2.http://www.geeksforgeeks.org/check-binary-tree-subtree-another-binary-tree-set-2/ | 
| Convert sorted array to BST |  https://github.com/mukul-raina/practice-cpp/blob/master/BST/given_sorted_array_make_BST_minimal_height.cpp | http://www.geeksforgeeks.org/sorted-array-to-balanced-bst/ | 
| Level with max # of nodes |  https://github.com/mukul-raina/practice-cpp/blob/master/BST/BT_level_with_max_#_of_nodes.cpp | http://www.geeksforgeeks.org/maximum-width-of-a-binary-tree/ | 
| Lowest common ancestor (BT) | https://github.com/mukul-raina/practice-cpp/blob/master/BST/LCA_BT.cpp | http://www.geeksforgeeks.org/lowest-common-ancestor-binary-tree-set-1/ | 
| Connect nodes at the same level | https://github.com/mukul-raina/practice-cpp/blob/master/BST/connect_nodes_at_same_level_bst.cpp | 1.http://leetcode.com/2010/03/first-on-site-technical-interview.html 2.http://www.geeksforgeeks.org/connect-nodes-at-same-level-with-o1-extra-space/ 3.http://www.geeksforgeeks.org/connect-nodes-at-same-level/ | 
| Check if a binary tree is balanced | https://github.com/mukul-raina/geeks-for-geeks/blob/master/BT/check_if_BT_is_balanced.cpp | http://www.geeksforgeeks.org/how-to-determine-if-a-binary-tree-is-balanced/ | 
| Construct a tree with preorder and inorder | https://github.com/mukul-raina/practice-cpp/blob/master/BST/build_tree_from_inOrder_and_preOrder.cpp | http://www.geeksforgeeks.org/construct-tree-from-given-inorder-and-preorder-traversal/ | 
| Construct a tree with preorder and postorder | | http://www.programcreek.com/2013/01/construct-binary-tree-from-inorder-and-postorder-traversal/| 
| Construct a tree with preorder and levelorder | |http://www.geeksforgeeks.org/construct-tree-inorder-level-order-traversals/ |
| Tree traversal without recursion | | 1.http://leetcode.com/2010/04/binary-search-tree-in-order-traversal.html 2.http://www.geeksforgeeks.org/inorder-tree-traversal-without-recursion/ 3.http://www.geeksforgeeks.org/inorder-tree-traversal-without-recursion-and-without-stack/ |
| Check if 2 trees are isomorphic | | http://www.geeksforgeeks.org/tree-isomorphism-problem/ | 
| Max width of a tree| 
| Convery binary tree to array| 
| Reverse a Binary Tree |
| Tree sort| 
| Vertical sum |
| Max sum path| 
| Reverse level order traversal | 
| Print all nodes with their level |
| Find parent in binary tree| 

**Graphs**
==============================
| Question        
| :------------- |:-------------|
| BFS | http://www.geeksforgeeks.org/breadth-first-traversal-for-a-graph/


**Linked List**
=======================

|ProgrammingProblem | Solution | Source
|:---|:----|:---|
| Basic LL | https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/LL_basic.cpp | 1.http://geeksquiz.com/linked-list-set-1-introduction/ 2.http://cslibrary.stanford.edu/110/
| Insert a node | 1.at the start: https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/LL_insert_at_start.cpp 2.at the end: https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/LL_insert_at_the_end.cpp 3.after a value: https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/LL_insert_after_a_value.cpp 4.before a value: https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/LL_insert_before_a_value.cpp | http://geeksquiz.com/linked-list-set-2-inserting-a-node/ |
| Delete the list | https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/LL_delete_list.cpp | http://www.geeksforgeeks.org/write-a-function-to-delete-a-linked-list/ |
| Delete a node | https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/LL_delete_a_node.cpp | http://geeksquiz.com/linked-list-set-3-deleting-node/ |
| Search a node | https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/LL_search.cpp | | 
| Print the list recursively | https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/LL_print_recursive.cpp | | 
| Print the list in reverse order | https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/rev_print_recur.cpp | http://www.geeksforgeeks.org/write-a-recursive-function-to-print-reverse-of-a-linked-list/ |
| Reverse a Linked List | 1.Iterative-https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/rev_iter_sep_func.cpp https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/rev_iter_class_memb.cpp 2.Recursive-https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/rev_list_recursive.cpp| http://www.geeksforgeeks.org/write-a-function-to-reverse-the-nodes-of-a-linked-list/| 
| Get last node | https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/LL_last_node.cpp|| 
| Kth node from the front | https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/nth_node_from_front.cpp | | 
| Kth node form the end |https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/nth_node_from_end.cpp | http://www.geeksforgeeks.org/nth-node-from-the-end-of-a-linked-list/ | 
| Size of LL | 1.Iterative-https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/LL_size_iter.cpp 2.Recursive-https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/LL_size_recur.cpp ||
| Middle of LL | https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/get_middle_of_LL.cpp | http://www.geeksforgeeks.org/write-a-c-function-to-print-the-middle-of-the-linked-list/| 
| Rotate a LL k times | https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/rotate_LL_k_times.cpp | http://www.geeksforgeeks.org/rotate-a-linked-list/ | 
| Reverse a LL in groups of size K | https://github.com/mukul-raina/geeks-for-geeks/blob/master/LL/reverse_LL_in_groups_of_k_iterative.cpp | http://www.geeksforgeeks.org/reverse-a-list-in-groups-of-given-size/ | 
| Y shaped LL (intersection point)
| Check if a LL has a cycle
| Remove the cycle in a LL
| Get the starting point of a cycle in LL
| Interweave a LL
| Shift even and odd nodes in LL
| Segregate 0s and 1s in LL
| Sort a LL of 0s 1s and 2s
| Remove duplicates from sorted LL | http://www.geeksforgeeks.org/remove-duplicates-from-a-sorted-linked-list/
| Remove duplicates from unsorted LL | http://www.geeksforgeeks.org/remove-duplicates-from-an-unsorted-linked-list, https://github.com/mukul-raina/practice-cpp/blob/master/LL/delete_duplicates_from_LL.cpp
| Partition a LL around a value x
| Intersection of 2 unsorted LL | http://www.geeksforgeeks.org/union-and-intersection-of-two-linked-lists/
| Intersection of 2 sorted LL
| Union of 2 unsorted LL | http://www.geeksforgeeks.org/union-and-intersection-of-two-linked-lists/
| Union of 2 sorted LL | 
| Merge 2 sorted LL |  http://www.geeksforgeeks.org/merge-two-sorted-linked-lists/
| Pairwise swap LL (by links) |  http://www.geeksforgeeks.org/pairwise-swap-elements-of-a-given-linked-list-by-changing-links/
| Pairwise swap LL (by values)
| LL Palindrome (stack)
| LL Palindrome (recursion)
| Merge 2 LL at alternate positions (in-place)
| Merge 2 LL at alternate positions (extra space)
| MoveNode() (move an element from 1 LL to another - in front)
| frontBackSplit() LL
| merge 2 sorted LL
| merge sort on LL
| flattening a LL
| inverse rotate a LL
| swap kthNode from the start and from the end
