#include <iostream>
using namespace std;

class Node{
public:
	int value;
	Node *next;
	Node(int v);
};
Node::Node(int v){
	value = v;
	next = NULL;
}

class LL{
public:
	Node *head;
	LL();
	void insertAtTheEnd(int v);
	void deleteNode(int v);
	void printList();
	void deleteList();
};
LL::LL(){
	head = NULL;
}

void LL::insertAtTheEnd(int v){
	// Make a node
	Node *newNode = new Node(v);

	// if it's an empty list
	if(head == NULL){
		head = newNode; // make head point to the new node
	}
	else{
		// else traverse to the last node
		Node *c = head;

		while(c->next != NULL){
			c = c->next;
		}

		// now c points to the last node
		c->next = newNode;
	}
}

void LL::deleteNode(int v){
	// case 1: empty list
	if(head == NULL)
		return;
	else{
		// check if the value is in the list or not
		Node *c = head;
		Node *p = NULL;

		while(c != NULL){
			if(c->value == v)
				break;

			p = c;
			c = c->next;
		}

		// case 2: value not in list
		if(c == NULL)
			return;
		else{
			// case 3: value in the list

			// case 3a: first node to be deleted (also works if there's only 1 node in list)
			if(p == NULL){
				head = head->next; // make head point to the 2nd node
				delete c; // delete c
			}

			else{
				// node is somewhere in the middle or is the last node
				p->next = c->next;
				delete c;
			}
		}
	}
}

void LL::printList(){
	// check if list is empty
	if(head == NULL){
		cout << "sorry empty list" << endl;
		return;
	}
	else{
		Node *c = head;
		while(c != NULL){
			cout << c->value << '\t';
			c = c->next;
		}
		cout << endl;
	}
}

void LL::deleteList(){
	if(head == NULL)
		return;
	else{
		Node *c = head;
		Node *n = NULL;

		while(c != NULL){
			// keep track of next node
			n = c->next;

			// delete the current node
			delete(c);

			// make next node the current node
			c = n;
		}

		// make head point to NULL
		head = NULL;
	}
}

int main(){
	LL *list = new LL();

	list->insertAtTheEnd(1);
	list->insertAtTheEnd(2);
	list->insertAtTheEnd(3);
	list->insertAtTheEnd(4);

	list->printList();

//	list->deleteNode(8); // test case 1
//	list->deleteNode(4); // test case 2
//	list->deleteNode(1); // test case 3
//  list->deleteNode(2); // test case 4

	list->printList();

	// always delete list
	list->deleteList();

	return 0;
}

