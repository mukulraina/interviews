// inserts a node in front of the DLL
void DLL::insertAtFront(int v){
	// Step 1: make a node that needs to be inserted
	// This a node for DLL (not LL)
	// its structure is 1 value and 2 pointers (prev and next)
	// both prev and next are NULL initialized in the constructor
	Node *newNode = new Node(v);

	// Case 1: 0 node in the list 
	if(head == NULL) 
		head->next = newNode;
	else{
		Node *t = head; // keep a temp pointer for the original first node
		head->next = newNode; // make head point to the newNode
		newNode->next = t; // newNode points to the original first node
		t->prev = newNode; // old first node points back to newNode
	}
}

void DLL::insertInTheEnd(int v){
	// make a node
	Node *newNode = new Node(v);

	// check if there's a node in the list
	if(head == NULL)
		return;
	else{
		// go to the end
		Node *c = head;

		while(c->next != NULL){
			c = c->next;
		}

		// now c points to the end of the list
		c->next = newNode;
		newNode->prev = c;
	}
}

void DLL::insertAfterANode(int nodeValue, int v){
	if(head == NULL) // 0 node in the list
		return;
	else{
		// make a node
		Node *newNode = new Node(v);

		// Check if that node is in the list or not
		Node *c = head;
		while(c != NULL){
			if(c->value == nodeValue) // compare
				break;

			c = c->next;
		}

		// if c is NULL that means value wasn't found
		if(c == NULL)
			return;

		// now c points to the node after which we need to insert
		// now n points to the node after c
		n = c->next;

		newNode->next = n; // newNode next points to next of c
		newNode->prev = c; // newNode prev points back to c
		n->prev = newNode; // c next points to newNode
		c->next = newNode;
	}
}

void DLL::insertBeforeANode(int nodeValue,int v){
	if(head == NULL) // if 0 node in list, return;
		return;
	else{
		// Make a node
		Node *newNode = new Node(v);

		// look for the value
		Node *c = head;
		Node *p = NULL;

		while(c != NULL){
			if(c->value == nodeValue)
				break;

			p = c;
			c = c->next;
		}

		// check if its the first node only
		if(p == NULL){
			head = newNode;
			newNode->next = c;
			c->prev = newNode;
		}
		else{
			// Now p points to the node before and c points to the node after
			p->next = newNode;
			newNode->prev = p;
			newNode->next = c;
			c->prev = newNode;
		}
	}
}


