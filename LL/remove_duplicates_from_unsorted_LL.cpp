// Program to remove duplicats from a LL (inspace)
#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node();
  Node(int v);
};
Node::Node(){
  next = NULL;
}
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void removeDuplicates();
};
LL::LL(){
  head = NULL;
}
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node
  if(head == NULL){
    head = newNode;
    return;
  }

  // Case 2: 1 or more nodes
  Node *c = head;
  // Traverse c so that it points to the last node
  while(c->next != NULL){
    c = c->next;
  }

  // Make the last node point to newNode instead of null
  c->next = newNode;
}

void LL::printLL(){
  // Case 1: 0 node 
  if(head == NULL){
    cout << "sorry empty list" << endl;
    return;
  }

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

// In place removal of duplicates in a LL (T : O(1))
void LL::removeDuplicates(){
  // Case 1: 0 node in the list
  if(head == NULL)
    cout << "Sorry empty list" << endl;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    cout << "Sorry cant do anything just 1 node in the list" << endl;

  // Case 3: More than 1 nodes
  else{
    Node *i = head;
    while(i != NULL){
      Node *j = i->next;
      Node *p = i;
      while(j != NULL){
        if(i->value == j->value){
          Node *t = j;
          p->next = j->next;
          j = j->next;
          delete t;
        }
        else{
          p = j;
          j = j->next;
        }
      }
      i = i->next;
    }
  }
}

int main(){
  LL *l = new LL();
  l->printLL();
  l->insertAtTheEnd(1);
  l->insertAtTheEnd(2);
  l->insertAtTheEnd(2);
  l->insertAtTheEnd(4);
  l->insertAtTheEnd(4);
  l->insertAtTheEnd(5);
  l->insertAtTheEnd(5);
  
  cout << "original list" << endl;
  l->printLL();

  cout << "LL after removing duplicats" << endl;
  l->removeDuplicates();
  l->printLL();
  return 0;
}
