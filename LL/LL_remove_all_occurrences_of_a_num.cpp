#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void deleteList();
  void removeAllOccurOfElem(Node *n, int v);
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::deleteList(){
  if(head == NULL)
    return;
  else{
    Node *c = head;
    Node *n = NULL;

    while(c != NULL){
      // keep track of next node
      n = c->next;

      // delete the current node
      delete(c);

      // make next node the current node
      c = n;
    }

    // make head point to NULL
    head = NULL;
  }
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}


void LL::removeAllOccurOfElem(Node *n, int v){
  // case 1: 0 node in the list
  if(n == NULL) 
    return;

  // case 2: only 1 node in the list
  if(n->next == NULL){
    if(n->value == v){
      head = NULL;
      delete n;
    }
    else
      return;
  }

  // change head to the first non-v
  Node *c = n;
  while(c != NULL){
    if(c->value != v){
      head = c;
      break;
    }
    c = c->next;
  }

  // case 3: check if all values were 2
  if(c == NULL){
    deleteList(); // delete the whole list in that case
    head = NULL;
  }

  // case 4: now head points to the first non-v node
  c = n;
  Node *p = c;

  // case 6: general case
  while(c != NULL){
    // found the value to be deleted
    if(c->value == v){
      // keep track of the current node that needs to be deleted
      Node *t = c;

      // move c to the next node
      c = c->next;

      // case 5: first node of list needs to be deleted as well
      if(p != NULL)
        p->next = c;

      // delete the node
      delete t;
    }

    // if value not found, keep incrementing
    else{
      p = c;
      c = c->next;
    }
  }

  // case 7: even if v was not in the list, it will just traverse the list
}


int main(){
  LL l1;
  l1.insertAtTheEnd(2);
  l1.insertAtTheEnd(2);
  l1.insertAtTheEnd(8);
  l1.insertAtTheEnd(9);
  l1.insertAtTheEnd(7);
  l1.insertAtTheEnd(2);
  l1.insertAtTheEnd(2);
  l1.insertAtTheEnd(2);
  l1.insertAtTheEnd(6);
  l1.insertAtTheEnd(1);
  l1.insertAtTheEnd(5);
  l1.insertAtTheEnd(2);
  l1.insertAtTheEnd(2);

  l1.printLL();

  l1.removeAllOccurOfElem(l1.head,2);

  l1.printLL();

  return 0;
}