#include <iostream>
#include <stack>
using namespace std;

class Node{
  public: 
    int value;
    Node *next;
    Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
  public:
    Node *head;
    LL();
    void insertAtTheEnd(int v);
    void printList();
    void deleteList();
};
LL::LL(){
  head = NULL;
}

void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // case 0: 0 node in the list
  if(head == NULL){
    head = newNode;
    return;
  }

  // case 1: 1 node in the list
  if(head->next == NULL){
    head->next = newNode;
    return;
  }

  // case 2: more than 1 node in the list
  Node *c = head;

  //traverse to the last node
  while(c->next != NULL){
    c = c->next;
  }

  // now c points to the last node
  c->next = newNode;
}

void LL::printList(){
  if(head == NULL)
    return;

  Node *c = head;

  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }

  cout << endl;
}

void LL::deleteList(){
  if(head == NULL)
    return;

  Node *c = head;
  Node *n = NULL;

  // traverse + delete all the nodes in the list
  while(c != NULL){
    n = c->next; // keep track of the next node
    delete c;
    c = n;
  }

  head = NULL;
}

bool isPalindrome(Node *n){
  if(n == NULL || n->next == NULL)
    return true;

  stack<int> s;

  // traverse the list + put in the stack
  Node *c = n;
  while(c != NULL){
    s.push(c->value);
    c = c->next;
  }

  // traverse again + pop values from stack
  c = n;
  while(s.empty() != true){
    int v = s.top();
    s.pop();
    if(c->value != v)
      return false;

    c = c->next;
  }

  return true;
}

int main(){
  LL *list = new LL();

  list->insertAtTheEnd(1);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(4);
  list->insertAtTheEnd(4);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(1);

  list->printList();

  cout << isPalindrome(list->head) << endl;

  list->deleteList();
}
