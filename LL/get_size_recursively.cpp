// Program to partition a LL around a value x
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  int getSize(Node *n);
  int getSizeHelper(Node *n, int &s);
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

int LL::getSize(Node *n){
  // Case 1: 0 node in the list
  if(head == NULL)
    return 0;

  // Case 2: 1 node in the list
  else if(head->next == NULL)
    return 1;

  // Case 3: More than 1 node in the list
  else{
    int s = 0;
    return getSizeHelper(n,s);
  }
}

int LL::getSizeHelper(Node *n, int &s){
  if(n == NULL)
    return s;

  else{
    s++;
    return getSizeHelper(n->next,s);
  }
}

int main(){
  LL list;
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(8);
  list.insertAtTheEnd(4);
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(6);
  list.insertAtTheEnd(7);
  list.printLL();
  cout << "size: " << list.getSize(list.head) << endl;
  return 0;
}