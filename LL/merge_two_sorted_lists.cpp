//http://stackoverflow.com/questions/10707352/interview-merging-two-sorted-singly-linked-list
#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class List{
public:
  Node *head;
  List();
  void insertAtTheEnd(int v);
  void printList(Node *n);
};
List::List(){
  head = NULL;
}
void List::insertAtTheEnd(int v){
  // Make a node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;
  
  // Case 3: More than 1 node in the list
  else{
    // Get to the last node
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Make the last node point to the newNode
    c->next = newNode;
  }
}

void List::printList(Node *n){
  // Case 1: 0 node
  if(n == NULL)
    cout << "sorry empty list" << endl;

  // Case 2: 1 node 
  else if(n->next == NULL)
    cout << n->value << '\t';

  // Case 3: More than 1 node
  else{
    Node *c = n;
    while(c != NULL){
      cout << c->value << '\t';
      c = c->next;
    }
  }
  cout << endl;
}

Node* merge(Node *a, Node *b){
  if(a == NULL)
    return b;
  else if(b == NULL)
    return a;
  else{
    if(a->value < b->value){
      a = merge(a->next,b);
      return a;
    }
    else{
      b = merge(b->next,a);
      return b;
    }
  }
}

int main(){
  List *a = new List();
  List *b = new List();

  a->insertAtTheEnd(5);
  a->insertAtTheEnd(9);
  a->insertAtTheEnd(15);
  b->insertAtTheEnd(2);
  b->insertAtTheEnd(7);
  b->insertAtTheEnd(12);

  Node *n = merge(a->head,b->head);
  cout << "printing the list" << endl;
  b->printList(n);

  return 0;
}
