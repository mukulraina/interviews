// Program to partition a LL around a value x
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  bool isEmpty();
  void insertAtTheEnd(int v);
  void printLL();
  void partition(int x);
};
LL::LL(){
  head = NULL;
}
bool LL::isEmpty(){
  return head == NULL;
}
void LL::insertAtTheEnd(int v){
  // Make the node
  Node *newNode = new Node(v);

  // Case 1: empty list
  if(isEmpty()){
    head = newNode;
    return;
  }

  Node *c = head;
  while(c->next != NULL){
    c = c->next;
  }
  c->next = newNode;
}

void LL::printLL(){
  if(isEmpty())
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

// Time: O(n) Space: O(n)
void LL::partition(int v){
  // Case 1: 0 nodes in the LL
  if(head == NULL)
    cout << "sorry empty list, cant partition" << endl;

  // Case 2: 1 node in the LL
  else if(head->next == NULL)
    cout << "sorry 1 node in the list, cant partition" << endl;

  // Case 3: More than 1 nodes in the list
  else{
    // Make 2 queues, 1 for smaller values and 1 for bigger values
    queue<int> lower;
    queue<int> upper;

    // Traverse throught the LL and add elements to either lower or upper queue
    Node *c = head;
    while(c != NULL){
      if(c->value < v)
        lower.push(c->value);
      if(c->value > v)
        upper.push(c->value);
      c = c->next;
    }

    // Now first copy the lower elements to the list, then x and then upper elements
    // We are just gonna overwrite the existing LL
    c = head;
    
    // Copy the lower list
    while(lower.empty() != true){
      // get the front value of the queue
      c->value = lower.front();
      // Pop the front
      lower.pop();
      // Increment current pointer
      c = c->next;
    }

    // copy the partition value
    c->value = v;
    c = c->next;

    // Copy the upper queue
    while(upper.empty() != true){
      c->value = upper.front();
      upper.pop();
      c = c->next;
    }
  }
}

int main(){
  LL list;
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(8);
  list.insertAtTheEnd(4);
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(6);
  list.insertAtTheEnd(7);
  cout << "list before partition" << endl;
  list.printLL();
  list.partition(5);
  cout << "list after partition" << endl;
  list.printLL(); 
  return 0;
}