#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void printLL(Node *n){
  if(n == NULL)
    return;

  Node *c = n;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

Node* getMiddleNode(Node *n){
  if(n == NULL)
    return NULL;
  else{
    Node *a = n;
    Node *b = n;

    // go till b is not the last node or b is not null
    while(b != NULL && b->next != NULL){
      a = a->next;
      b = b->next->next;
    }

    return a;
  }
}

// will reflect changes to the actual head
void reverse(Node* &head){
  // if 0 or 1 node, return
  if(head == NULL || head->next == NULL)
    return;
  else{
    Node *c = head;
    Node *p = NULL;
    Node *n = NULL;

    while(c != NULL){
      n = c->next;
      c->next = p;
      p = c;
      c = n;
    }

    head = p;
  }
}

bool compareTwoHalves(Node *a, Node *b){
  if(a == NULL && b == NULL) // both are empty
    return true;
  else if(a == NULL) // only 1 is empty
    return false;
  else if(b == NULL) // only 1 is empty
    return false;
    // none of them are empty
  else{
    Node *p = a;
    Node *q = b;

    // Traverse both the LL (until one of them ends)
    while(p != NULL && q != NULL){
      // if at any point the value of corresponding nodes are not same, return false
      if(p->value != q->value) 
        return false;

      p = p->next;
      q = q->next;
    }

    // if both of them ended at the same time, return true
    // else return false (as one list is longer than the other)
    if(p == NULL && q == NULL)
      return true;
    else
      return false;
  }
}

bool isPalindrome(Node *n){
  // if 0 or 1 node
  if(n == NULL)
    return false;
  else{
    Node *origHead = n;

    // Get middle
    Node *midNode = getMiddleNode(n);

    // Reverse the 2nd half after middle
    reverse(midNode->next);

    // Check if the first and second halves are same 
    // 1->2->3 to 1->2->3 (dont conside the midNode in the first half)
    // if they are then its a palindrome (Return true)
    // if the 2 halves are no same then return false (not palindrome)
    
    // break the list at 3->4 (jus before mid)
    Node *c = origHead;

    while(c->next != midNode){
      c = c->next;
    }

    // now c points to 1 node before the midNode (so just break the list)
    c->next = NULL;

    // so you'll be comparing the following two halves 1->2->3    4->1->2->3
    bool result = compareTwoHalves(origHead,midNode->next);

    // rejoin the LL
    c->next = midNode;

    return result;
  }
}

// NOTE: only works for odd elements, make it work for even too
int main(){

  LL list;
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(3);
  list.insertAtTheEnd(4);
  list.insertAtTheEnd(3);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(1);

  cout << isPalindrome(list.head) << endl;

  LL list2;
  list2.insertAtTheEnd(1);
  list2.insertAtTheEnd(2);
  list2.insertAtTheEnd(3);
  list2.insertAtTheEnd(4);
  list2.insertAtTheEnd(4);
  list2.insertAtTheEnd(3);
  list2.insertAtTheEnd(2);
  list2.insertAtTheEnd(1);

  cout << isPalindrome(list2.head) << endl;
  printLL(list2.head);
  return 0;
}