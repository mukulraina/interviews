// Program to partition a LL around a value x
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void deleteIndex(int i);
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void LL::deleteIndex(int i){
  // Case 1: empty list
  if(head == NULL)
    cout << "sorry empty list" << endl;
  
  // Check if the index is in the list or nor
  else{
    int count = 0;
    Node *p = NULL;
    Node *c = head;
    while(c != NULL){
      if(count == i)
        break;
      count++;
      p = c;
      c = c->next;
    }

    // Case 2: if the index is not in the list
    if(c == NULL)
      cout << "sorry bigger index than the size of the LL" << endl;

    // Now we know we have a index, c is pointing to that node and p is pointing to the prev
    else{
      // if the index is 0
      if(i == 0){
        head = head->next;
        delete c;
      }
      // Index is somewhere in the middle
      else{
        p->next = c->next;
        delete c;
      }
    }
  }
}

int main(){
  LL list;
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(3);
  list.insertAtTheEnd(4);
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(6);
  list.insertAtTheEnd(7);
  list.printLL();
  list.deleteIndex(0);
  list.printLL();
  return 0;
}