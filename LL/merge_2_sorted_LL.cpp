#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void print(Node *n){
  if(n == NULL)
    return;
  else{
    cout << n->value << '\t';
    print(n->next);
  }
}

Node * mergeLists(Node *h1, Node *h2){
  if(h1 == NULL && h2 == NULL)
    return NULL;

  else if(h1 != NULL && h2 == NULL)
    return h1;

  else if(h1 == NULL && h2 != NULL)
    return h2;

  else{
    Node *p = NULL;
    Node *c = NULL;
    Node *k = NULL;
    Node *origHead = NULL;

    // Check which LL's first value is smaller
    if(h1->value < h2->value)
      p = h1;
    else
      p = h2;

    // Make origHead point to p
    origHead = p;

    // Make c point to the next of p
    c = p->next;

    // Make k point to the other list
    if(p == h1)
      k = h2;
    else 
      k = h1;

    // Traverse the list with 'p' n 'c'
    while(c != NULL && k != NULL){
      if(p->value < k->value && k->value < c->value){
        Node *t = k;
        k = k->next;
        p->next = t;
        t->next = c;
        p = p->next;
      }
      else{
        p = p->next;
        c = c->next;
      }
    }

    if(k == NULL)
      return origHead;
    else{ // That means c is NULL
      p->next = k;
      return origHead;
    }
  }
}

int main(){
  LL list1;
  list1.insertAtTheEnd(3);
  list1.insertAtTheEnd(5);
  list1.insertAtTheEnd(9);
  list1.insertAtTheEnd(12);
  
  LL list2;
  list2.insertAtTheEnd(1);
  list2.insertAtTheEnd(6);
  list2.insertAtTheEnd(8);
  list2.insertAtTheEnd(11);

  Node *c = mergeLists(list1.head,list2.head);

  print(c); // can pass NULL to the print() cos i handle that condition there
  
  cout << endl;
  return 0;
}
