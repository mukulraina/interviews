// Program to partition a LL around a value x
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  int getNthNode(int n);
  int getSize();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

int LL::getSize(){
  if(head == NULL)
    return 0;
  else{
    int size = 0;
    Node *c = head;
    while(c != NULL){
      size++;
      c = c->next;
    }
    return size;
  }
}

int LL::getNthNode(int n){
  // Case 1: empty list || or negative index
  if(head == NULL || n < 0)
    return -1;

  else{
    // Case 2: check if the n is bigger than the size
    int size = getSize();

    if(n >= size)
      return -1;

    // Case 3: so n is def somewhere in the LL
    else{
      int k = 0; // K is the node we're currently looking at
      Node *c = head;
      while(c != NULL){
        if(k == n)
          return c->value;
        else{
          k++;
          c = c->next;
        }
      }

      return -1;
    }
  } 
}

int main(){
  LL list;
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(3);
  list.insertAtTheEnd(4);
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(6);
  list.insertAtTheEnd(7);
  list.printLL();
  cout << list.getSize() << endl;
  cout << list.getNthNode(7) << endl;
  return 0;
}