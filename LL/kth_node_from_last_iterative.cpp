#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class List{
public:
  Node *head;
  List();
  void insertAtTheEnd(int v);
  void printList();
  int kToLast(int k);
};
List::List(){
  head = NULL;
}
void List::insertAtTheEnd(int v){
  // Make a node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;
  
  // Case 3: More than 1 node in the list
  else{
    // Get to the last node
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Make the last node point to the newNode
    c->next = newNode;
  }
}

void List::printList(){
  // Case 1: 0 node
  if(head == NULL)
    cout << "sorry empty list" << endl;

  // Case 2: 1 node 
  else if(head->next == NULL)
    cout << head->value << '\t';

  // Case 3: More than 1 node
  else{
    Node *c = head;
    while(c != NULL){
      cout << c->value << '\t';
      c = c->next;
    }
  }
}

int List::kToLast(int k){
  // Case 1: 0 node
  if(head == NULL)
    return -1;

  // Case 2: 1 or more node
  else{
    Node *c = head;
    Node *p = head;
    int i = 0; // i is 0 so that we can use it as a diff b/w 2 pointers
    while(c != NULL){
      if(i >= k)
        p = p->next;
      i++;
      c = c->next;
    }
    if(i < k)
      return -1;
    return p->value;
  }
}

int main(){
  List list;

  
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(7);
  list.insertAtTheEnd(12);
  list.insertAtTheEnd(7);
  list.insertAtTheEnd(16);
  list.insertAtTheEnd(18);
  list.insertAtTheEnd(25);
  list.insertAtTheEnd(11);
  list.insertAtTheEnd(5);
  cout << "printing the list" << endl;
  list.printList();

  cout << endl;

  int k = 20;
  cout << k << "th element from the last" << endl;
  cout << list.kToLast(k) << endl;

  return 0;
}