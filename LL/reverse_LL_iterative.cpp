// Program to partition a LL around a value x
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  bool isEmpty();
  void insertAtTheEnd(int v);
  void printLL();
  void reverse();
};
LL::LL(){
  head = NULL;
}
bool LL::isEmpty(){
  return head == NULL;
}
void LL::insertAtTheEnd(int v){
  // Make the node
  Node *newNode = new Node(v);

  // Case 1: empty list
  if(isEmpty()){
    head = newNode;
    return;
  }

  Node *c = head;
  while(c->next != NULL){
    c = c->next;
  }
  c->next = newNode;
}

void LL::printLL(){
  if(isEmpty())
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void LL::reverse(){
  // Case 1: 0 node
  if(head == NULL)
    cout << "empty list" << endl;

  // Case 2: 1 node
  else if(head->next == NULL)
    cout << "just 1 node in the list" << endl;

  // Case 3: More than 1 node
  else{
    Node *c = head;
    Node *p = NULL;
    Node *n = NULL;
    while(c != NULL){
      n = c->next;
      c->next = p;
      p = c;
      c = n;
    }
    head = p;
  }
}

int main(){
  LL list;
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(8);
  list.insertAtTheEnd(4);
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(6);
  list.insertAtTheEnd(7);
  cout << "list before reverse" << endl;
  list.printLL();
  list.reverse();
  cout << "list after reverse" << endl;
  list.printLL(); 
  return 0;
}