#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void seg(Node *n);
  void moveOddNodesToEndAndMakeFirstEvenHead(Node *n);
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

bool isEven(int v){
  return (v % 2 == 0);
}

Node* getLastNode(Node *n){
  if(n == NULL)
    return NULL;
  else{
    Node *c = n;
    while(c->next != NULL){
      c = c->next;
    }
    return c;
  }
}

void LL::moveOddNodesToEndAndMakeFirstEvenHead(Node *n){
// step 1: move all the odd elements to the end + make the first even node head
  Node *c = n;
  Node *pEven = NULL;
  while(c != NULL){
    if(isEven(c->value))
      break;
    pEven = c;
    c = c->next;
  }

  // no even node was found
  if(c == NULL) 
    return;

  // get the last node in list
  Node *l = getLastNode(n);
  // c points to first even, l points to last node
  Node *origHead = head; // or n
  // make head point to the 1st even
  head = c;
  // make l connect to the origH
  l->next = origHead;
  // break the chain
  pEven->next = NULL;
}

void LL::seg(Node *n){
  if(n == NULL || n->next == NULL)
    return;

  else{
    // so 8 is head and list is like -> 8 12  10  5 4 1 7 6 17  15
    moveOddNodesToEndAndMakeFirstEvenHead(n);

    Node *l = getLastNode(n);
    Node *origLast = getLastNode(n);
    Node *a = head;
    Node *b = head->next;

    while(b != origLast){
      if(isEven(b->value)){
        a = b;
        b = b->next;
      }
      else{
        // gotta move the odd nodes to the end

        // save the next node of b
        Node *bNext = b->next;
        // make last node point to b
        l->next = b;
        // break chain
        b->next = NULL;
        b = bNext;
        a->next = b;
        // update l
        l = l->next;
      }
    }
  }
}


int main(){
  LL list;
  list.insertAtTheEnd(17);
  list.insertAtTheEnd(15);
  list.insertAtTheEnd(8);
  list.insertAtTheEnd(12);
  list.insertAtTheEnd(10);
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(4);
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(7);
  list.insertAtTheEnd(6);

  list.printLL();
  cout << endl;

  list.seg(list.head);

  list.printLL();
  cout << endl;

  return 0;
}