#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void rev(Node *n);
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void printLL(Node *n){
  if(n == NULL)
    return;

  Node *c = n;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

Node *getMiddleNode(Node *n){
  if(n == NULL || n->next == NULL)
    return n;
  else{
    Node *a = n;
    Node *b = n;

    // go till you hit the end or the last node
    while(b != NULL && b->next != NULL){
      a = a->next;
      b = b->next->next;
    }

    return a;
  }
}

void reverse(Node *& h){
  if(h == NULL || h->next == NULL)
    return;
  else{
    Node *p = NULL;
    Node *c = h;
    Node *n = NULL;

    while(c != NULL){
      n = c->next;
      c->next = p;
      p = c;
      c = n;
    }

    h = p;
  }
}

// NOT WORKING - FIX IT
bool isPalindrome(Node *n){
  if(n == NULL || n->next == NULL)
    return true;
  else{
    // get the middle node
    Node *midNode = getMiddleNode(n);

    // reverse the linked list after the middle node
    reverse(midNode->next);

    printLL(n);

    // Compare first half and second half
    Node *f = n;
    Node *s = midNode->next;

    while(s != NULL){
      cout << f->value << s->value << endl;
      if(f->value != s->value)
        return false;
      f = f->next;
      s = s->next;
    }

    return true;
  }
}

int main(){
  LL *list = new LL();

  list->insertAtTheEnd(1);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(4);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(1);

  cout << isPalindrome(list->head) << endl;

  return 0;
}