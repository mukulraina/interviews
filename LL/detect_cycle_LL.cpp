bool LL::isLoop(){
	if(head == NULL || head->next == NULL)
		return false;
	else{
		Node *a = head;
		Node *b = head;

		while(b != NULL && b->next != NULL){
			a = a->next;
			b = b->next->next;
			if(a == b)
				return true;
		}

		// if the program came till here 
		// that means no loop was found
		return false;
	}
}