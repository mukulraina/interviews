// Ask the recruiter what he wants to do if k > size of the LL
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void deleteList();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void LL::deleteList(){
  if(head == NULL)
    return;
  else{
    Node *c = head;
    Node *n = NULL;

    while(c != NULL){
      // keep track of next node
      n = c->next;

      // delete the current node
      delete(c);

      // make next node the current node
      c = n;
    }

    // make head point to NULL
    head = NULL;
  }
}


void removeDuplicates(Node *n){
  if(n == NULL || n->next == NULL)
    return;

  Node *i = n;
  Node *p = NULL;
  Node *c = NULL;

  // Traverse the list
  while(i != NULL){
    p = i;
    c = i->next;

    while(c != NULL){
      if(c->value == i->value){
        Node *t = c;
        c = c->next;
        p->next = c;
        delete t;
      }
      else{
        p = c;
        c = c->next;
      }
    }

    i = i->next;
  }
}

int main(){
  LL list;
  list.insertAtTheEnd(12);
  list.insertAtTheEnd(11);
  list.insertAtTheEnd(12);
  list.insertAtTheEnd(21);
  list.insertAtTheEnd(41);
  list.insertAtTheEnd(43);
  list.insertAtTheEnd(21);

  list.printLL();

  removeDuplicates(list.head);

  list.printLL();

  // always delete list
  //list.deleteList();

  return 0;
}