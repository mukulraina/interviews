#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void printLL(Node *n){
  if(n == NULL)
    return;

  Node *c = n;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

int getSize(Node *n){
  if(n == NULL)
    return 0;
  else{
    int i = 0;
    Node *c = n;

    while(c != NULL){
      i++;
      c = c->next;
    }

    return i;
  } 
}

// not working - fix it
void appendNodesFromEndToFront(Node *h,int k){
  if(h == NULL || k == 0)
    return;
  else{
    Node *origHead = h;
    Node *c = h;
    int i = 1;

    int size = getSize(h);

    cout << size;
  }
}


int main(){
  LL *l1 = new LL();

  l1->insertAtTheEnd(1);
  l1->insertAtTheEnd(2);
  l1->insertAtTheEnd(3);
  l1->insertAtTheEnd(4);
  l1->insertAtTheEnd(5);
  l1->insertAtTheEnd(6);

  int k = 2;

  appendNodesFromEndToFront(l1->head,k);

  return 0;
}
