// Program to reverse a LL
#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  bool isEmpty();
  void insertAtTheEnd(int v);
  void printLL();
  void reverse(Node *node);
};
LL::LL(){
  head = NULL;
}
bool LL::isEmpty(){
  return head == NULL;
}
void LL::insertAtTheEnd(int v){
  // Make the node
  Node *newNode = new Node(v);

  // Case 1: empty list
  if(isEmpty()){
    head = newNode;
    return;
  }

  Node *c = head;
  while(c->next != NULL){
    c = c->next;
  }
  c->next = newNode;
}

void LL::printLL(){
  if(isEmpty())
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void LL::reverse(Node *n){
  if(n == NULL || n->next == NULL)
    return;

  else{
    if(n->next->next == NULL){
      n->next->next  = n;
      head = n->next;
      n->next = NULL;
    }
    else{
      reverse(n->next);
      n->next->next = n;
      n->next = NULL;
    } 
  }
}

int main(){
  LL list;
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(3);
  list.insertAtTheEnd(4);
  list.insertAtTheEnd(5);
  list.printLL();
  list.reverse(list.head);
  list.printLL();
  return 0;
}
