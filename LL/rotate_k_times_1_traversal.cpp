// Ask the recruiter what he wants to do if k > size of the LL
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void rotate(Node *&h, int k){
  if(h == NULL || h->next == NULL || k == 0)
    return;

  else{
    Node *l = NULL;
    Node *kNode = NULL;
    Node *c = h;
    int i = 1;

    // Traverse to the last node of the LL
    while(c->next != NULL){
      if(i == k)
        kNode = c;

      i++;
      c = c->next;
    }

    // Make l point to the last node now
    l = c;

    // Do the pointer stuff
    l->next = h;
    h = kNode->next;
    kNode->next = NULL;
  }
}

int main(){
  LL list;
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(3);
  list.insertAtTheEnd(4);
  list.insertAtTheEnd(5);


  list.printLL();
  
  rotate(list.head,5);

  list.printLL();
  return 0;
}