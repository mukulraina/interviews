// Program to partition a LL around a value x
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void rotate(Node *& head);
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

// Time: O(n)
Node* pointerToLastNode(Node *&head){
  if(head == NULL)
    return NULL;
  else if(head->next == NULL)
    return head;
  else{
    // Traverse to the end
    Node *l = head;
    while(l->next != NULL)
      l = l->next;
    return l;
  }
}

// Time: O(n) if last pointer NOT given
// Time: O(1) if last pointer IS given
void LL::rotate(Node *& head){
  if(head == NULL || head->next == NULL)
    return;

  else{
    Node *l = pointerToLastNode(head);
    l->next = head;
    Node *t = head;
    head = head->next;
    t->next = NULL;
    l = l->next;
  }
}

int main(){
  LL list;
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(3);
  list.insertAtTheEnd(4);
  list.insertAtTheEnd(5);


  list.printLL();
  
  list.rotate(list.head);

  list.printLL();
  return 0;
}