#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  void pushAtTheEnd(int v);
  void printLL();
  void reverse(Node *n);
};

void LL::pushAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: if 0 node in the list
  if(head == NULL)
    head = newNode;

  // Case 2: if 1 node in the list
  else if(head->next == NULL){
    head->next = newNode;
  }

  // Case 3: if more than 1 node
  else{
    Node *c = head;
    // Point c to the last node of the list
    while(c->next != NULL){
      c = c->next;
    }

    c->next = newNode;
  }
}

void LL::printLL(){
  // Case 1: 0 node in the list
  if(head == NULL)
    return;

  // Case 2: 1 node in the list
  else if(head->next == NULL)
    cout << head->value << '\t' << endl;

  else{
    Node *c = head;
    while(c != NULL){
      cout << c->value << '\t';
      c = c->next;
    }
    cout << endl;
  }
}

void LL::reverse(Node *n){
  
}

int main(){
  LL *list = new LL();
  list->pushAtTheEnd(1);
  list->pushAtTheEnd(2);
  list->pushAtTheEnd(3);
  list->pushAtTheEnd(4);
  list->pushAtTheEnd(5);
  list->pushAtTheEnd(6);
  list->pushAtTheEnd(7);
  list->printLL();
  return 0;
}
