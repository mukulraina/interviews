// Program to partition a LL around a value x
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void del(int v);
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void LL::del(int v){
  // Case 1: empty list
  if(head == NULL)
    return;

  // Case 2: delete 1st value
  else if(head->value == v){
    Node *t = head;
    head = head->next;
    delete t;
  }

  // Case 3: value somewhere in the middle
  else{
    Node *c = head;
    Node *p = NULL;
    while(c != NULL){
      if(c->value == v)
        break;
      p = c;
      c = c->next;
    }

    // If value wasnt found c must be finished the whole list + Must be pointing to NULL
    if(c == NULL)
      cout << "sorry value not in LL" << endl;
    
    // c is pointing to the node
    else{
      p->next = c->next;
      delete c;
    }
  }
}

int main(){
  LL list;
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(4);
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(8);


  cout << "printing list now" << endl;
  list.printLL();
  
  list.del(8);

  cout << "printing list now" << endl;
  list.printLL();
  return 0;
}