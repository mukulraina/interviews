// Program to check if a LinkedList is palindrome or not
#include <iostream>
#include <stack>
using std::cout;
using std::endl;
using std::stack;

// Node Class
class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    Node(int value);
    ~Node();
    int getValue();
    Node* getNext();
    void setValue(int value);
    void setNext(Node *next);
    void removeNode(Node *node);
};

Node::Node(){
  next = NULL;
}
Node::Node(int value){
  this->value = value;
}
Node::~Node(){
  cout << "Node with value: " << value << " is deleted" << endl;
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int value){
  this->value = value;
}
void Node::setNext(Node *next){
  this->next = next;
}

// List Class
class List{
  public:
    Node *head;
    List();
    void printList();
    void insertAtTheEnd(int value);
    bool isPalindrome();
    int getSize();
};

List::List(){
  head = NULL;
}

void List::printList(){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  else if(head->getNext() == NULL){
    cout << head->getValue() << endl;
  }
  else{
    Node *temp = head;
    while(temp != NULL){
      cout << temp->getValue() << endl;
      temp = temp->getNext();
    }
  }
}

void List::insertAtTheEnd(int value){
  Node *newNode = new Node(value);
  if (head == NULL){
    head = newNode;
  }
  else if (head->getNext() == NULL){
    head->setNext(newNode);
  }
  else{
    Node *temp = head;
    while (temp->getNext() != NULL){
      temp = temp->getNext();
    }
    temp->setNext(newNode);
  }
}

int List::getSize(){
  if(!head)
    return 0;

  Node *current = head;
  int size = 0;
  while(current){
    size++;
    current = current->getNext();
  }
  return size;
}

bool List::isPalindrome(){
  // Case 1: 0 nodes in the list
  if(head == NULL)
    return false;

  // Case 2: 1 node in the list
  else if(head->getNext() == NULL)
    return true;

  // Case 3: More than 1 node in the list
  else{
    // Make a stack to store values
    stack<int> s;
    // Push elements onto the stack
    Node *c = head;
    while(c != NULL){
      s.push(c->getValue());
      c = c->getNext();
    }

    // Now traverse again through the LL and compare
    c = head;
    while(c != NULL){
      if(c->getValue() == s.top())
        s.pop();
      c = c->getNext();
    }

    // Now the stack shoule be empty if its palindrome
    return s.empty();
  }
}

int main(){
  List *list = new List();

  /*
  list->insertAtTheEnd(5);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(6);
  list->insertAtTheEnd(6);
  list->insertAtTheEnd(3);
  //list->insertAtTheEnd(5);
  */

  list->insertAtTheEnd(5);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(6);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(5);
  list->insertAtTheEnd(6);
  list->insertAtTheEnd(3);
  
  cout << list->getSize() << endl;
  cout << "Result: " << list->isPalindrome() << endl;
  return 0;
}
