#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void pairwiseSwap();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void swap(Node *a, Node *b){
  if(a == NULL || b == NULL)
    return;
  else{
    int t = a->value;
    a->value = b->value;
    b->value = t;
  }
}

void pairSwapIterative(Node *n){
  if(n == NULL || n->next == NULL)
    return;
  else{
    Node *a = n;
    Node *b = n->next;

    while(b != NULL){
      swap(a,b);
      a = b->next;
      b = a->next;
    }
  }
}

void LL::pairwiseSwap(){
  if(head == NULL || head->next == NULL) // return if 0 or 1 node in the list
    return;
  else{ // so we know there are atleast 2 nodes in the list
    Node *c = head->next; // point to 2 
    Node *p = head; // point to 1

    // update head already
    head = c; // make it point to the 2nd node already

    while(true){
      Node *n = c->next;
      
      // make 2 point to 1
      c->next = p;

      if(n == NULL || n->next == NULL){
        p->next = n;
        break;
      }

      // make 1 point to 4
      p->next = n->next;

      // update p and c
      p = n;
      c = p->next;
    }
  }
}

int main(){
  LL l1;
  l1.insertAtTheEnd(1);
  l1.insertAtTheEnd(2);
  l1.insertAtTheEnd(3);
  l1.insertAtTheEnd(4);
  l1.insertAtTheEnd(5);
  l1.insertAtTheEnd(6);
  l1.insertAtTheEnd(7);

  l1.printLL();

  l1.pairwiseSwap();

  l1.printLL();

  return 0;
}