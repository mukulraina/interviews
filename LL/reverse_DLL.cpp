void DLL::reverse(){
	// if only 0 or 1 node in the list, return
	if(head == NULL || head->next == NULL)
		return;
	else{
		Node *c = head;
		Node *t = NULL;

		while(c != NULL){
			t = c->prev;
			c->prev = c->next;
			c->next = t;
			c = c->prev;
		}

		// change head
		if(t != NULL)
			head = t->prev;
	}
}