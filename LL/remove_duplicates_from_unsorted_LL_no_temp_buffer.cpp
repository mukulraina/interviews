#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void rev(Node *n);
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

// NOT WORKING - FIX IT
void deleteDuplicates(Node *n){
  if(n == NULL || n->next == NULL)
    return;
  else{
    Node *a = n;
    while(a != NULL){
      Node *p = a;
      Node *q = q->next;

      while(q != NULL){
        if(q->value == a->value){
          Node *t = q;
          p->next = q->next;
          q = q->next;
          delete t;
        }
        else{
          p = q;
          q = q->next;
        }
      }
    }
  }
}

int main(){
  LL *list = new LL();

  list->insertAtTheEnd(1);
  list->insertAtTheEnd(12);
  list->insertAtTheEnd(1);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(5);
  list->insertAtTheEnd(6);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(4);
  list->insertAtTheEnd(1);
  list->insertAtTheEnd(5);

  cout << "list before deletion" << endl;
  list->printLL();

  deleteDuplicates(list->head);

  cout << "list after deletion" << endl;
  list->printLL();

  return 0;
}