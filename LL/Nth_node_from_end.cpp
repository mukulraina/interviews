// Program to partition a LL around a value x
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  int getNthNodeFromEnd(int size, int n);
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

int LL::getNthNodeFromEnd(int size, int n){
  // Case 1: empty list
  if(head == NULL)
    return -1;

  else{
    if(n > size)
      return -1;

    else{
      Node *b = head;
      Node *a = NULL;
      int k = 0;
      while(b != NULL){
        if(k == n-1)
          a = head;
        if(k > n-1)
          a = a->next;
        k++;
        b = b->next;
      }
      return a->value;
    }
  }
}

int main(){
  LL list;
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(3);
  list.insertAtTheEnd(4);
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(6);
  list.insertAtTheEnd(7);
  list.printLL();
  int n = 7;
  int size = 7;
  cout << n <<"rd node from behind: " << list.getNthNodeFromEnd(size,n) << endl;
  return 0;
}