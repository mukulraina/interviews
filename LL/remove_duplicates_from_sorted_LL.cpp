#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void removeDuplicates(Node *n){
  if(n == NULL || n->next == NULL)
    return;

  else{
    Node *a = n;
    Node *b = n->next;

    while(b != NULL){
      if(a->value == b->value){
        Node *t = b;
        b = b->next;
        a->next = b;
        delete t;
      }
      else{
        a = b;
        b = b->next;
      }
    }
  }
}

int main(){
  LL list;
  list.insertAtTheEnd(11);
  list.insertAtTheEnd(11);
  list.insertAtTheEnd(11);
  list.insertAtTheEnd(21);
  list.insertAtTheEnd(43);
  list.insertAtTheEnd(43);
  list.insertAtTheEnd(60);
  list.insertAtTheEnd(60);

  list.printLL();
  cout << endl;

  removeDuplicates(list.head);

  list.printLL();
  cout << endl;

  return 0;
}