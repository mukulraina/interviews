// Program to partition a LL around a value x
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void rotate(Node *n,int k);
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

Node *getLastNode(Node *n){
  if(n == NULL)
    return NULL;
  else{
    Node *c = NULL;
    
    while(c->next != NULL){
      c = c->next;
    }
    
    return c;
  }
}

Node *getKNode(Node *n, int k){
  if(n == NULL)
    return NULL;
  else{
    int i = 1;
    Node *c = n;

    while(c != NULL){
      if(i == k)
        break;

      i++;
      c = c->next;
    }

    return c;
  }
}

void LL::rotate(Node *n,int k){
  if(n == NULL || n->next == NULL)
    return;
  else{
    // get the kth Node
    Node *kNode = getKNode(n,k);

    if(kNode == NULL)
      return;

    Node *origHead = n;

    Node *lastNode = getLastNode(origHead);

    // make last node point to the original head
    lastNode->next = origHead;

    // change the head of the list to newHead
    head = kNode->next;

    // break the LL at 4 (point of rotation)
    kNode->next = NULL;
  }
}

int main(){
  LL list;
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(3);
  list.insertAtTheEnd(4);
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(6);
    cout << "a";
    cout << "a";
  list.printLL();
      cout << "a";
  list.rotate(list.head,4);

  list.printLL();
  return 0;
}
