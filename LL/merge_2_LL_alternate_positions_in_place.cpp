#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void seg(Node *n);
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void mergeInPlace(Node *&h1, Node *&h2){
  if(h1 == NULL || h2 == NULL)
    return;
  else{
    Node *a = h1;
    Node *b = h2;
    Node *aN = NULL;
    Node *bN = NULL;

    while(a != NULL && b != NULL){
      // save next pointers
      aN = a->next;
      bN = b->next;

      a->next = b;
      b->next = aN;

      a = aN;
      b = bN;
    }
    // change head of the 2nd list
    h2 = b;
  }
}

// NOT WORKING - fix it
Node* mergeExtraSpace(Node *h1, Node *h2){
  if(h1 == NULL)
    return h2;
  else if(h2 == NULL)
    return h1;
  else{
    h1->next = h2;
    h2->next = mergeExtraSpace(h1->next,h2->next);
  }
  return h1;
}


int main(){
  LL *l1 = new LL();
  LL *l2 = new LL();

  l1->insertAtTheEnd(5);
  l1->insertAtTheEnd(7);
  l1->insertAtTheEnd(11);

  l2->insertAtTheEnd(12);
  l2->insertAtTheEnd(10);
  l2->insertAtTheEnd(17);
  l2->insertAtTheEnd(4);

  l1->printLL();
  l2->printLL();

  //mergeInPlace(l1->head,l2->head);
  l1->head = mergeExtraSpace(l1->head,l2->head);

  l1->printLL();

  return 0;
}