#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void printLL(Node *n){
  if(n == NULL)
    return;

  Node *c = n;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

// NOT WORKING - FIX IT
void merge(Node * h1, Node * h2){
  if(h1 == NULL || h2 == NULL)
    return;
  else{
    Node *p1 = h1;
    Node *c1 = h1->next;

    while(c1 != NULL){
      p1->next = h2;
      h2 = h2->next;
      p1->next->next = c1;
      p1 = c1;
      c1 = c1->next;
    }

    p1->next = h2;
    h2 = h2->next;
    p1->next->next = NULL;
  }
}


int main(){
  LL *l1 = new LL();
  LL *l2 = new LL();

  l1->insertAtTheEnd(1);
  l1->insertAtTheEnd(2);
  l1->insertAtTheEnd(3);

  l1->insertAtTheEnd(4);
  l1->insertAtTheEnd(5);
  l1->insertAtTheEnd(6);
  l1->insertAtTheEnd(7);
  l1->insertAtTheEnd(8);

  merge(l1->head,l2->head);
  cout << "list 1:\n";
  //printLL(l1->head);
  cout << "list 2:\n";
  //printLL(l2->head);

  return 0;
}
