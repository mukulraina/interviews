#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node();
  Node(int v);
};
Node::Node(){
  next = NULL;
}
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void rotate(int k);
};
LL::LL(){
  head = NULL;
}
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node
  if(head == NULL){
    head = newNode;
    return;
  }

  // Case 2: 1 or more nodes
  Node *c = head;
  // Traverse c so that it points to the last node
  while(c->next != NULL){
    c = c->next;
  }

  // Make the last node point to newNode instead of null
  c->next = newNode;
}

void LL::printLL(){
  // Case 1: 0 node 
  if(head == NULL){
    cout << "sorry empty list" << endl;
    return;
  }

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void LL::rotate(int k){
  if(k == 0 || head == NULL)
    return;
  else{
    // Get a pointer to point to the last node of the list
    Node *l = head;
    while(l->next != NULL)
      l = l->next;

    //rotate
    int i = 0;
    Node *t;
    while(i != k){
      t = head;
      l->next = head;
      head = head->next;
      t->next = NULL;
      l = l->next;
      i++;
    }
  }
}

int main(){
  LL *l = new LL();
  l->printLL();
  l->insertAtTheEnd(1);
  l->insertAtTheEnd(2);
  l->insertAtTheEnd(3);
  l->insertAtTheEnd(4);
  l->insertAtTheEnd(5);
  l->insertAtTheEnd(6);
  cout << "original list" << endl;
  l->printLL();

  l->rotate(4);
  l->printLL();
  return 0;
}
