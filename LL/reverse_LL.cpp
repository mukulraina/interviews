x// Program to partition a LL around a value x
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void rev(Node *n);
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void LL::rev(Node *n){
  // Case 1: 0 node in list
  if(n == NULL)
    return;

  // Case 2: 1 node in the list
  else if(n->next == NULL)
    return;

  // Case 3: 2 or more nodes
  else{
    if(n->next->next == NULL){
      n->next->next = n;
      head = n->next;
      n->next = NULL;
    }
    else{
      rev(n->next);
      n->next->next = n;
      n->next = NULL;
    }
  }
}

int main(){
  LL list;
  list.insertAtTheEnd(1);


  list.printLL();
  list.rev(list.head);
  list.printLL();
  return 0;
}