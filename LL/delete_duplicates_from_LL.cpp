#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void removeDuplicates(Node *&h){
  if(h == NULL || h->next == NULL)
    return;

  else{
    // Declare 2 running pointers
    Node *c = h;
    Node *p = NULL;

    // Make the hash + iterator
    Mymap m;
    Mymap::iterator iter;

    // Traverse the list
    while(c != NULL){
      iter = m.find(c->value);

      // Value not in hash, insert it
      if(iter == m.end()){
        m.insert(Mymap::value_type(c->value,1));
        p = c;
        c = c->next;
      }

      // Value already in hash (so duplicates, delete it)
      else{
        Node *t = c;
        c = c->next;
        p->next = c;
        delete t;
      }
    }
  }
}

int main(){
  LL list;
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(7);
  list.insertAtTheEnd(8);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(7);
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(8);

  list.printLL();
  
  removeDuplicates(list.head);

  list.printLL();
  
  return 0;
}