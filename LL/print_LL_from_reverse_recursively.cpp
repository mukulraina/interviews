#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class List{
public:
  Node *head;
  List();
  void insertAtTheEnd(int v);
  void printList();
  void printFromReverse(Node *n);
};
List::List(){
  head = NULL;
}
void List::insertAtTheEnd(int v){
  // Make a node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;
  
  // Case 3: More than 1 node in the list
  else{
    // Get to the last node
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Make the last node point to the newNode
    c->next = newNode;
  }
}

void List::printList(){
  // Case 1: 0 node
  if(head == NULL)
    cout << "sorry empty list" << endl;

  // Case 2: 1 node 
  else if(head->next == NULL)
    cout << head->value << '\t';

  // Case 3: More than 1 node
  else{
    Node *c = head;
    while(c != NULL){
      cout << c->value << '\t';
      c = c->next;
    }
  }
}

void List::printFromReverse(Node *n){
  if(n == NULL)
    return;

  printFromReverse(n->next);
  cout << n->value << '\t';
  return;
}

int main(){
  List list;

  
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(7);
  list.insertAtTheEnd(12);
  list.insertAtTheEnd(7);
  list.insertAtTheEnd(16);
  list.insertAtTheEnd(18);
  list.insertAtTheEnd(25);
  list.insertAtTheEnd(11);
  list.insertAtTheEnd(5);
  cout << "printing the list" << endl;
  list.printList();
  cout << endl;

  cout << "Printing the list from the reverse" << endl;
  list.printFromReverse(list.head);
  cout << endl;
  return 0;
}