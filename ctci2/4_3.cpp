// Program to convert a sorted array to BST
Node* convert(int *a,int l, int h){
	if(l > h)
		return NULL;
	else{
		// get middle
		int m = (l + h) / 2;

		// make root node
		Node *r = new Node(a[m]);

		// construct left subtree recursively 
		r->left = convert(a,0,m-1);

		// construct right subtree recursively
		r->right = convert(a,m+1,h);

		return r;
	}
}