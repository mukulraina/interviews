#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

class Node{
public:
  int value;
  Node *next;
  Node();
  Node(int v);
};
Node::Node(){
  next = NULL;
}
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void removeDuplicates();
};
LL::LL(){
  head = NULL;
}
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node
  if(head == NULL){
    head = newNode;
    return;
  }

  // Case 2: 1 or more nodes
  Node *c = head;
  // Traverse c so that it points to the last node
  while(c->next != NULL){
    c = c->next;
  }

  // Make the last node point to newNode instead of null
  c->next = newNode;
}

void LL::printLL(){
  // Case 1: 0 node 
  if(head == NULL){
    cout << "sorry empty list" << endl;
    return;
  }

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

/*
  Logic:
    traverse 2 pointers make c insert in the hash
    if already there delete c
      for deleting make p point to c's next n stuff
    if not insert c in the hash
*/
void removeDuplicates(Node *n){
  if(n == NULL || n->next == NULL)
    return;
  else{
    Mymap m;
    Mymap::iterator iter;

    Node *p = NULL;
    Node *c = n;

    while(c != NULL){
      iter = m.find(c->value);

      if(iter == m.end()){ // not found in hash, so insert it
        m.insert(Mymap::value_type(c->value),1);
        p = c;
        c = c->next;
      }
      else{ // found in the hash
        Node *t = c;
        p->next = c->next;
        c = c->next;
        delete t;
      }
    }
  }
}

int main(){
  LL *l = new LL();
  l->printLL();
  l->insertAtTheEnd(12);
  l->insertAtTheEnd(11);
  l->insertAtTheEnd(12);
  l->insertAtTheEnd(21);
  l->insertAtTheEnd(41);
  l->insertAtTheEnd(43);
  l->insertAtTheEnd(21);
  
  cout << "original list" << endl;
  l->printLL();

  cout << "LL after removing duplicats" << endl;
  l->removeDuplicates();
  l->printLL();
  return 0;
}