#include <iostream>
#include <stdlib.h>
#include <queue>
using namespace std;

class Node{
  public:
    int value;
    Node *next;
    Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
  public:
    Node *head;
    LL();
    void insert(int v);
    void print();
    void partition(int v);
};
LL::LL(){
  head = NULL;
}
void LL::insert(int v){
  // make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in list
  if(head == NULL)
    head = newNode;
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;
  // Case 3: traverse till the end
  else{
    Node *c = head;
    while(c->next != NULL)
      c = c->next;
    c->next = newNode;
  }
}
void LL::print(){
  if(head == NULL)
    return;
  else{
    Node *c = head;
    while(c != NULL){
      cout << c->value << '\t';
      c = c->next;
    }
    cout << endl;
  }
}

// Time: O(n) Space: O(n)
// Logic:
// put smaller values in the smaller queue
// put bigger values in the bigger queue
// then overwrite the original linked list (smallerQ + parition value + biggerQ)
void LL::partition(int v){
  if(head == NULL || head->next == NULL) // if there are 0 or 1 nodes in the LL
    return;
  else{
    queue<int> smallerQ;
    queue<int> biggerQ;

    // traverse the list
    Node *c = head;
    while(c != NULL){
      if(c->value < v)
        smallerQ.push(c->value);
      if(c->value > v)
        biggerQ.push(c->value);
      c = c->next;
    }

    // overwrite the list
    c = head;
    while(smallerQ.empty() != true){
      c->value = smallerQ.front();
      smallerQ.pop();
      c = c->next;
    }

    c->value = v;
    c = c->next;

    while(biggerQ.empty() != true){
      c->value = biggerQ.front();
      biggerQ.pop();
      c = c->next;
    }
  }
}

int main(){
  LL *list = new LL();
  if( !(list  = new LL()))
  {
     cout << "Error: out of memory." <<endl;
     exit(1);
  }

  list->insert(2);
  list->insert(8);
  list->insert(4);
  list->insert(5);
  list->insert(1);
  list->insert(2);
  list->insert(6);
  list->insert(7);

  list->print();

  list->partition(5);

  list->print();
  return 0;
}