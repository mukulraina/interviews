#include <iostream>
using namespace std;

bool checkAnagram(string a, string b){
  if(a.size() != b.size())
    return false;
  else{
    //if they have the same character count then they are anagram
    int charCount1[256] = {0}; // all characters have 0 # of occurences initially
    int charCount2[256] = {0}; 

    // traverse the first string and populate its charCount
    for(int i = 0; i < a.size(); i++){
      int v = a[i];
      charCount1[v]++;
    }

    // traverse the 2nd string
    for (int i = 0; i < b.size(); ++i){
      int v = b[i];
      charCount2[v]++;
    }
    
    // compare the 2 charCount arrays
    for(int i = 0; i < 256; i++){
      if(charCount1[i] != charCount2[i])
        return false;
    }

    return true;
  }
}

// Test Client
int main(){
  // Normal Case
    std::string s1 = "mukul";
    std::string s2 = "papel";
    std::cout<<checkAnagram(s1,s2)<<'\n';

    std::string s3 = "madam god";
    std::string s4 = "dog madam";
    std::cout<<checkAnagram(s3,s4)<<'\n';
  
  // Extreme Case
    std::string s5 = "";
    std::string s6 = "dog madam";
    std::cout<<checkAnagram(s5,s6)<<'\n';

    std::string s7 = "dog madam";
    std::string s8 = "";
    std::cout<<checkAnagram(s7,s8)<<'\n';
    
    // large

  // Null / illegal 

  // Strange input

  return 0;
}