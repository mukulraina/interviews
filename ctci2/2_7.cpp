#include <iostream>
#include <stack>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  bool isPalindrome();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}


// using stack so space will be O(n) instead of O(1)
bool LL::isPalindrome(){
	if(head == NULL)
		return false;
	else{
		// Traverse the list and put all elements into stack
		Node *c = head;
		stack<int> stackLL;

		while(c != NULL){
			stackLL.push(c->value);
			c = c->next;
		}

		// now pop from list and compare
		c = head;
		while(c != NULL){
			if(c->value != stackLL.top())
				return false;
			stackLL.pop();
			c = c->next;
		}

		// if program came till here that means it was palindrome
		return true;
	}
}

int main(){

  LL list;
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(3);
  list.insertAtTheEnd(4);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(1);

  list.printLL();
  cout << list.isPalindrome() << endl;

  return 0;
}