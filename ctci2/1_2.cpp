#include <iostream>
using namespace std;

void swap(char *a, char *b){
  char temp = *a;
  *a = *b;
  *b = temp;
}

void reverseString(char *s){
  if(s == NULL) // empty string
    return;
  else{
    // get a pointer pointing to the last character
    char *l = s;

    while(*l != '\0')
      l++;

    // decrement once cos right now its pointing to the null char
    l--; // now points to the last char

    while(s < l){
      swap(s,l);
      s++;
      l--;
    }
  }
}

// Test Client
int main(){
  // Normal Case
    // 0 space
    char s1[] = {"Mukul"};
    reverseString(s1);
    std::cout<<s1<<'\n';

    // 1 space
    char s2[] = "Mukul Raina";
    reverseString(s2);
    std::cout<<s2<<'\n';

  // Extreme Case
    // 0
    char s3[] = "";
    reverseString(s3);
    std::cout<<s3<<'\n';

    // 1
    char s4[] = "a";
    reverseString(s4);
    std::cout<<s4<<'\n';

  // Null / illegal 
    // no -ve n in fibonacci 

  // Strange input
    // Already sorted
    // Reverse sorted
  return 0;
}