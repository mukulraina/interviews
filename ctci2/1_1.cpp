#include <iostream>
using namespace std;

// checks if a string has all unique characters
bool isUnique(string s){
  if(s.size() == 0 || s.size() > 256)
    return false;
  else{
    // Make an array for character count
    // size will be 256 as there are 256 unique ascii chars
    // and all of them are intially false (will turn true if they are found in string)
    bool countArray[256] = {false};

    // traverse the string
    for(int i = 0; i < s.size(); i++){
      // get the ascii value
      int asciiValue = s[i];

      // check the corresponding ascii value in the count array
      if(countArray[asciiValue]) // that means already was found (not unique)
        return false;
      else
        countArray[asciiValue] = true; // if not found, mark it found
    }

    // if the program reached here then no char was found to be repeated, so return true
    return true;
  }
}

// Test Client
int main(){
  std::cout<<isUnique("mukul")<<'\n';
  std::cout<<isUnique("abcdef")<<'\n';
  
  return 0;
}