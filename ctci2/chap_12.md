**Table of Contents**
  1. 4 types of testing questions
  - 5 different areas you need to understand
  - Testing a real world object
  - Testing a piece of software
  - Testing a function
  - Troubleshooting Questions

**4 types of testing questions**

  1. Test a real world object
  - Test a piece of software
  - Write test code for a function
  - Troubleshoot an existing issue
  
**What interviewer is looking for**
  1. Can come up with test cases
  - Understands big picture
  - Knows how different software pieces fit together
  - Organization (Do you approach the problem in a structred manner)
  - Practicality (are the test cases reasonable)
  
**Testing a real world object**
  1. Users + motivation
  - What are the use cases
  - What are the bounds of the use
  - What are the stress/failure conditions
  - test cases + How would you perform the testing
  
**Testing a piece of software**
  1. Types of testing (manual vs automted + blackbox vs whitebox)
  - Users + motivation
  - What are the use cases
  - What are the bounds of the use
  - What are the stress/failure conditions
  - test cases + How would you perform the testing
  
**Testing a function**
  1. Define the test cases
    - Normal case
    - Exteme cases
    - Null and illegal input
    - String input
  - Define the expected result 
  - Write test code
  
**Troubleshooting questions**
  1. Understand the scenario
  - Break down the problem
  - Create specific, manageable tests
