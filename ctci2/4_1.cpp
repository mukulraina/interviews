int getHeight(Node *n){
	if(n == NULL)
		return 0;
	else{
		// check if left subtree is balanced
		int left = getHeight(n->left);
		if(left == -1)
			return -1; // if left subtree is not balanced return false;

		// check if right subtree is balanced
		int right = getHeight(n->right)
		if(right == -1)
			return -1;

		// check if the current node is balanced
		int diff = abs(l - r);
		if(diff > 1)
			return -1; // if diff of left and right subtree is more than 1, its not balanced
		else
			return (max(left,right) + 1);
	}
}

bool isBalanced(Node *n){
	if(getHeight(n) == -1)
		return false;
	else
		return true;
}