#include <iostream>
#include <stack>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};

Node::Node(int v){
  value = v;
}

class stackLL{
public:
  Node *head;
  stackLL();
  stack<int> minStack;
  void push(int v);
  int pop();
  void print();
  int min();
};

stackLL::stackLL(){
  head = NULL;
}

// Time: O(1)
void stackLL::push(int v){
  // Make a new node
  Node *newNode = new Node(v);

  if(minStack.empty() != true){
    // check if we have to put it in minStack
    int minVal = minStack.top();

    if(v < minVal){
      minStack.push(v);
    }  
  }
  else{
    minStack.push(v);
  }
  
  // if 0 node
  if(head == NULL)
    head = newNode;

  // More than 0 nodes
  else{
    newNode->next = head;
    head = newNode;
  }
}

// Time: O(1)
int stackLL::pop(){
  // if empty list
  if(head == NULL)
    return -1;

  else{
    int v = head->value;
    Node *t = head;
    head = head->next;
    delete t;

    // pop it from minStack if required
    int minVal = minStack.top();

    if(v == minVal)
      minStack.pop();

    return v;
  }
}

// Time: O(n)
void stackLL::print(){
  // if empty stack
  if(head == NULL)
    cout << "sorry empty stack" << endl;

  else{
    Node *c = head;
    while(c != NULL){
      cout << c->value << endl;
      c = c->next;
    }
  }
}

int stackLL::min(){
  if(minStack.empty() != true)
    return minStack.top();
  else
    return -1;
}

int main(){
  stackLL s;
  s.push(7);
  s.push(2);
  s.push(5);
  s.push(1);
  s.push(3);
  s.push(8);
  s.push(2);

  s.print();
  cout << "min: " <<  s.min() << endl;

  s.pop();
  s.pop();
  s.pop();
  s.pop();
 cout << "min: " <<  s.min() << endl;

 
  return 0;
}