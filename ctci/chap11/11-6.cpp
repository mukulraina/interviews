//http://www.geeksforgeeks.org/search-in-row-wise-and-column-wise-sorted-matrix/
#include <iostream>
using namespace std;

void search(int mat[4][4],int n, int x){
  int i = 0;
  int j = n - 1; // set index of the top right element
  
  while(i < n && j >= 0){
    if(mat[i][j] == x){
      cout << "index: " << i << ":" << j << endl;
      return;
    }   
    else if(x < mat[i][j])
      j--;
    else
      i++;
  }
  cout << "sorry element wasnt found" << endl;
}

int main(){
  int mat[4][4] = { {10, 20, 30, 40},
                  {15, 25, 35, 45},
                  {27, 29, 37, 48},
                  {32, 33, 39, 50},
                };
  search(mat,4,29);
  return 0;
}
