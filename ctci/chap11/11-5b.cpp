// Brute force approach
#include <iostream>
#include <vector>
using namespace std;


// Time: O(log n) - Iterative Binary Search
int locateIterative(vector<string> a, string s, int first, int last){
  while(first <= last){
    int mid = (first + last) / 2;

    //Lets check if mid is an empty string
    if(a[mid] == ""){
      int left = mid - 1; // Assign left to point to 1 element before mid
      int right = mid + 1;  // Assign right to point to 1 element after mid
      
      // Keep going until you find an element(either on left or right) that is not ""
      while(true){
        
        // That means all the strings are ""
        if(first > left && right > last)
          return -1;

        // Check the right element
        else if(right <= last && a[right] != ""){
          mid = right;
          break;
        }

        // Check the left element
        else if(left >= first && a[left] != ""){
          mid = left;
          break;
        }

        // Say both the right and left to the mid were "" so
        // We move right to the righ(right++) and left to the left (left--)
        right++;
        left--;
      }
    }   

    // So now mid points to an empty string
    // Now its all good old binary search logic
    if(a[mid] == s)
      return mid;
    // Search left
    else if(s < a[mid])
      last = mid -1;
    // Search right
    else
      first = mid + 1;
  }
  // If first crossed last, that means element was not found in the array
  return -1;
}

int locateRecursive(vector<string> a, string s, int first, int last){
 if(first > last)
  return -1;

 else{
    int mid = (first + last) / 2;

    //Lets check if mid is an empty string
    if(a[mid] == ""){
      int left = mid - 1; // Assign left to point to 1 element before mid
      int right = mid + 1;  // Assign right to point to 1 element after mid
      
      // Keep going until you find an element(either on left or right) that is not ""
      while(true){
        
        // That means all the strings are ""
        if(first > left && right > last)
          return -1;

        // Check the right element
        else if(right <= last && a[right] != ""){
          mid = right;
          break;
        }

        // Check the left element
        else if(left >= first && a[left] != ""){
          mid = left;
          break;
        }

        // Say both the right and left to the mid were "" so
        // We move right to the righ(right++) and left to the left (left--)
        right++;
        left--;
      }
    }   

    // So now mid points to an empty string
    // Now its all good old binary search logic
    if(a[mid] == s)
      return mid;
    // Search left
    else if(s < a[mid])
      return locateRecursive(a,s,first,mid-1);
    // Search right
    else
      return locateRecursive(a,s,mid+1,last);
  }
}

void printVector(vector<string> v){
  for(int i = 0; i < v.size(); i++){
    cout << v[i] << endl;
  }
}

int main(){
  vector<string> v;
  v.push_back("aadad");
  v.push_back("");
  v.push_back("basdasda");
  v.push_back("");
  v.push_back("");
  v.push_back("casdasd");
  cout << "vector: " << endl;
  printVector(v);
  int l = 0;
  int h = v.size() - 1;
  cout << locateRecursive(v,"basdasda",l,h) << endl;
  cout << locateRecursive(v,"muku",l,h) << endl;
  return 0;
}