#include <iostream>
using namespace std;

int search(int *a, int left, int right, int key){
  int L = left;
  int R = right;
 
  while (L <= R) {
    // Avoid overflow, same as M=(L+R)/2
    int M = L + ((R - L) / 2);
    if (A[M] == key) return M;
 
    // the bottom half is sorted
    if (A[L] <= A[M]) {
      if (A[L] <= key && key < A[M])
        R = M - 1;
      else
        L = M + 1;
    }
    // the upper half is sorted
    else {
      if (A[M] < key && key <= A[R])
        L = M + 1;
      else 
        R = M - 1;
    }
  }
  return -1;
}

int main(){
  int a[] = {10,15,20,0,5};
  int b[] = {50,5,20,30,40};

  int l = 0;
  int h = 4;
  int x = 5;
  cout << search(a,l,h,x) << endl;
  cout << search(b,l,h,x) << endl;
}