// Brute force approach
#include <iostream>
#include <vector>
using namespace std;


// Brute Force method - O(n)
int locate(vector<string> v, string s){
  if(v.size() == 0)
    return -1;
  else{
    for(int i = 0; i < v.size(); i++){
      if(v[i] == s)
        return i;
    }
    return -1;
  }
}

void printVector(vector<string> v){
  for(int i = 0; i < v.size(); i++){
    cout << v[i] << endl;
  }
}

int main(){
  vector<string> v;
  v.push_back("aadad");
  v.push_back("");
  v.push_back("basdasda");
  v.push_back("");
  v.push_back("");
  v.push_back("casdasd");
  cout << "vector: " << endl;
  printVector(v);
  cout << locate(v,"basdasda") << endl;
  cout << locate(v,"muku") << endl;
  return 0;
}