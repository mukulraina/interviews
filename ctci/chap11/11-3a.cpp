#include <iostream>
using namespace std;

int search(int *a, int left, int right, int x){
  int mid = (left + right) / 2;
  if (x == a[mid]) { // Found element
      return mid;
  }
  if (right < left) {
      return -1;
  }
  
  /* While there may be an inflection point due to the rotation, either the left or 
   * right half must be normally ordered.  We can look at the normally ordered half
   * to make a determination as to which half we should search. 
   */
  if (a[left] < a[mid]) { // Left is normally ordered.
      if (x >= a[left] && x <= a[mid]) { 
          return search(a, left, mid - 1, x);
      } else {
          return search(a, mid + 1, right, x);
      }
  } else if (a[mid] < a[left]) { // Right is normally ordered.
      if (x >= a[mid] && x <= a[right]) {
          return search(a, mid + 1, right, x);
      } else {
          return search(a, left, mid - 1, x);
      }               
  } else if (a[left] == a[mid]) { // Left is either all repeats OR loops around (with the right half being all dups)
      if (a[mid] != a[right]) { // If right half is different, search there
          return search(a, mid + 1, right, x);
      } else { // Else, we have to search both halves
          int result = search(a, left, mid - 1, x); 
          if (result == -1) {
              return search(a, mid + 1, right, x); 
          } else {
              return result;
          }
      }
  }
  return -1;
}

int main(){
  int a[] = {10,15,20,0,5};
  int b[] = {50,5,20,30,40};

  int l = 0;
  int h = 4;
  int x = 5;
  cout << search(a,l,h,x) << endl;
  cout << search(b,l,h,x) << endl;
}